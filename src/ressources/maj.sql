SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `planningpoker`.`Statistique` 
CHANGE COLUMN `valeurMoenne` `valeur_moyenne` VARCHAR(5) NULL DEFAULT NULL ,
CHANGE COLUMN `valeur_ecart` `valeur_ecart` VARCHAR(5) NULL DEFAULT NULL ,
CHANGE COLUMN `valeur_absolu` `absolu_cumule` VARCHAR(5) NULL DEFAULT NULL ,
ADD COLUMN `ecart_cumule` VARCHAR(5) NULL DEFAULT NULL AFTER `absolu_cumule`;

ALTER TABLE `planningpoker`.`Statistique` 
DROP COLUMN `ecart_cumule`,
DROP COLUMN `absolu_cumule`;

ALTER TABLE `planningpoker`.`Statistique` 
ADD COLUMN `idStat` INT(11) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`idStat`);



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

