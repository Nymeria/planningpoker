/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package composant;

import java.awt.TextArea;

import javax.swing.JButton;
import javax.swing.JTextField;

import main.Interface;

/**
 * Permet de gérer l'affichage du chat sur le plateau de jeu.
 * 
 * @author morgan
 */
public class ChatGraphique {

	public static TextArea textArea;
	public JButton btnEnvoi;
	public JTextField textField;
	public JButton btnFini;

	/**
	 * Constructeur de la partie graphique du chat.
	 */
	public ChatGraphique() {
		textArea = new TextArea();
		textArea.setEditable(false);
		textField = (new JTextField());
		textField.setColumns(10);
		btnEnvoi = new JButton("envoyer");

		btnFini = new JButton("j'ai fini");

	}

	/**
	 * Permet d'autoriser l'utilisation du chat.
	 */
	public void enableChat() {
		Interface.setFinDiscution(false) ;
		this.textField.setEnabled(true);
		this.btnEnvoi.setEnabled(true);
		this.btnFini.setEnabled(true);

	}

	/**
	 * Permet de bloquer l'utilisation du chat.
	 */
	public void disableChat() {
		this.textField.setEnabled(false);
		this.btnEnvoi.setEnabled(false);
		this.btnFini.setEnabled(false);
	}

}
