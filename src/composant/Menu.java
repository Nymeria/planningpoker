/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composant;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import main.Interface;
import static main.Interface.getMaPartie;
import static main.Interface.idSalon;
import main.Choixconnect;
import main.Main;
import main.PannelgestionSalon;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import api.Api;
import api.ScrumMaster;
import thread.ThreadParticipant;

/**
 * Permet de gérer le menu sur le fenêtre principale.
 * 
 * @author morgan
 */
public class Menu {

	public JMenuItem mntmCrerSalon;
	public JMenuItem mntmGrerSalon;
	public JMenuItem mntmRejoindreSalon;
	public JMenuItem mntmQuitterSalon;
	public JMenu mnCompte;
	public JMenuItem mntmConnection;
	public JMenuItem mntmDconnection;
	JButton btnClose;
	public boolean boolGerer = false;

	/**
	 * Constructeur du menu.
	 */
	public Menu() {
		mntmGrerSalon = new JMenuItem("Gérer salon");
		mntmCrerSalon = new JMenuItem("Créer salon");
		mntmRejoindreSalon = new JMenuItem("Rejoindre salon");
		mntmQuitterSalon = new JMenuItem("Quitter salon");
		mnCompte = new JMenu("Compte");
		mntmConnection = new JMenuItem("Connection");
		mntmDconnection = new JMenuItem("Déconnection");
	}

	/**
	 * Permet de gérer l'action sur le bouton déconnexion
	 * 
	 * @param interfc l'interface où est le menu.
	 */
	public void actionPerformedDeconnection(Interface interfc) {
		if (getMaPartie().is_logged == true) {
			getMaPartie().quiterSalon(idSalon);
			interfc.chatThread.arret();
			interfc.partieDemarrerThread.arret();
			interfc.participantThread.arret();
			interfc.table_1.setVisible(false);
			interfc.table_3.setVisible(false);
			interfc.panel_cartes_participants.setVisible(false);
			interfc.chat.textArea.setVisible(false);
			for (Component component : interfc.zone_clavardage.getComponents()) {
				component.setEnabled(false);
			}
			for (Component component : interfc.panel.getComponents()) {
				component.setEnabled(false);
			}
			getMaPartie().is_logged = false;
		} else {
			new JOptionPane();
			JOptionPane.showMessageDialog(null, "Vous n'êtes pas connecté",
					"Action impossible", JOptionPane.WARNING_MESSAGE);
		}
	}

	/**
	 * permet de gérer l'action sur le bouton Quitter salon
	 * 
	 * @param interfc l'interface où est le menu.
	 */
	public void actionPerformedQuitterSalon(Interface interfc) {
		ScrumMaster scrum = new ScrumMaster(interfc.getMaPartie().url);
		if (ThreadParticipant.nbParticipantEnLigne()<=1)
		{
			scrum.mettrePartieEnPause(idSalon);
		}
		getMaPartie().quiterSalon(idSalon);
		interfc.chatThread.arret();
		interfc.partieDemarrerThread.arret();
		interfc.participantThread.arret();
		Choixconnect ch = new Choixconnect(interfc.getMaPartie());
		ch.setVisible(true);
		interfc.dispose();
	}

	/**
	 * Gère l'action sur le menu Gérer salon
	 * 
	 * @param idSalon idSalon l'id du salon.
	 * @param idUtilisateur l'id de l'utilisateur.
	 * @param onglets Les onglets.
	 */
	public void actionPerformedGererSalon(String idSalon, Object idUtilisateur, final JTabbedPane onglets) {
		if (getMaPartie() != null) {
			JSONArray salons = getMaPartie().salonByOwner();
			boolean estOwner = false;
			int cpt = 0;
			while (cpt < salons.size() && estOwner == false) {
				JSONObject salon = (JSONObject) salons.get(cpt);
				if (idSalon.equals(salon.get("idSalon"))) {
					estOwner = (salon.get("owner_id").equals(idUtilisateur));
				}
				cpt++;
			}
			if (estOwner) {
				if (!boolGerer) {
					boolGerer = true;

					final PannelgestionSalon c = new PannelgestionSalon(getMaPartie(), idSalon);
					onglets.addTab(null, c);
					int pos = onglets.indexOfComponent(c);
					FlowLayout f = new FlowLayout(FlowLayout.CENTER, 5, 0);
					JPanel pnlTab = new JPanel(f);
					pnlTab.setOpaque(false);
					JLabel lblTitle = new JLabel("Gestion");
					btnClose = new JButton();
					btnClose.setOpaque(false);
					btnClose.setRolloverIcon(new ImageIcon(Main.class.getResource("/ImgCarte/closeTabButton.png")));
					btnClose.setRolloverEnabled(true);
					btnClose.setIcon(new ImageIcon(Main.class.getResource("/ImgCarte/closeTabButton_desab.png")));
					btnClose.setBorder(null);
					btnClose.setFocusable(false);
					pnlTab.add(lblTitle);
					pnlTab.add(btnClose);
					pnlTab.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
					onglets.setTabComponentAt(pos, pnlTab);

					btnClose.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							onglets.remove(c);
							boolGerer = false;
						}
					});
				}
			} else {
				new JOptionPane();
				JOptionPane.showMessageDialog(null, "Vous n'êtes pas le "
						+ "propriétaire de la partie", "Action impossible", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
