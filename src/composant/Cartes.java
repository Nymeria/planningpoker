/*
 * To change interfc license header, choose License Headers in Project Properties.
 * To change interfc template file, choose Tools | Templates
 * and open the template in the editor.
 */

package composant;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import main.BtnListener;
import main.Interface;
import static main.Interface.idSalon;
import main.TestFram;

/**
 * Cette classe permet de gérer l'affichage des cartes sur le plateau de jeu.
 * 
 * @author morgan
 */
public class Cartes {

	public JButton btnCardInterro;
	public JButton btnCard0;
	public JButton btnCard1demi;
	public JButton btnCard1;
	public JButton btncard2;
	public JButton btnCard3;
	public JButton btnCard5;
	public JButton btnCard8;
	public JButton btnCard13;
	public JButton btnCard20;
	public JButton btnCard40;
	public JButton btnCard100;
	public JButton btnCardCafe;
	public boolean estClique = false;

	/**
	 * Permet d'initialiser les cartes.
	 * 
	 * @param interfc l'interface contenant le plateau de jeu.
	 */
	public void init(Interface interfc) {
		/**
		 * **************************************	Bouton Carte Interrogation
		 * ***********************************
		 */
		btnCardInterro = new JButton("");
		btnCardInterro.addMouseListener(new BtnListener(interfc, estClique, "?", idSalon));
		btnCardInterro.setBorder(null);
		btnCardInterro.setBackground(Color.WHITE);
		btnCardInterro.setOpaque(false);
		interfc.panel.add(btnCardInterro);
		btnCardInterro.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/interro.png")));
		btnCardInterro.setPreferredSize(new Dimension(89, 135));

		/**
		 * ********************************************************************************************************
		 */

		/**
		 * ****************************************	Bouton Carte Cafe
		 * *******************************************
		 */
		btnCardCafe = new JButton("");
		btnCardCafe.addMouseListener(new BtnListener(interfc, estClique, "café", idSalon));
		btnCardCafe.setBorder(null);
		btnCardCafe.setBackground(Color.WHITE);
		btnCardCafe.setOpaque(false);
		interfc.panel.add(btnCardCafe);
		btnCardCafe.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/cafe.png")));
		btnCardCafe.setPreferredSize(new Dimension(89, 135));

		/**
		 * ********************************************************************************************************
		 */
		/**
		 * **************************************	Bouton Carte 0
		 * ***********************************************
		 */
		btnCard0 = new JButton("");
		btnCard0.setBorder(null);
		btnCard0.setBackground(Color.WHITE);
		btnCard0.setOpaque(false);
		interfc.panel.add(btnCard0);
		btnCard0.addMouseListener(new BtnListener(interfc, estClique, "0", idSalon));
		btnCard0.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/0.png")));
		btnCard0.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 1/2
		 * *******************************************
		 */
		btnCard1demi = new JButton("");
		btnCard1demi.setBorder(null);
		btnCard1demi.setBackground(Color.WHITE);
		btnCard1demi.setOpaque(false);
		interfc.panel.add(btnCard1demi);
		btnCard1demi.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/1-2.png")));
		btnCard1demi.addMouseListener(new BtnListener(interfc, estClique, "0.5", idSalon));
		btnCard1demi.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 1
		 * ***********************************************
		 */
		btnCard1 = new JButton("");
		btnCard1.addMouseListener(new BtnListener(interfc, estClique, "1", idSalon));
		btnCard1.setBorder(null);
		btnCard1.setBackground(Color.WHITE);
		btnCard1.setOpaque(false);
		interfc.panel.add(btnCard1);
		btnCard1.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/1.png")));
		btnCard1.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 2
		 * ***********************************************
		 */
		btncard2 = new JButton("");
		btncard2.addMouseListener(new BtnListener(interfc, estClique, "2", idSalon));
		btncard2.setBorder(null);
		btncard2.setBackground(Color.WHITE);
		btncard2.setOpaque(false);
		interfc.panel.add(btncard2);
		btncard2.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/2.png")));
		btncard2.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 3
		 * ***********************************************
		 */
		btnCard3 = new JButton("");
		btnCard3.addMouseListener(new BtnListener(interfc, estClique, "3", idSalon));
		btnCard3.setBorder(null);
		btnCard3.setBackground(Color.WHITE);
		btnCard3.setOpaque(false);
		interfc.panel.add(btnCard3);
		btnCard3.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/3.png")));
		btnCard3.setPreferredSize(new Dimension(89, 135));
		btnCard3.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 5
		 * ***********************************************
		 */
		btnCard5 = new JButton("");
		btnCard5.addMouseListener(new BtnListener(interfc, estClique, "5", idSalon));
		btnCard5.setBorder(null);
		btnCard5.setBackground(Color.WHITE);
		btnCard5.setOpaque(false);
		interfc.panel.add(btnCard5);
		btnCard5.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/5.png")));
		btnCard5.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 8
		 * ***********************************************
		 */
		btnCard8 = new JButton("");
		btnCard8.addMouseListener(new BtnListener(interfc, estClique, "8", idSalon));
		btnCard8.setBorder(null);
		btnCard8.setBackground(Color.WHITE);
		btnCard8.setOpaque(false);
		interfc.panel.add(btnCard8);
		btnCard8.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/8.png")));
		btnCard8.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 13
		 * ***********************************************
		 */
		btnCard13 = new JButton("");
		btnCard13.addMouseListener(new BtnListener(interfc, estClique, "13", idSalon));
		btnCard13.setBorder(null);
		btnCard13.setBackground(Color.WHITE);
		btnCard13.setOpaque(false);
		interfc.panel.add(btnCard13);
		btnCard13.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/13.png")));
		btnCard13.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 20
		 * ***********************************************
		 */
		btnCard20 = new JButton("");
		btnCard20.addMouseListener(new BtnListener(interfc, estClique, "20", idSalon));
		btnCard20.setBorder(null);
		btnCard20.setBackground(Color.WHITE);
		btnCard20.setOpaque(false);
		interfc.panel.add(btnCard20);
		btnCard20.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/20.png")));
		btnCard20.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 40
		 * ***********************************************
		 */
		btnCard40 = new JButton("");
		btnCard40.addMouseListener(new BtnListener(interfc, estClique, "40", idSalon));
		btnCard40.setBorder(null);
		btnCard40.setBackground(Color.WHITE);
		btnCard40.setOpaque(false);
		interfc.panel.add(btnCard40);
		btnCard40.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/40.png")));
		btnCard40.setPreferredSize(new Dimension(89, 135));
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton Carte 100
		 * *******************************************
		 */
		btnCard100 = new JButton("");
		btnCard100.addMouseListener(new BtnListener(interfc, estClique, "100", idSalon));
		btnCard100.setBorder(null);
		btnCard100.setBackground(Color.WHITE);
		btnCard100.setOpaque(false);
		interfc.panel.add(btnCard100);
		btnCard100.setIcon(new ImageIcon(TestFram.class.getResource("/ImgCarte/100.png")));
		btnCard100.setPreferredSize(new Dimension(89, 135));
	}  

	/**
	 * Permet d'activer les cartes. 
	 */
	public void enableInterface() {

		btnCardInterro.setEnabled(true);
		btnCardCafe.setEnabled(true);
		btnCard0.setEnabled(true);
		btnCard1.setEnabled(true);
		btnCard1demi.setEnabled(true);
		btncard2.setEnabled(true);
		btnCard3.setEnabled(true);
		btnCard5.setEnabled(true);
		btnCard8.setEnabled(true);
		btnCard13.setEnabled(true);
		btnCard20.setEnabled(true);
		btnCard40.setEnabled(true);
		btnCard100.setEnabled(true);

	}

	/**
	 * Permet de désactiver les cartes.
	 */
	public void disableInterface() {

		btnCardInterro.setEnabled(false);
		btnCardCafe.setEnabled(false);
		btnCard0.setEnabled(false);
		btnCard1.setEnabled(false);
		btnCard1demi.setEnabled(false);
		btncard2.setEnabled(false);
		btnCard3.setEnabled(false);
		btnCard5.setEnabled(false);
		btnCard8.setEnabled(false);
		btnCard13.setEnabled(false);
		btnCard20.setEnabled(false);
		btnCard40.setEnabled(false);
		btnCard100.setEnabled(false);
	}

}
