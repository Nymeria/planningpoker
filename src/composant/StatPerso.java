package composant;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.json.simple.JSONArray;

import net.sourceforge.chart2d.Chart2D;
import main.Graphiques;
import api.Statistique;

/**
 * Cette classe permet d'afficher les statistiques personnels
 * pour le backlog en cours du joueur
 */
public class StatPerso{

	//Attributs

	private int numTour;
	private JLabel lblNumTour;
	private JPanel monPanel;
	private JPanel monGrandPanel;
	private JLabel lblNomBacklog;
	private Statistique stat;
	private Graphiques graph;
	private Chart2D chart;

	/**
	 * Constructeur de la classe : Initialise toute les valeurs
	 * pour obtenir un affichage à un tours 0.
	 */
	public StatPerso(String url) {
		stat = new Statistique(url);
		graph = new Graphiques(stat);

		monGrandPanel = new JPanel();
		monGrandPanel.setLayout(new GridLayout(0, 1, 0, 0));
		monPanel = new JPanel();
		monPanel.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblBacklogCours = new JLabel("Tâche en cours :");
		lblBacklogCours.setHorizontalAlignment(JLabel.CENTER);
		monPanel.add(lblBacklogCours);

		lblNomBacklog = new JLabel("");
		monPanel.add(lblNomBacklog);

		JLabel lblTour = new JLabel("Tour numéro : ");
		lblTour.setHorizontalAlignment(JLabel.CENTER);
		monPanel.add(lblTour);

		lblNumTour = new JLabel("");
		monPanel.add(lblNumTour);

		chart = graph.TendanceVoteTache(null, lblNomBacklog.getText());
		chart.setMinimumSize(new Dimension(monGrandPanel.getWidth(), 150));

		monGrandPanel.add(monPanel);
		monGrandPanel.add(chart);


		numTour = 0;
	}

	/**
	 * Cette fonction permet de mettre à jour les statistiques personnels pour un
	 * nouveau tour.
	 * 
	 * @param tendance le tableau contenant les statistiques du joueur pour un
	 * backlog donné
	 */
	public void majStats(JSONArray tendance){
		// On récupère les valeurs utiles pour la mise à jour
		if (tendance != null){
		numTour = tendance.size();
		}
		else{
		numTour = 0;
		}
		// On met à jour les labels
		lblNumTour.setText(String.valueOf(numTour));
		if (numTour !=0)
		{
		chart = graph.TendanceVoteTache(tendance, lblNomBacklog.getText());
		chart.setMinimumSize(new Dimension(monGrandPanel.getWidth(), 150));
		monGrandPanel.remove(1);
		monGrandPanel.add(chart);
		}

	}

	/**
	 * Cette fonction permet de reinitialiser les valeurs des labels pour
	 * les statistiques personnels. Elle correspond au tour 0 d'un nouveau backlog.
	 */
	public void resetStats(){
		numTour = 0;
		lblNumTour.setText("0");
	}

	/**
	 * Cette fonction permet de mettre à jour le label indiquant
	 * le nom du backlog en cours.
	 * 
	 * @param nomBacklog Le nom du backlog en cours.
	 */
	public void changeNomBacklog(String nomBacklog){
		lblNomBacklog.setText(nomBacklog);
	}

	/**
	 * Cette fonction permet de récuperer le panel
	 * 
	 * @return Le Panel contenant les différents labels.
	 */
	public JPanel getPanel() {
		return this.monGrandPanel;
	}

}