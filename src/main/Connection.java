package main;


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JTextField;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

import org.json.simple.JSONObject;

import api.Partie;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;

/**
 * Cette classe implémente le fenêtre de connection.
 */
public class Connection extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogin;
	private JTextField valLogin;
	private JTextField txtPassword;
	private JTextField txtUrl;
	private JTextField valUrl;
	private Partie maPartie;
	private JPasswordField valPassword;
	private JButton inscription;

	/**
	 * Crée la fenêtre.
	 */
	public Connection() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 321);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{60, 0, 0, 0, 0, 0, 66, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		txtLogin = new JTextField();
		txtLogin.setFocusable(false);
		txtLogin.setEditable(false);
		txtLogin.setMinimumSize(new Dimension(100, 20));
		txtLogin.setFont(new Font("Times New Roman", Font.BOLD, 16));
		txtLogin.setText("      Login : ");
		GridBagConstraints gbc_txtLogin = new GridBagConstraints();
		gbc_txtLogin.anchor = GridBagConstraints.SOUTH;
		gbc_txtLogin.insets = new Insets(0, 0, 5, 0);
		gbc_txtLogin.gridx = 0;
		gbc_txtLogin.gridy = 0;
		contentPane.add(txtLogin, gbc_txtLogin);
		txtLogin.setColumns(10);
		
		valLogin = new JTextField();
		valLogin.setText("Nymeria");
		valLogin.setMinimumSize(new Dimension(100, 20));
		GridBagConstraints gbc_valLogin = new GridBagConstraints();
		gbc_valLogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_valLogin.insets = new Insets(0, 0, 5, 0);
		gbc_valLogin.gridx = 0;
		gbc_valLogin.gridy = 1;
		contentPane.add(valLogin, gbc_valLogin);
		valLogin.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setFocusable(false);
		txtPassword.setEditable(false);
		txtPassword.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtPassword.setMinimumSize(new Dimension(100, 20));
		txtPassword.setPreferredSize(new Dimension(100, 20));
		txtPassword.setText("     Password : ");
		GridBagConstraints gbc_txtPassword = new GridBagConstraints();
		gbc_txtPassword.insets = new Insets(0, 0, 5, 0);
		gbc_txtPassword.gridx = 0;
		gbc_txtPassword.gridy = 2;
		contentPane.add(txtPassword, gbc_txtPassword);
		txtPassword.setColumns(10);
		
		valPassword = new JPasswordField();
		valPassword.setText("icescrum");
		GridBagConstraints gbc_valPassword = new GridBagConstraints();
		gbc_valPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_valPassword.insets = new Insets(0, 0, 5, 0);
		gbc_valPassword.gridx = 0;
		gbc_valPassword.gridy = 3;
		contentPane.add(valPassword, gbc_valPassword);
		
		txtUrl = new JTextField();
		txtUrl.setFocusable(false);
		txtUrl.setText("     Url : ");
		txtUrl.setPreferredSize(new Dimension(100, 20));
		txtUrl.setMinimumSize(new Dimension(100, 20));
		txtUrl.setFont(new Font("Dialog", Font.BOLD, 14));
		txtUrl.setEditable(false);
		txtUrl.setColumns(10);
		GridBagConstraints gbc_txtUrl = new GridBagConstraints();
		gbc_txtUrl.insets = new Insets(0, 0, 5, 0);
		gbc_txtUrl.gridx = 0;
		gbc_txtUrl.gridy = 4;
		contentPane.add(txtUrl, gbc_txtUrl);
		
		valUrl = new JTextField();
		valUrl.setText("http://192.168.1.5");
		valUrl.setMinimumSize(new Dimension(100, 20));
		valUrl.setColumns(10);
		GridBagConstraints gbc_valUrl = new GridBagConstraints();
		gbc_valUrl.fill = GridBagConstraints.HORIZONTAL;
		gbc_valUrl.insets = new Insets(0, 0, 5, 0);
		gbc_valUrl.gridx = 0;
		gbc_valUrl.gridy = 5;
		contentPane.add(valUrl, gbc_valUrl);
		
		JButton btnNewButton = new JButton("envoyer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				maPartie = new Partie(valUrl.getText());
				JSONObject val = maPartie.login(valLogin.getText(),valPassword.getText());
				if (val != null)
				{
					maPartie.idUser = (String) val.get("idUtilisateur");
					maPartie.is_logged = true;
					dispose();
							try {
								Choixconnect frame = new Choixconnect(maPartie);
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
				}
				else
				{
					JOptionPane jop2;
					//Boîte du message préventif
					jop2 = new JOptionPane();
					jop2.showMessageDialog(null, "L'utilisateur ou le mot de passe ne correspondent pas.",
							"Login impossible", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.NORTH;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 6;
		contentPane.add(btnNewButton, gbc_btnNewButton);
		
		inscription = new JButton("Inscription");
		inscription.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Inscription frameInscription = new Inscription(valUrl.getText());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				dispose();
			}
		});
		GridBagConstraints gbc_inscription = new GridBagConstraints();
		gbc_inscription.anchor = GridBagConstraints.SOUTH;
		gbc_inscription.gridx = 0;
		gbc_inscription.gridy = 7;
		contentPane.add(inscription, gbc_inscription);
		}}