package main;


import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JTextPane;

import java.awt.GridBagConstraints;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import api.Partie;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Cette classe implémente la fenêtre d'inscription.
 */
public class Inscription {

	private JFrame frame;
	private JTextField txtPrenom;
	private JTextField txtNom;
	private JTextField txtPseudo;
	private JTextPane txtpnConfirmerMotDe;
	private JTextPane txtpnSiVousUltilisez;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JButton btnValider;
	String Url;
	Partie maPartie;

	/**
	 * Crée la fenêtre d'inscription.
	 * 
	 * @param URL l'url du serveur de jeu.
	 */
	public Inscription(String URL) {
		this.Url = URL;
		maPartie = new Partie(Url);
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialise les composants de la fenêtre.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 748, 444);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);

		JTextPane txtpnNom = new JTextPane();
		txtpnNom.setEditable(false);
		txtpnNom.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnNom.setText("Prénom :");
		GridBagConstraints gbc_txtpnNom = new GridBagConstraints();
		gbc_txtpnNom.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnNom.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnNom.gridx = 0;
		gbc_txtpnNom.gridy = 0;
		frame.getContentPane().add(txtpnNom, gbc_txtpnNom);

		JTextPane txtpnPrnom = new JTextPane();
		txtpnPrnom.setEditable(false);
		txtpnPrnom.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnPrnom.setText("Nom :");
		GridBagConstraints gbc_txtpnPrnom = new GridBagConstraints();
		gbc_txtpnPrnom.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnPrnom.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnPrnom.gridx = 1;
		gbc_txtpnPrnom.gridy = 0;
		frame.getContentPane().add(txtpnPrnom, gbc_txtpnPrnom);

		txtPrenom = new JTextField();
		GridBagConstraints gbc_txtPrenom = new GridBagConstraints();
		gbc_txtPrenom.insets = new Insets(0, 0, 5, 5);
		gbc_txtPrenom.anchor = GridBagConstraints.NORTH;
		gbc_txtPrenom.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPrenom.gridx = 0;
		gbc_txtPrenom.gridy = 1;
		frame.getContentPane().add(txtPrenom, gbc_txtPrenom);
		txtPrenom.setColumns(10);

		txtNom = new JTextField();
		GridBagConstraints gbc_txtNom = new GridBagConstraints();
		gbc_txtNom.insets = new Insets(0, 0, 5, 0);
		gbc_txtNom.anchor = GridBagConstraints.NORTH;
		gbc_txtNom.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNom.gridx = 1;
		gbc_txtNom.gridy = 1;
		frame.getContentPane().add(txtNom, gbc_txtNom);
		txtNom.setColumns(10);

		JTextPane txtpnPseudo = new JTextPane();
		txtpnPseudo.setEditable(false);
		txtpnPseudo.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnPseudo.setText("Pseudo :");
		GridBagConstraints gbc_txtpnPseudo = new GridBagConstraints();
		gbc_txtpnPseudo.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnPseudo.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnPseudo.gridx = 0;
		gbc_txtpnPseudo.gridy = 2;
		frame.getContentPane().add(txtpnPseudo, gbc_txtpnPseudo);

		JTextPane txtpnMotDePasse = new JTextPane();
		txtpnMotDePasse.setEditable(false);
		txtpnMotDePasse.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnMotDePasse.setText("Mot de passe :");
		GridBagConstraints gbc_txtpnMotDePasse = new GridBagConstraints();
		gbc_txtpnMotDePasse.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnMotDePasse.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnMotDePasse.gridx = 1;
		gbc_txtpnMotDePasse.gridy = 2;
		frame.getContentPane().add(txtpnMotDePasse, gbc_txtpnMotDePasse);

		txtPseudo = new JTextField();
		GridBagConstraints gbc_txtPseudo = new GridBagConstraints();
		gbc_txtPseudo.anchor = GridBagConstraints.NORTH;
		gbc_txtPseudo.insets = new Insets(0, 0, 5, 5);
		gbc_txtPseudo.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPseudo.gridx = 0;
		gbc_txtPseudo.gridy = 3;
		frame.getContentPane().add(txtPseudo, gbc_txtPseudo);
		txtPseudo.setColumns(10);

		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.anchor = GridBagConstraints.NORTH;
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.gridx = 1;
		gbc_passwordField.gridy = 3;
		frame.getContentPane().add(passwordField, gbc_passwordField);

		txtpnSiVousUltilisez = new JTextPane();
		txtpnSiVousUltilisez.setEditable(false);
		txtpnSiVousUltilisez.setFont(new Font("Times New Roman", Font.ITALIC, 16));
		txtpnSiVousUltilisez.setText("Si vous ultilisez IceScrum, merci d'utiliser \r\n     le même pseudo et mot de passe.");
		GridBagConstraints gbc_txtpnSiVousUltilisez = new GridBagConstraints();
		gbc_txtpnSiVousUltilisez.gridheight = 2;
		gbc_txtpnSiVousUltilisez.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnSiVousUltilisez.fill = GridBagConstraints.VERTICAL;
		gbc_txtpnSiVousUltilisez.gridx = 0;
		gbc_txtpnSiVousUltilisez.gridy = 4;
		frame.getContentPane().add(txtpnSiVousUltilisez, gbc_txtpnSiVousUltilisez);

		txtpnConfirmerMotDe = new JTextPane();
		txtpnConfirmerMotDe.setEditable(false);
		txtpnConfirmerMotDe.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnConfirmerMotDe.setText("Confirmer mot de passe :");
		GridBagConstraints gbc_txtpnConfirmerMotDe = new GridBagConstraints();
		gbc_txtpnConfirmerMotDe.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnConfirmerMotDe.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnConfirmerMotDe.gridx = 1;
		gbc_txtpnConfirmerMotDe.gridy = 4;
		frame.getContentPane().add(txtpnConfirmerMotDe, gbc_txtpnConfirmerMotDe);

		passwordField_1 = new JPasswordField();
		GridBagConstraints gbc_passwordField_1 = new GridBagConstraints();
		gbc_passwordField_1.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField_1.anchor = GridBagConstraints.NORTH;
		gbc_passwordField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField_1.gridx = 1;
		gbc_passwordField_1.gridy = 5;
		frame.getContentPane().add(passwordField_1, gbc_passwordField_1);

		//bouton valider
		btnValider = new JButton("Valider");
		btnValider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JOptionPane erreurMDP;
				if ( passwordField.getText().equals(passwordField_1.getText())) {
					maPartie.inscription(txtNom.getText(),txtPrenom.getText(),txtPseudo.getText(),passwordField_1.getText());
					frame.dispose();
					Connection connect = new Connection();
					connect.setVisible(true);
				}
				else {
					erreurMDP = new JOptionPane();
					JOptionPane.showMessageDialog(null, "Veuillez vérifier que les deux champs de mot de passe sont identiques", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
				try {
					Connection frameConnection = new Connection();
				} catch (Exception e2) {
					e2.printStackTrace();
				}

			}

		});
		GridBagConstraints gbc_btnValider = new GridBagConstraints();
		gbc_btnValider.gridwidth = 2;
		gbc_btnValider.insets = new Insets(0, 0, 0, 5);
		gbc_btnValider.gridx = 0;
		gbc_btnValider.gridy = 6;
		frame.getContentPane().add(btnValider, gbc_btnValider);
	}

}
