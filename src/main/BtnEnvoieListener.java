package main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Permet de gérer les actions sur le bouton de
 * fin d'argumentation.
 */
public class BtnEnvoieListener implements MouseListener {

	private Interface f ;
	private String idUtilisateur;

	/**
	 * Crée le listener.
	 * 
	 * @param f le plateau de jeu.
	 * @param idUtilisateur l'id du joueur.
	 */
	public BtnEnvoieListener (Interface f,String idUtilisateur){
		this.f=f;
		this.idUtilisateur=idUtilisateur;
	}

	@Override
	/**
	 * Permet d'effectuer l'action de fin d'argumentation lors du clique.
	 */
	public void mouseClicked(MouseEvent me) {
		if (f.chat.textField.getText().isEmpty() == false || !f.chat.textField.getText().equals("Le scrum master à mis fin à la discution") || !f.chat.textField.getText().equals("c'est déconnecter")) {

			Interface.apiChat.envoyerMessage(f.chat.textField.getText(), idUtilisateur);
			if (f.chat.textField.getText().equals("j'ai fini d'argumenter")){
				f.disableChat();
			}
			f.chat.textField.setText("");
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Stub de la méthode généré automatiquement

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Stub de la méthode généré automatiquement

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Stub de la méthode généré automatiquement

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Stub de la méthode généré automatiquement

	}

}
