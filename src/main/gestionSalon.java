package main;

import api.Chat;

import javax.swing.JFrame;

import api.Participant;
import api.Partie;
import api.ScrumMaster;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import java.awt.GridBagConstraints;

import javax.swing.JButton;

import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Dimension;

import javax.swing.JSeparator;

import java.awt.Font;

import javax.swing.SwingConstants;


public class gestionSalon extends JFrame {
	private JTextField txtTachesSupprimer;
	private JPanel pnUtilisateur;

	/************************************************/
	/*		Definitions des variables				*/
	/************************************************/

	JSONArray utilisateurs;
	JSONArray backlog;
	private api.ScrumMaster monApi;
	private Participant mesParticipants;
	private String idSalon = "";
	private JComboBox cBoxUtilisateurs;
	private JTable table;
	private JTable tabUtilisateurs;
	private JTextField txtUtilisateurSupprimer;
	private JTable tabTache;
	private Partie mapartie;
	DefaultTableModel modeltabUtilisateurs;
	DefaultTableModel modeltabTaches;
	JScrollPane scrolltabUtilisateur;
	private JTextField txtAjoutNom;
	private JTextField txtAjoutDescription;
	Chat chat;


	/************************************************/
	/*		Constructeur							*/
	/************************************************/

	/**
	 * Crée le fenêtre de gestion de salon.
	 * 
	 * @param mapartie la partie en cours.
	 * @param idSalon le salon à gérer.
	 */
	public gestionSalon(final Partie mapartie, final String idSalon) {
		getContentPane().setMinimumSize(new Dimension(560, 0));
		getContentPane().setMaximumSize(new Dimension(560, 2147483647));
		setMinimumSize(new Dimension(1180, 630));
		setMaximumSize(new Dimension(1180, 630));
		setResizable(false);
		monApi = new ScrumMaster(mapartie.url);
		mesParticipants = new Participant(mapartie.url);
		monApi.idUser = mapartie.idUser;
		monApi.is_logged = true;
		this.idSalon = idSalon;
		this.setMapartie(mapartie);
		chat = new Chat(mapartie.url, idSalon);
		cBoxUtilisateurs = new JComboBox<String>();
		backlog = mapartie.recupererBackLog(idSalon);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{600, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 419, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addWindowListener(new WindowCloserChoixConnect(this));
		setBounds(100, 100, 1215, 630);
		getContentPane().setLayout(gridBagLayout);

		JPanel pnPartie = new JPanel();
		GridBagConstraints gbc_pnPartie = new GridBagConstraints();
		gbc_pnPartie.gridwidth = 2;
		gbc_pnPartie.insets = new Insets(0, 0, 5, 0);
		gbc_pnPartie.fill = GridBagConstraints.BOTH;
		gbc_pnPartie.gridx = 0;
		gbc_pnPartie.gridy = 0;
		getContentPane().add(pnPartie, gbc_pnPartie);
		GridBagLayout gbl_pnPartie = new GridBagLayout();
		gbl_pnPartie.columnWidths = new int[]{306, 297, 387, 216, 0};
		gbl_pnPartie.rowHeights = new int[]{0, 0, 29, 28, 33, 0, 0, 0, 0};
		gbl_pnPartie.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pnPartie.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		pnPartie.setLayout(gbl_pnPartie);

		JButton btnDemarrer = new JButton("Demarrer");
		btnDemarrer.setMaximumSize(new Dimension(300, 20));
		btnDemarrer.setMinimumSize(new Dimension(300, 20));
		btnDemarrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				monApi.demarrerPartie(idSalon);
			}
		});
		GridBagConstraints gbc_btnDemarrer = new GridBagConstraints();
		gbc_btnDemarrer.gridwidth = 2;
		gbc_btnDemarrer.fill = GridBagConstraints.BOTH;
		gbc_btnDemarrer.insets = new Insets(0, 0, 5, 5);
		gbc_btnDemarrer.gridx = 0;
		gbc_btnDemarrer.gridy = 0;
		pnPartie.add(btnDemarrer, gbc_btnDemarrer);

		/******* ComboBox pour tous les utilisateurs	******/
		mettreUtilisateurDansComboBox();

		JPanel pnUtilisateur = new JPanel();
		pnUtilisateur.setMinimumSize(new Dimension(660, 10));
		pnUtilisateur.setMaximumSize(new Dimension(660, 32767));
		GridBagConstraints gbc_pnUtilisateur = new GridBagConstraints();
		gbc_pnUtilisateur.insets = new Insets(0, 0, 0, 5);
		gbc_pnUtilisateur.fill = GridBagConstraints.BOTH;
		gbc_pnUtilisateur.gridx = 0;
		gbc_pnUtilisateur.gridy = 1;
		getContentPane().add(pnUtilisateur, gbc_pnUtilisateur);
		GridBagLayout gbl_pnUtilisateur = new GridBagLayout();
		gbl_pnUtilisateur.columnWidths = new int[]{266, 88, 227, 0};
		gbl_pnUtilisateur.rowHeights = new int[]{0, 0, 0, 18, 3, 0};
		gbl_pnUtilisateur.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pnUtilisateur.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnUtilisateur.setLayout(gbl_pnUtilisateur);

		JScrollPane scrolltabUtilisateur = new JScrollPane();
		GridBagConstraints gbc_scrolltabUtilisateur = new GridBagConstraints();
		gbc_scrolltabUtilisateur.gridwidth = 3;
		gbc_scrolltabUtilisateur.insets = new Insets(0, 0, 5, 0);
		gbc_scrolltabUtilisateur.fill = GridBagConstraints.BOTH;
		gbc_scrolltabUtilisateur.gridx = 0;
		gbc_scrolltabUtilisateur.gridy = 0;
		pnUtilisateur.add(scrolltabUtilisateur, gbc_scrolltabUtilisateur);

		tabUtilisateurs = new JTable();
		tabUtilisateurs.setMaximumSize(new Dimension(600, 0));
		tabUtilisateurs.setMinimumSize(new Dimension(600, 0));
		modeltabUtilisateurs = new modelTabUtilisateurs();
		tabUtilisateurs.setFillsViewportHeight(true);
		tabUtilisateurs.setModel(modeltabUtilisateurs);
		modeltabUtilisateurs.addColumn("ID");
		modeltabUtilisateurs.addColumn("Nom");
		modeltabUtilisateurs.addColumn("Prénom");
		/*tabUtilisateurs.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);*/
		//tabUtilisateurs.getColumnModel().getColumn(0).setResizable(false);
		//TableColumn col = tabUtilisateurs.getColumnModel().getColumn(0);
		//col.setPreferredWidth(1);

		scrolltabUtilisateur.setViewportView(tabUtilisateurs);

		mettreUtilisateurDansTableau();

		JLabel lblUserASupprimer = new JLabel(" ID de l'utilisateur à supprimer :");
		GridBagConstraints gbc_lblUserASupprimer = new GridBagConstraints();
		gbc_lblUserASupprimer.insets = new Insets(0, 0, 5, 5);
		gbc_lblUserASupprimer.gridx = 0;
		gbc_lblUserASupprimer.gridy = 1;
		pnUtilisateur.add(lblUserASupprimer, gbc_lblUserASupprimer);


		txtUtilisateurSupprimer = new JTextField();
		GridBagConstraints gbc_txtUtilisateurSupprimer = new GridBagConstraints();
		gbc_txtUtilisateurSupprimer.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUtilisateurSupprimer.insets = new Insets(0, 0, 5, 5);
		gbc_txtUtilisateurSupprimer.gridx = 1;
		gbc_txtUtilisateurSupprimer.gridy = 1;
		pnUtilisateur.add(txtUtilisateurSupprimer, gbc_txtUtilisateurSupprimer);
		txtUtilisateurSupprimer.setColumns(10);

		JButton btnSupprimerUtilisateur = new JButton("Supprimer");
		btnSupprimerUtilisateur.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				Boolean trouve = false;
				String idUserASupprimer = txtUtilisateurSupprimer.getText();
				if (idUserASupprimer != "" && !idUserASupprimer.equals(mapartie.idUser)) {
					for(i=0;i<modeltabUtilisateurs.getRowCount();++i) {
						String user = (String) modeltabUtilisateurs.getValueAt(i, 0);
						if(user.equals(idUserASupprimer) == false) {
							trouve = false;
						}
						else {
							trouve = true;
							break;
						}
					}
					if(trouve == true) {
						monApi.supprimerParticipant(idSalon, idUserASupprimer);
						supprimerUtilisateurTableau(idUserASupprimer);
						txtUtilisateurSupprimer.setText("");
					}
					else {
						JOptionPane popErreurSupprimer = new JOptionPane();
						popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer un utilisateur qui ne participe pas à la partie", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
						txtUtilisateurSupprimer.setText("");
					}
				}
				else {
					JOptionPane popErreurSupprimer = new JOptionPane();
					popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer le ScrumMaster de la partie", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
					txtUtilisateurSupprimer.setText("");
				}
			}
		});

		GridBagConstraints gbc_btnSupprimerUtilisateur = new GridBagConstraints();
		gbc_btnSupprimerUtilisateur.insets = new Insets(0, 0, 5, 0);
		gbc_btnSupprimerUtilisateur.gridx = 2;
		gbc_btnSupprimerUtilisateur.gridy = 1;
		pnUtilisateur.add(btnSupprimerUtilisateur, gbc_btnSupprimerUtilisateur);

		JSeparator separator_2 = new JSeparator();
		GridBagConstraints gbc_separator_2 = new GridBagConstraints();
		gbc_separator_2.gridwidth = 3;
		gbc_separator_2.insets = new Insets(0, 0, 5, 0);
		gbc_separator_2.gridx = 0;
		gbc_separator_2.gridy = 2;
		pnUtilisateur.add(separator_2, gbc_separator_2);
		
				JButton btnFinDiscussion = new JButton("Mettre fin a la discussion");
				btnFinDiscussion.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						chat.envoyerMessage("Le scrum master à mis fin à la discution", mapartie.idUser);
					}
				});
				GridBagConstraints gbc_btnFinDiscussion = new GridBagConstraints();
				gbc_btnFinDiscussion.insets = new Insets(0, 0, 5, 0);
				gbc_btnFinDiscussion.gridx = 2;
				gbc_btnFinDiscussion.gridy = 3;
				pnUtilisateur.add(btnFinDiscussion, gbc_btnFinDiscussion);

		JSeparator separator_1 = new JSeparator();
		GridBagConstraints gbc_separator_1 = new GridBagConstraints();
		gbc_separator_1.gridwidth = 3;
		gbc_separator_1.gridx = 0;
		gbc_separator_1.gridy = 4;
		pnUtilisateur.add(separator_1, gbc_separator_1);

		JButton btnPause = new JButton("Pause");
		btnPause.setMaximumSize(new Dimension(300, 20));
		btnPause.setMinimumSize(new Dimension(300, 20));
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				monApi.mettrePartieEnPause(idSalon);
				chat.envoyerMessage("Le scrum master a mis la partie en pause",mapartie.idUser);
			}
		});
		GridBagConstraints gbc_btnPause = new GridBagConstraints();
		gbc_btnPause.gridwidth = 2;
		gbc_btnPause.fill = GridBagConstraints.BOTH;
		gbc_btnPause.insets = new Insets(0, 0, 5, 0);
		gbc_btnPause.gridx = 2;
		gbc_btnPause.gridy = 0;
		pnPartie.add(btnPause, gbc_btnPause);

		JLabel lblAjoutUtilisateur = new JLabel("Ajouter un utilisateur :");
		lblAjoutUtilisateur.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblAjoutUtilisateur = new GridBagConstraints();
		gbc_lblAjoutUtilisateur.insets = new Insets(0, 0, 5, 5);
		gbc_lblAjoutUtilisateur.gridx = 0;
		gbc_lblAjoutUtilisateur.gridy = 2;
		pnPartie.add(lblAjoutUtilisateur, gbc_lblAjoutUtilisateur);

		JLabel lblAjouterTache = new JLabel("Ajouter une tâche :");
		lblAjouterTache.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblAjouterTache = new GridBagConstraints();
		gbc_lblAjouterTache.insets = new Insets(0, 0, 5, 0);
		gbc_lblAjouterTache.gridx = 2;
		gbc_lblAjouterTache.gridy = 2;
		pnPartie.add(lblAjouterTache, gbc_lblAjouterTache);

		JLabel lblAjoutNom = new JLabel("Nom :");
		lblAjoutNom.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutNom.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblAjoutNom.setMinimumSize(new Dimension(14, 14));
		lblAjoutNom.setMaximumSize(new Dimension(14, 14));
		GridBagConstraints gbc_lblAjoutNom = new GridBagConstraints();
		gbc_lblAjoutNom.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAjoutNom.insets = new Insets(0, 0, 5, 5);
		gbc_lblAjoutNom.gridx = 2;
		gbc_lblAjoutNom.gridy = 3;
		pnPartie.add(lblAjoutNom, gbc_lblAjoutNom);

		txtAjoutNom = new JTextField();
		txtAjoutNom.setMaximumSize(new Dimension(100, 20));
		GridBagConstraints gbc_txtAjoutNom = new GridBagConstraints();
		gbc_txtAjoutNom.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAjoutNom.insets = new Insets(0, 0, 5, 0);
		gbc_txtAjoutNom.gridx = 3;
		gbc_txtAjoutNom.gridy = 3;
		pnPartie.add(txtAjoutNom, gbc_txtAjoutNom);
		txtAjoutNom.setMinimumSize(new Dimension(100, 20));
		txtAjoutNom.setColumns(10);

		JLabel lblSelectionUtilisateur = new JLabel("Selectionner :");
		lblSelectionUtilisateur.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblSelectionUtilisateur.setMaximumSize(new Dimension(120, 14));
		lblSelectionUtilisateur.setMinimumSize(new Dimension(120, 14));
		GridBagConstraints gbc_lblSelectionUtilisateur = new GridBagConstraints();
		gbc_lblSelectionUtilisateur.anchor = GridBagConstraints.EAST;
		gbc_lblSelectionUtilisateur.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectionUtilisateur.gridx = 0;
		gbc_lblSelectionUtilisateur.gridy = 4;
		pnPartie.add(lblSelectionUtilisateur, gbc_lblSelectionUtilisateur);
		cBoxUtilisateurs.setMaximumSize(new Dimension(200, 20));
		cBoxUtilisateurs.setMinimumSize(new Dimension(200, 20));


		GridBagConstraints gbc_cBoxUtilisateurs = new GridBagConstraints();
		gbc_cBoxUtilisateurs.insets = new Insets(0, 0, 5, 5);
		gbc_cBoxUtilisateurs.fill = GridBagConstraints.HORIZONTAL;
		gbc_cBoxUtilisateurs.gridx = 1;
		gbc_cBoxUtilisateurs.gridy = 4;
		pnPartie.add(cBoxUtilisateurs, gbc_cBoxUtilisateurs);

		JLabel lblAjoutDescription = new JLabel("Description :");
		lblAjoutDescription.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblAjoutDescription.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblAjoutDescription = new GridBagConstraints();
		gbc_lblAjoutDescription.fill = GridBagConstraints.VERTICAL;
		gbc_lblAjoutDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblAjoutDescription.gridx = 2;
		gbc_lblAjoutDescription.gridy = 4;
		pnPartie.add(lblAjoutDescription, gbc_lblAjoutDescription);

		txtAjoutDescription = new JTextField();
		GridBagConstraints gbc_txtAjoutDescription = new GridBagConstraints();
		gbc_txtAjoutDescription.insets = new Insets(0, 0, 5, 0);
		gbc_txtAjoutDescription.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAjoutDescription.gridx = 3;
		gbc_txtAjoutDescription.gridy = 4;
		pnPartie.add(txtAjoutDescription, gbc_txtAjoutDescription);
		txtAjoutDescription.setColumns(10);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Boolean trouve = false;
				String utilisateurAAjouter = "";
				String utilisateurCourant = (String) cBoxUtilisateurs.getSelectedItem();
				String utilisateur;
				JSONObject detailsUtilisateurs;

				if (utilisateurCourant != null && utilisateurCourant.isEmpty()==false){
					/* on récupère l'utilise selectionner dans le menu déroulant */
					for (int i = 0; i < utilisateurs.size(); ++i) {

						JSONObject result = (JSONObject) utilisateurs.get(i);
						utilisateur = (String) result.get("prenom") + " " + result.get("nom") + " " + result.get("idUtilisateur");
						if (utilisateur.equals(utilisateurCourant)) {
							utilisateurAAjouter = (String) result.get("idUtilisateur");
						}
					}

					/* on ajoute l'utilisateur à la partie */
					for(int i = 0; i < mesParticipants.listeDesParticipants(idSalon).size(); ++i) {

						JSONObject users = (JSONObject) mesParticipants.listeDesParticipants(idSalon).get(i);
						String user = (String) users.get("Utilisateur_idUtilisateur");
						if (!utilisateurAAjouter.equals(user) ) {
							trouve = false;
						}
						else {
							trouve = true;
							break;
						}
					}
					
					if(trouve == false) {
						monApi.ajouterParticipant(idSalon, utilisateurAAjouter);

						/* on récupère les informations (nom, prenom) de l'utilisateur selectionner */
						detailsUtilisateurs = mapartie.detailsUser(utilisateurAAjouter);
						modeltabUtilisateurs.addRow(new Object[]{(String) detailsUtilisateurs.get("idUtilisateur"), (String) detailsUtilisateurs.get("nom"), detailsUtilisateurs.get("prenom")});
						cBoxUtilisateurs.setSelectedItem(null);
					}
					else {
						JOptionPane popErreurAjout = new JOptionPane();
						popErreurAjout.showMessageDialog(null, "Impossible d'ajouter un utilisateur déjà participant a la partie", "Erreur", popErreurAjout.INFORMATION_MESSAGE);
						cBoxUtilisateurs.setSelectedItem(null);
					}



				} // fin du mouse click ajout utilisateur
			}
		});
		GridBagConstraints gbc_btnAjouter = new GridBagConstraints();
		gbc_btnAjouter.anchor = GridBagConstraints.EAST;
		gbc_btnAjouter.insets = new Insets(0, 0, 5, 5);
		gbc_btnAjouter.gridx = 1;
		gbc_btnAjouter.gridy = 5;
		pnPartie.add(btnAjouter, gbc_btnAjouter);

		JButton btnAjoutTache = new JButton("Ajouter la tâche");
		GridBagConstraints gbc_btnAjoutTache = new GridBagConstraints();
		gbc_btnAjoutTache.anchor = GridBagConstraints.EAST;
		gbc_btnAjoutTache.insets = new Insets(0, 0, 5, 0);
		gbc_btnAjoutTache.gridx = 3;
		gbc_btnAjoutTache.gridy = 5;
		pnPartie.add(btnAjoutTache, gbc_btnAjoutTache);
		btnAjoutTache.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				JSONObject detailTache;
				JSONArray detailsTache;
				if(txtAjoutNom.getText().isEmpty()==false) {
					if ( txtAjoutDescription.getText().isEmpty() == true) {
						JSONObject tache = monApi.ajouterTache(txtAjoutNom.getText(),null, idSalon);
						modeltabTaches.addRow(new Object[]{(String) tache.get("idBacklog"), (String) txtAjoutNom.getText(), ""});
					}
					else {
						JSONObject tache = monApi.ajouterTache(txtAjoutNom.getText(),txtAjoutDescription.getText(), idSalon);
						modeltabTaches.addRow(new Object[]{ (String) tache.get("idBacklog"), (String) txtAjoutNom.getText(), (String) txtAjoutDescription.getText()});
					}
				} // fin du mouse click ajout tache
				txtAjoutNom.setText("");
				txtAjoutDescription.setText("");
			}
		});

		JPanel pnTaches = new JPanel();
		GridBagConstraints gbc_pnTaches = new GridBagConstraints();
		gbc_pnTaches.fill = GridBagConstraints.BOTH;
		gbc_pnTaches.gridx = 1;
		gbc_pnTaches.gridy = 1;
		getContentPane().add(pnTaches, gbc_pnTaches);
		GridBagLayout gbl_pnTaches = new GridBagLayout();
		gbl_pnTaches.columnWidths = new int[]{135, 70, 0, 0, 0};
		gbl_pnTaches.rowHeights = new int[]{0, 0, 0, 0, 5, 0};
		gbl_pnTaches.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pnTaches.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnTaches.setLayout(gbl_pnTaches);

		JScrollPane scrolltabTache = new JScrollPane();
		GridBagConstraints gbc_scrolltabTache = new GridBagConstraints();
		gbc_scrolltabTache.gridwidth = 4;
		gbc_scrolltabTache.insets = new Insets(0, 0, 5, 0);
		gbc_scrolltabTache.fill = GridBagConstraints.BOTH;
		gbc_scrolltabTache.gridx = 0;
		gbc_scrolltabTache.gridy = 0;
		pnTaches.add(scrolltabTache, gbc_scrolltabTache);

		JLabel lblTacheASupprimer = new JLabel("ID de la tâche à supprimer : ");
		GridBagConstraints gbc_lblTacheASupprimer = new GridBagConstraints();
		gbc_lblTacheASupprimer.insets = new Insets(0, 0, 5, 5);
		gbc_lblTacheASupprimer.gridx = 0;
		gbc_lblTacheASupprimer.gridy = 1;
		pnTaches.add(lblTacheASupprimer, gbc_lblTacheASupprimer);

		/* Text Area des taches a supprimer */
		txtTachesSupprimer = new JTextField();
		GridBagConstraints gbc_txtTachesSupprimer = new GridBagConstraints();
		gbc_txtTachesSupprimer.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTachesSupprimer.insets = new Insets(0, 0, 5, 5);
		gbc_txtTachesSupprimer.gridx = 1;
		gbc_txtTachesSupprimer.gridy = 1;
		pnTaches.add(txtTachesSupprimer, gbc_txtTachesSupprimer);
		txtTachesSupprimer.setColumns(10);

		modeltabTaches = new modelTabTaches();
		//Object[][] Ligne = new Object[][]{};
		tabTache = new JTable();
		tabTache.setFillsViewportHeight(true);

		tabTache.setModel(modeltabTaches);
		modeltabTaches.addColumn("ID");
		modeltabTaches.addColumn("Nom");
		modeltabTaches.addColumn("Description");

		scrolltabTache.setViewportView(tabTache);

		JButton btnSupprimerTache = new JButton("Supprimer");
		btnSupprimerTache.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				Boolean trouve = false;
				String idTacheASupprimer = txtTachesSupprimer.getText();
				if (idTacheASupprimer != "") {
					for(i=0;i<modeltabTaches.getRowCount();++i) {
						String tache = (String) modeltabTaches.getValueAt(i, 0);
						if(tache.equals(idTacheASupprimer) == false) {
							trouve = false;
						}
						else {
							trouve = true;
							break;
						}
					}
					if(trouve == true) {
						monApi.supprimerTache(idTacheASupprimer);
						supprimerTacheTableau(idTacheASupprimer);
						txtTachesSupprimer.setText("");
					}
					else {
						JOptionPane popErreurSupprimer = new JOptionPane();
						popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer une tâche qui n'existe pas", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
						cBoxUtilisateurs.setSelectedItem(null);
					}
				}
			}
		});
		GridBagConstraints gbc_btnSupprimerTache = new GridBagConstraints();
		gbc_btnSupprimerTache.insets = new Insets(0, 0, 5, 5);
		gbc_btnSupprimerTache.gridx = 2;
		gbc_btnSupprimerTache.gridy = 1;
		pnTaches.add(btnSupprimerTache, gbc_btnSupprimerTache);

		mettreTacheDansTableau();

		JButton btnMettreAJour = new JButton("Mettre à jour");
		btnMettreAJour.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i;
				int row = -1;
				//JSONArray Taches = mapartie.recupererBackLog(idSalon);
				for(i=0;i<=tabUtilisateurs.getRowCount();++i) {
					String nomT = (String) tabTache.getValueAt(i, 1);
					String descriptionT = (String) tabTache.getValueAt(i, 2);
					String idT = (String) tabTache.getValueAt(i, 0);
					monApi.majTache(nomT, descriptionT, idT);
				}
			}
		});
		GridBagConstraints gbc_btnMettreAJour = new GridBagConstraints();
		gbc_btnMettreAJour.insets = new Insets(0, 0, 5, 0);
		gbc_btnMettreAJour.gridx = 3;
		gbc_btnMettreAJour.gridy = 1;
		pnTaches.add(btnMettreAJour, gbc_btnMettreAJour);

		JSeparator separator_3 = new JSeparator();
		GridBagConstraints gbc_separator_3 = new GridBagConstraints();
		gbc_separator_3.gridwidth = 4;
		gbc_separator_3.insets = new Insets(0, 0, 5, 0);
		gbc_separator_3.gridx = 0;
		gbc_separator_3.gridy = 2;
		pnTaches.add(separator_3, gbc_separator_3);
		
				JButton btnTerminer = new JButton("Terminer la partie");
				btnTerminer.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						monApi.fermerSalon(idSalon);
					}
				});
				GridBagConstraints gbc_btnTerminer = new GridBagConstraints();
				gbc_btnTerminer.insets = new Insets(0, 0, 5, 5);
				gbc_btnTerminer.gridx = 0;
				gbc_btnTerminer.gridy = 3;
				pnTaches.add(btnTerminer, gbc_btnTerminer);
		
				JButton btnSupprimerSalon = new JButton("Supprimer le salon");
				btnSupprimerSalon.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						JOptionPane jop = new JOptionPane();
						Boolean estTermine = false;
						int option = jop.showConfirmDialog(null, "Êtes vous sûr de vouloir supprimer ce salon ?", "Suppression du salon", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						String etat = (String) getMapartie().detailSalon(idSalon).get("partieDemarrer");
						
						if(etat != null && etat.equals("2") && option == jop.YES_OPTION) {
							monApi.supprimerSalon(idSalon);
							dispose();
							Choixconnect C =new Choixconnect(mapartie);
							C.setVisible(true);
						}
						else {
							JOptionPane popErreurSupprimer = new JOptionPane();
							popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer une partie qui n'est pas terminée", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
							}
						
						}
					});
				GridBagConstraints gbc_btnSupprimerSalon = new GridBagConstraints();
				gbc_btnSupprimerSalon.insets = new Insets(0, 0, 5, 5);
				gbc_btnSupprimerSalon.gridx = 2;
				gbc_btnSupprimerSalon.gridy = 3;
				pnTaches.add(btnSupprimerSalon, gbc_btnSupprimerSalon);

		JSeparator separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 4;
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 4;
		pnTaches.add(separator, gbc_separator);

	}

	/**
	 * Permet de mettre les utilisateurs dans une combo box
	 */
	private void mettreUtilisateurDansComboBox() {
		// TODO Auto-generated method stub
		int i, j;
		String utilisateur;
		utilisateurs = monApi.listeUtilisateurs();
		for (i = 0; i < utilisateurs.size(); ++i) {

			JSONObject result = (JSONObject) utilisateurs.get(i);
			utilisateur = (String) result.get("prenom") + " " + result.get("nom") + " " + result.get("idUtilisateur");
			cBoxUtilisateurs.addItem(utilisateur);
			cBoxUtilisateurs.setSelectedItem(null);
		}
	}


	/**
	 * Permet de mettre les utilisateurs dans une combo box.
	 */
	private void mettreUtilisateurDansComboBox2() {
		// TODO Auto-generated method stub
		int i;
		String utilisateur;
		utilisateurs = monApi.listeUtilisateurs();
		for (i = 0; i < utilisateurs.size(); ++i) {
			JSONObject result = (JSONObject) utilisateurs.get(i);
			utilisateur = (String) result.get("prenom") + " " + result.get("nom") + " " + result.get("idUtilisateur");
			cBoxUtilisateurs.addItem(utilisateur);
			cBoxUtilisateurs.setSelectedItem(null);
		}
	}

	/**
	 * Permet de rendre des cellules non éditables.
	 */
	private void rendreCellulesNonEditables() {
		for(int i=0;i<tabUtilisateurs.getRowCount();++i) {
			tabUtilisateurs.isCellEditable(i, 0);
		}
	}

	/**
	 * Permet de mettre les utilisateurs dans un tableau.
	 */
	private void mettreUtilisateurDansTableau() {
		/* Affichage des participants dans le tableau */
		JSONObject detailUsers;
		JSONObject User;
		JSONArray Users = mesParticipants.listeDesParticipants(idSalon);

		for(int j=0;j<Users.size();++j){
			User = (JSONObject) Users.get(j);
			detailUsers = getMapartie().detailsUser((String) User.get("Utilisateur_idUtilisateur"));
			modeltabUtilisateurs.addRow(new Object[] {(String) User.get("Utilisateur_idUtilisateur"), (String) detailUsers.get("nom"), (String) detailUsers.get("prenom")});
		}
		/* Fin des affichages */
	}

	/**
	 * Mettre les backlogs du salon dans un tableau.
	 */
	private void mettreTacheDansTableau() {
		/* Affichage des participants dans le tableau */
		JSONObject detailTache;
		JSONObject Tache;
		JSONArray Taches = getMapartie().recupererBackLog(idSalon);

		for(int j=0;j<Taches.size();++j){
			Tache = (JSONObject) Taches.get(j);
			detailTache = getMapartie().detailSalon((String) Tache.get("Salon_idSalon"));
			modeltabTaches.addRow(new Object[] {(String) Tache.get("idBacklog"), (String) Tache.get("tache"), (String) Tache.get("description")});
		}
		/* Fin des affichages */
	}

	/**
	 * Permet de supprimer un utilisateur dans le tableau.
	 * 
	 * @param idUtilisateur l'id de l'utilisateur.
	 */
	private void supprimerUtilisateurTableau(String idUtilisateur) {
		int row = -1;//index of row or -1 if not found

		//search for the row based on the ID in the first column
		for(int i=0;i<modeltabUtilisateurs.getRowCount();++i)
			if(modeltabUtilisateurs.getValueAt(i, 0).equals(idUtilisateur))
			{
				row = i;
				break;
			}

		if(row != -1)
			modeltabUtilisateurs.removeRow(row);//remove row
	}

	/**
	 * Permet de supprimer une tâche du tableau.
	 * 
	 * @param idTache l'id du backlog à supprimer.
	 */
	private void supprimerTacheTableau(String idTache) {
		int row = -1;//index of row or -1 if not found

		//search for the row based on the ID in the first column
		for(int i=0;i<modeltabTaches.getRowCount();++i)
			if(modeltabTaches.getValueAt(i, 0).equals(idTache))
			{
				row = i;
				break;
			}

		if(row != -1)
			modeltabTaches.removeRow(row);//remove row
	}


	public Partie getMapartie() {
		return mapartie;
	}


	public void setMapartie(Partie mapartie) {
		this.mapartie = mapartie;
	}


}
