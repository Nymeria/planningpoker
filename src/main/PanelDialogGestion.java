package main;

import javax.swing.JPanel;

import api.Partie;


import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Cette classe implémente le panel permettant d'afficher les
 * boîtes de dialogue lors de la gestion de salon.
 */
public class PanelDialogGestion extends JPanel{

	private Partie maPartie;
	private JSONArray resOwner;
	private String salonOwner;
	private PannelgestionSalon panelGestion;

	/**
	 * Crée le Panel du dialogue
	 * 
	 * @param maPartie la partie en cours.
	 */
	public PanelDialogGestion(final Partie maPartie){	

		this.maPartie = maPartie;
		this.resOwner = maPartie.salonByOwner();

		this.setSize(460,150);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{450, 0};
		gridBagLayout.rowHeights = new int[]{19, 41, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);

		JLabel lblVeuillezChoisirUn = new JLabel("Veuillez choisir un salon à gérer :");
		GridBagConstraints gbc_lblVeuillezChoisirUn = new GridBagConstraints();
		gbc_lblVeuillezChoisirUn.fill = GridBagConstraints.VERTICAL;
		gbc_lblVeuillezChoisirUn.insets = new Insets(0, 0, 5, 0);
		gbc_lblVeuillezChoisirUn.gridx = 0;
		gbc_lblVeuillezChoisirUn.gridy = 1;
		this.add(lblVeuillezChoisirUn, gbc_lblVeuillezChoisirUn);

		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.BOTH;
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 2;

		for (int i=0; i<resOwner.size();++i) {

			JSONObject result =  (JSONObject) resOwner.get(i);
			salonOwner = (String) result.get("nom_salon");
			comboBox.addItem(salonOwner);
			comboBox.setSelectedItem(null);
		}
		this.add(comboBox, gbc_comboBox);

		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JDialog jd = new JDialog();
				PannelgestionSalon panelGest = new PannelgestionSalon(maPartie, salonOwner);
				jd.getContentPane().add(panelGest);
				jd.setVisible(true);
				jd.setSize(800, 600);
				jd.setResizable(false);
			}
		});
		GridBagConstraints gbc_btnValider = new GridBagConstraints();
		gbc_btnValider.insets = new Insets(0, 0, 5, 0);
		gbc_btnValider.fill = GridBagConstraints.BOTH;
		gbc_btnValider.gridx = 0;
		gbc_btnValider.gridy = 4;
		this.add(btnValider, gbc_btnValider);


	}

	/**
	 * Permet de récupérer le Panel du dialogue.
	 *  
	 * @return le Panel du dialogue.
	 */
	PannelgestionSalon getPanel(){
		return this.panelGestion;
	}

}
