package main;

import javax.swing.table.DefaultTableModel;

/**
 * Cette classe implémente le modèle pour le tableau des utilisateurs.
 */
public class modelTabUtilisateurs extends DefaultTableModel {

	@Override
	public boolean isCellEditable(int RowIndex, int columnIndex) {
		return false;
	}

}