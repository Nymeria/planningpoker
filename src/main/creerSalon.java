package main;

import api.Participant;
import api.Partie;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

/**
 * Cette classe implémente la fenêtre de création de fenêtre.
 */
public class creerSalon extends JFrame {

	private boolean iceScrum;
	private JPanel contentPane;
	private JTextField textFielddescrption;
	private JTextField textField_nomdetache;
	private JTable Tableparti;
	private JTable tableTache;
	private JTextField txtUrl;
	private JTextField txtNom;
	private JTextField txtSprint;
	private api.ScrumMaster monApi;
	private Participant mesParticipants;
	private Partie mapartie;
	//public static final String URL = "http://localhost";
	public static String idSalon = ""; // a changer
	JSONArray utilisateurs;
	JSONArray taches;
	DefaultTableModel model;
	DefaultTableModel modelTache;
	private JTextField txtUtilisateurASupprimer;
	private JTextField txtTacheASupprimer;

	/**
	 * crée la fenêtre.
	 *
	 * @param mapartie
	 */
	public creerSalon(final Partie mapartie) {
		iceScrum=false;
		monApi = new api.ScrumMaster(mapartie.url);
		monApi.is_logged = true;
		monApi.idUser = mapartie.idUser;
		mesParticipants = new Participant(mapartie.url);
		this.setMapartie(mapartie);

		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addWindowListener(new WindowCloserCreerSalon(this));
		setBounds(100, 100, 1243, 726);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 333, 0, 0};
		gbl_contentPane.rowHeights = new int[]{102, 52, 72, -54, 109, 71, 80, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0};
		contentPane.setLayout(gbl_contentPane);

		final JTextPane txtpnParticipants = new JTextPane();
		txtpnParticipants.setEnabled(false);
		txtpnParticipants.setMinimumSize(new Dimension(90, 22));
		txtpnParticipants.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnParticipants.setEditable(false);
		txtpnParticipants.setText("Participants :");
		txtpnParticipants.setPreferredSize(new Dimension(80, 22));
		GridBagConstraints gbc_txtpnParticipants = new GridBagConstraints();
		gbc_txtpnParticipants.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnParticipants.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnParticipants.gridx = 1;
		gbc_txtpnParticipants.gridy = 0;
		contentPane.add(txtpnParticipants, gbc_txtpnParticipants);

		final JTextPane txtpnNomDeLa = new JTextPane();
		txtpnNomDeLa.setEnabled(false);
		txtpnNomDeLa.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnNomDeLa.setMinimumSize(new Dimension(120, 22));
		txtpnNomDeLa.setToolTipText("");
		txtpnNomDeLa.setPreferredSize(new Dimension(200, 22));
		txtpnNomDeLa.setText("Nom de la tâche :");
		txtpnNomDeLa.setEditable(false);
		GridBagConstraints gbc_txtpnNomDeLa = new GridBagConstraints();
		gbc_txtpnNomDeLa.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnNomDeLa.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnNomDeLa.gridx = 2;
		gbc_txtpnNomDeLa.gridy = 0;
		contentPane.add(txtpnNomDeLa, gbc_txtpnNomDeLa);

		/* 
		 * Combo Box de la liste des participants
		 * 
		 * 
		 */
		final JComboBox listeparticipants = new JComboBox();
		listeparticipants.setEnabled(false);
		GridBagConstraints gbc_listeparticipants = new GridBagConstraints();
		gbc_listeparticipants.anchor = GridBagConstraints.NORTH;
		gbc_listeparticipants.insets = new Insets(0, 0, 5, 5);
		gbc_listeparticipants.fill = GridBagConstraints.HORIZONTAL;
		gbc_listeparticipants.gridx = 1;
		gbc_listeparticipants.gridy = 1;
		contentPane.add(listeparticipants, gbc_listeparticipants);
		int i;

		String utilisateur;
		utilisateurs = monApi.listeUtilisateurs();
		for (i = 0; i < utilisateurs.size(); ++i) {

			JSONObject result = (JSONObject) utilisateurs.get(i);
			utilisateur = (String) result.get("prenom") + " " + result.get("nom") + " " + result.get("idUtilisateur");
			listeparticipants.addItem(utilisateur);
			listeparticipants.setSelectedItem(null);
		}

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 5;
		contentPane.add(scrollPane, gbc_scrollPane);

		model = new modelTabUtilisateurs();
		Object[][] Ligne = new Object[][]{};
		Tableparti = new JTable();
		Tableparti.setFillsViewportHeight(true);

		Tableparti.setModel(model);
		model.addColumn("ID");
		model.addColumn("Nom");
		model.addColumn("Prénom");

		scrollPane.setViewportView(Tableparti);

		final JButton btnAjouterparti = new JButton("Ajouter");
		btnAjouterparti.setEnabled(false);
		btnAjouterparti.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				Boolean trouve = false;
				String utilisateurAAjouter = "";
				String utilisateurCourant = (String) listeparticipants.getSelectedItem();
				String utilisateur;
				JSONObject detailsUtilisateurs;

				if (!utilisateurCourant.equals("") && utilisateurCourant.isEmpty()==false){
					/* on récupère l'utilise selectionner dans le menu déroulant */
					for (int i = 0; i < utilisateurs.size(); ++i) {

						JSONObject result = (JSONObject) utilisateurs.get(i);
						utilisateur = (String) result.get("prenom") + " " + result.get("nom") + " " + result.get("idUtilisateur");
						if (utilisateur.equals(utilisateurCourant)) {
							utilisateurAAjouter = (String) result.get("idUtilisateur");
						}
					}

					/* on ajoute l'utilisateur à la partie */
					for(int i = 0; i < mesParticipants.listeDesParticipants(idSalon).size(); ++i) {
						JSONObject users = (JSONObject) mesParticipants.listeDesParticipants(idSalon).get(i);
						String user = (String) users.get("Utilisateur_idUtilisateur");
						if (!(utilisateurAAjouter.equals(user)) ) {
							trouve = false;
							//break;
						}
						else {
							trouve = true;
							break;
						}
					}
					
					if(trouve == false) {
						monApi.ajouterParticipant(idSalon, utilisateurAAjouter);

						/* on récupère les informations (nom, prenom) de l'utilisateur selectionner */
						detailsUtilisateurs = mapartie.detailsUser(utilisateurAAjouter);
						model.addRow(new Object[]{(String) detailsUtilisateurs.get("idUtilisateur"), (String) detailsUtilisateurs.get("nom"), detailsUtilisateurs.get("prenom")});
						listeparticipants.setSelectedItem(null);
					}
					else {
						JOptionPane popErreurAjout = new JOptionPane();
						popErreurAjout.showMessageDialog(null, "Impossible d'ajouter un utilisateur déjà participant a la partie", "Erreur", popErreurAjout.INFORMATION_MESSAGE);
						listeparticipants.setSelectedItem(null);
					}



				} // fin du mouse click ajout utilisateur
			}
		});
		GridBagConstraints gbc_btnAjouterparti = new GridBagConstraints();
		gbc_btnAjouterparti.gridheight = 2;
		gbc_btnAjouterparti.insets = new Insets(0, 0, 5, 5);
		gbc_btnAjouterparti.gridx = 1;
		gbc_btnAjouterparti.gridy = 2;
		contentPane.add(btnAjouterparti, gbc_btnAjouterparti);

		final JTextPane txtpnDescription = new JTextPane();
		txtpnDescription.setEditable(false);
		txtpnDescription.setEnabled(false);
		txtpnDescription.setMinimumSize(new Dimension(90, 22));
		txtpnDescription.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnDescription.setText("Description :");
		GridBagConstraints gbc_txtpnDescription = new GridBagConstraints();
		gbc_txtpnDescription.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnDescription.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnDescription.gridx = 2;
		gbc_txtpnDescription.gridy = 2;
		contentPane.add(txtpnDescription, gbc_txtpnDescription);

		/* Gestion des taches : Création et récupération depuis IceScrum */
		textField_nomdetache = new JTextField();
		GridBagConstraints gbc_textField_nomdetache = new GridBagConstraints();
		gbc_textField_nomdetache.insets = new Insets(0, 0, 5, 0);
		gbc_textField_nomdetache.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_nomdetache.gridx = 2;
		gbc_textField_nomdetache.gridy = 1;
		contentPane.add(textField_nomdetache, gbc_textField_nomdetache);
		textField_nomdetache.setColumns(10);

		textFielddescrption = new JTextField();
		GridBagConstraints gbc_textFielddescrption = new GridBagConstraints();
		gbc_textFielddescrption.insets = new Insets(0, 0, 5, 0);
		gbc_textFielddescrption.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFielddescrption.gridx = 2;
		gbc_textFielddescrption.gridy = 3;
		contentPane.add(textFielddescrption, gbc_textFielddescrption);
		textFielddescrption.setColumns(10);

		modelTache = new modelTabTaches();
		tableTache = new JTable();
		tableTache.setFillsViewportHeight(true);
		tableTache.setModel(modelTache);
		modelTache.addColumn("ID");
		modelTache.addColumn("Nom");
		modelTache.addColumn("Description");



		final JButton btnajoutertache = new JButton("Ajouter");
		btnajoutertache.setEnabled(false);
		GridBagConstraints gbc_btnajoutertache = new GridBagConstraints();
		gbc_btnajoutertache.insets = new Insets(0, 0, 5, 0);
		gbc_btnajoutertache.gridx = 2;
		gbc_btnajoutertache.gridy = 4;
		contentPane.add(btnajoutertache, gbc_btnajoutertache);
		btnajoutertache.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				JSONObject detailTache;
				JSONArray detailsTache;
				if(textField_nomdetache.getText().isEmpty()==false) {
					if ( textFielddescrption.getText().isEmpty() == true) {
						JSONObject tache = monApi.ajouterTache(textField_nomdetache.getText(),null, idSalon);
						modelTache.addRow(new Object[]{(String) tache.get("idBacklog"), (String) textField_nomdetache.getText(), ""});
					}
					else {
						JSONObject tache = monApi.ajouterTache(textField_nomdetache.getText(),textFielddescrption.getText(), idSalon);
						modelTache.addRow(new Object[]{ (String) tache.get("idBacklog"), (String) textField_nomdetache.getText(), (String) textFielddescrption.getText()});
					}
				} // fin du mouse click ajout tache
				textField_nomdetache.setText("");
				textFielddescrption.setText("");
			}
		});

		/**
		 * *****************************************************************
		 */
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 8;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{50, 0};
		gbl_panel.rowHeights = new int[]{25, 22, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);

		final JTextPane txtpnNom = new JTextPane();
		txtpnNom.setText("Nom :");
		txtpnNom.setPreferredSize(new Dimension(50, 22));
		txtpnNom.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnNom.setEditable(false);
		GridBagConstraints gbc_txtpnNom = new GridBagConstraints();
		gbc_txtpnNom.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnNom.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnNom.gridx = 0;
		gbc_txtpnNom.gridy = 0;
		panel.add(txtpnNom, gbc_txtpnNom);

		txtNom = new JTextField();
		txtNom.setColumns(10);
		GridBagConstraints gbc_txtNom = new GridBagConstraints();
		gbc_txtNom.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNom.anchor = GridBagConstraints.NORTH;
		gbc_txtNom.insets = new Insets(0, 0, 5, 0);
		gbc_txtNom.gridx = 0;
		gbc_txtNom.gridy = 1;
		panel.add(txtNom, gbc_txtNom);

		final JCheckBox checkBox = new JCheckBox("Interface avec IceScrum");

		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.fill = GridBagConstraints.VERTICAL;
		gbc_checkBox.insets = new Insets(0, 0, 5, 0);
		gbc_checkBox.gridx = 0;
		gbc_checkBox.gridy = 2;
		panel.add(checkBox, gbc_checkBox);

		final JTextPane txtpnUrl = new JTextPane();
		txtpnUrl.setEnabled(false);
		txtpnUrl.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnUrl.setText("URL :");
		txtpnUrl.setPreferredSize(new Dimension(50, 22));
		txtpnUrl.setEditable(false);
		GridBagConstraints gbc_txtpnUrl = new GridBagConstraints();
		gbc_txtpnUrl.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnUrl.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnUrl.gridx = 0;
		gbc_txtpnUrl.gridy = 3;
		panel.add(txtpnUrl, gbc_txtpnUrl);

		txtUrl = new JTextField();
		txtUrl.setText("http://localhost:8080/icescrum/ws/p/PROJ1/");
		txtUrl.setEnabled(false);
		txtUrl.setColumns(10);
		GridBagConstraints gbc_txtUrl = new GridBagConstraints();
		gbc_txtUrl.insets = new Insets(0, 0, 5, 0);
		gbc_txtUrl.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUrl.anchor = GridBagConstraints.NORTH;
		gbc_txtUrl.gridx = 0;
		gbc_txtUrl.gridy = 4;
		panel.add(txtUrl, gbc_txtUrl);

		final JTextPane txtpnNSprint = new JTextPane();
		txtpnNSprint.setEnabled(false);
		txtpnNSprint.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtpnNSprint.setText("N° Sprint :");
		GridBagConstraints gbc_txtpnNSprint = new GridBagConstraints();
		gbc_txtpnNSprint.anchor = GridBagConstraints.SOUTH;
		gbc_txtpnNSprint.insets = new Insets(0, 0, 5, 0);
		gbc_txtpnNSprint.gridx = 0;
		gbc_txtpnNSprint.gridy = 5;
		panel.add(txtpnNSprint, gbc_txtpnNSprint);

		txtSprint = new JTextField();
		txtSprint.setEnabled(false);
		GridBagConstraints gbc_txtSprint = new GridBagConstraints();
		gbc_txtSprint.anchor = GridBagConstraints.NORTH;
		gbc_txtSprint.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSprint.gridx = 0;
		gbc_txtSprint.gridy = 6;
		panel.add(txtSprint, gbc_txtSprint);
		txtSprint.setColumns(10);

		final JButton btnMAJ = new JButton("Mettre à jour");
		btnMAJ.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i;
				int row = -1;
				for(i=0;i<=Tableparti.getRowCount();++i) {
					String nomT = (String) tableTache.getValueAt(i, 1);
					String descriptionT = (String) tableTache.getValueAt(i, 2);
					String idT = (String) tableTache.getValueAt(i, 0);
					monApi.majTache(nomT, descriptionT, idT);
				}
			}
		});
		btnMAJ.setEnabled(false);
		GridBagConstraints gbc_btnMAJ = new GridBagConstraints();
		gbc_btnMAJ.insets = new Insets(0, 0, 5, 0);
		gbc_btnMAJ.gridx = 2;
		gbc_btnMAJ.gridy = 6;
		contentPane.add(btnMAJ, gbc_btnMAJ);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 7;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);

		JLabel lblSupprimerUtilisateur = new JLabel("ID de l'utilisateur a supprimer :");
		GridBagConstraints gbc_lblSupprimerUtilisateur = new GridBagConstraints();
		gbc_lblSupprimerUtilisateur.insets = new Insets(0, 0, 0, 5);
		gbc_lblSupprimerUtilisateur.anchor = GridBagConstraints.EAST;
		gbc_lblSupprimerUtilisateur.gridx = 0;
		gbc_lblSupprimerUtilisateur.gridy = 0;
		panel_1.add(lblSupprimerUtilisateur, gbc_lblSupprimerUtilisateur);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 2;
		gbc_panel_2.gridy = 8;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);

		final JButton btnTerminer = new JButton("Terminer");
		btnTerminer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
				Choixconnect C =new Choixconnect(mapartie);
				C.setVisible(true);
			}
		});

		JLabel lblTacheASupprimer = new JLabel("ID de la tâche a supprimer :");
		GridBagConstraints gbc_lblTacheASupprimer = new GridBagConstraints();
		gbc_lblTacheASupprimer.insets = new Insets(0, 0, 0, 5);
		gbc_lblTacheASupprimer.anchor = GridBagConstraints.EAST;
		gbc_lblTacheASupprimer.gridx = 0;
		gbc_lblTacheASupprimer.gridy = 0;
		panel_2.add(lblTacheASupprimer, gbc_lblTacheASupprimer);

		txtTacheASupprimer = new JTextField();
		txtTacheASupprimer.setEnabled(false);
		GridBagConstraints gbc_txtTacheASupprimer = new GridBagConstraints();
		gbc_txtTacheASupprimer.insets = new Insets(0, 0, 0, 5);
		gbc_txtTacheASupprimer.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTacheASupprimer.gridx = 1;
		gbc_txtTacheASupprimer.gridy = 0;
		panel_2.add(txtTacheASupprimer, gbc_txtTacheASupprimer);
		txtTacheASupprimer.setColumns(10);

		final JButton btnTacheASupprimer = new JButton("Supprimer");
		btnTacheASupprimer.setEnabled(false);
		btnTacheASupprimer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				Boolean trouve = false;
				String idTacheASupprimer = txtTacheASupprimer.getText();
				if (idTacheASupprimer != "") {
					for(i=0;i<modelTache.getRowCount();++i) {
						String tache = (String) modelTache.getValueAt(i, 0);
						if(tache.equals(idTacheASupprimer) == false) {
							trouve = false;
						}
						else {
							trouve = true;
							break;
						}
					}
					if(trouve == true) {
						monApi.supprimerTache(idTacheASupprimer);
						supprimerTacheTableau(idTacheASupprimer);
						txtTacheASupprimer.setText("");
					}
					else {
						JOptionPane popErreurSupprimer = new JOptionPane();
						popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer une tâche qui n'existe pas", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
						txtTacheASupprimer.setText("");
					}
				}
			}
		});
		GridBagConstraints gbc_btnTacheASupprimer = new GridBagConstraints();
		gbc_btnTacheASupprimer.gridx = 2;
		gbc_btnTacheASupprimer.gridy = 0;
		panel_2.add(btnTacheASupprimer, gbc_btnTacheASupprimer);
		btnTerminer.setEnabled(false);
		GridBagConstraints gbc_btnTerminer = new GridBagConstraints();
		gbc_btnTerminer.gridx = 2;
		gbc_btnTerminer.gridy = 9;
		contentPane.add(btnTerminer, gbc_btnTerminer);

		txtUtilisateurASupprimer = new JTextField();
		txtUtilisateurASupprimer.setEnabled(false);
		GridBagConstraints gbc_txtUtilisateurASupprimer = new GridBagConstraints();
		gbc_txtUtilisateurASupprimer.insets = new Insets(0, 0, 0, 5);
		gbc_txtUtilisateurASupprimer.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUtilisateurASupprimer.gridx = 1;
		gbc_txtUtilisateurASupprimer.gridy = 0;
		panel_1.add(txtUtilisateurASupprimer, gbc_txtUtilisateurASupprimer);
		txtUtilisateurASupprimer.setColumns(10);

		final JButton btnSupprimerUtilisateur = new JButton("Supprimer");
		btnSupprimerUtilisateur.setEnabled(false);
		btnSupprimerUtilisateur.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int i;
				Boolean trouve = false;
				String idUserASupprimer = txtUtilisateurASupprimer.getText();
				if (idUserASupprimer != "") {
					for(i=0;i<model.getRowCount();++i) {
						String user = (String) model.getValueAt(i, 0);
						if(user.equals(idUserASupprimer) == false) {
							trouve = false;
						}
						else {
							trouve = true;
							break;
						}
					}
					if(trouve == true) {
						monApi.supprimerParticipant(idSalon, idUserASupprimer);
						supprimerUtilisateurTableau(idUserASupprimer);
						txtUtilisateurASupprimer.setText("");
					}
					else {
						JOptionPane popErreurSupprimer = new JOptionPane();
						popErreurSupprimer.showMessageDialog(null, "Impossible de supprimer un utilisateur qui ne participe pas à la partie", "Erreur", popErreurSupprimer.INFORMATION_MESSAGE);
						listeparticipants.setSelectedItem(null);
						txtUtilisateurASupprimer.setText("");
					}
				}
			}
		});
		GridBagConstraints gbc_btnSupprimerUtilisateur = new GridBagConstraints();
		gbc_btnSupprimerUtilisateur.gridx = 2;
		gbc_btnSupprimerUtilisateur.gridy = 0;
		panel_1.add(btnSupprimerUtilisateur, gbc_btnSupprimerUtilisateur);

		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 2;
		gbc_scrollPane_1.gridy = 7;
		contentPane.add(scrollPane_1, gbc_scrollPane_1);

		scrollPane_1.setViewportView(tableTache); 



		final JButton btnCrerPartie = new JButton("Créer Partie");
		btnCrerPartie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(txtNom.getText().isEmpty()==false) {
					String nom_salon = txtNom.getText();

					String url_salon = "";
					if (!(txtUrl.getText().equals(""))) {
						url_salon = txtUrl.getText();

					}
					/* a changer par l'idSprint */
					String idSprint = "";
					if (!(txtSprint.getText().equals(""))) {
						idSprint = txtSprint.getText();

					}

					JSONObject salon = monApi.creerNouveauSalon(nom_salon, url_salon, idSprint);
					idSalon = (String) salon.get("idSalon");
					monApi.ajouterParticipant(idSalon, mapartie.idUser);


					btnTerminer.setEnabled(true);
					txtpnDescription.setEnabled(true);
					btnAjouterparti.setEnabled(true);
					txtpnNomDeLa.setEnabled(true);
					txtpnParticipants.setEnabled(true);
					btnajoutertache.setEnabled(true);
					listeparticipants.setEnabled(true);
					btnMAJ.setEnabled(true);
					txtTacheASupprimer.setEnabled(true);
					txtUtilisateurASupprimer.setEnabled(true);
					btnSupprimerUtilisateur.setEnabled(true);
					btnTacheASupprimer.setEnabled(true);

					txtpnUrl.setEnabled(false);
					txtUrl.setEnabled(false);
					txtpnNSprint.setEnabled(false);
					txtSprint.setEnabled(false);
					checkBox.setEnabled(false);
					txtNom.setEnabled(false);
					txtpnNom.setEnabled(false);
					btnCrerPartie.setEnabled(false);
					mettreUtilisateurDansTableau();
				}
			}
		});
		GridBagConstraints gbc_btnCrerPartie = new GridBagConstraints();
		gbc_btnCrerPartie.anchor = GridBagConstraints.NORTH;
		gbc_btnCrerPartie.insets = new Insets(0, 0, 5, 5);
		gbc_btnCrerPartie.gridx = 0;
		gbc_btnCrerPartie.gridy = 8;
		contentPane.add(btnCrerPartie, gbc_btnCrerPartie);





		checkBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ((boolean) ((AbstractButton) e.getSource()).isSelected() == true) {
					iceScrum =true;
					txtpnUrl.setEnabled(true);
					txtUrl.setEnabled(true);
					txtpnNSprint.setEnabled(true);
					txtSprint.setEnabled(true);
					contentPane.repaint();
				} else {
					iceScrum=false;
					txtpnUrl.setEnabled(false);
					txtUrl.setEnabled(false);
					txtpnNSprint.setEnabled(false);
					txtSprint.setEnabled(false);
					contentPane.repaint();
				}
			}
		});
	}

	private void mettreUtilisateurDansTableau() {
		/* Affichage des participants dans le tableau */
		JSONObject detailUsers;
		JSONObject User;
		JSONArray Users = mesParticipants.listeDesParticipants(idSalon);

		for(int j=0;j<Users.size();++j){
			User = (JSONObject) Users.get(j);
			detailUsers = getMapartie().detailsUser((String) User.get("Utilisateur_idUtilisateur"));
			model.addRow(new Object[] {(String) User.get("Utilisateur_idUtilisateur"), (String) detailUsers.get("nom"), (String) detailUsers.get("prenom")});
		}
		/* Fin des affichages */
	}

	/**
	 * Permet de supprimer un utilisateur dans le tableau
	 * @param idUtilisateur
	 */
	private void supprimerUtilisateurTableau(String idUtilisateur) {
		int row = -1;//index of row or -1 if not found

		//search for the row based on the ID in the first column
		for(int i=0;i<model.getRowCount();++i)
			if(model.getValueAt(i, 0).equals(idUtilisateur))
			{
				row = i;
				break;
			}

		if(row != -1)
			model.removeRow(row);//remove row
	}

	private void supprimerTacheTableau(String idUtilisateur) {
		int row = -1;//index of row or -1 if not found

		//search for the row based on the ID in the first column
		for(int i=0;i<modelTache.getRowCount();++i)
			if(modelTache.getValueAt(i, 0).equals(idUtilisateur))
			{
				row = i;
				break;
			}

		if(row != -1)
			modelTache.removeRow(row);//remove row
	}

	public Partie getMapartie() {
		return mapartie;
	}

	public void setMapartie(Partie mapartie) {
		this.mapartie = mapartie;
	}
}


