package main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import api.Chat;
import api.ScrumMaster;
import thread.ThreadChat;
import thread.ThreadParticipant;

/**
 * Cette classe implémente la fermeture de la
 * fenêtre de jeu
 */
public class WindowCloser extends WindowAdapter {

	private TestFram f;
	private api.ScrumMaster monApi;
	private Chat chat;

	public WindowCloser(TestFram f) {
		this.f = f;
		monApi = new ScrumMaster(Interface.URL);
		monApi.idUser = Interface.getIdUtilisateur();
		monApi.is_logged = true;
		chat = new Chat(Interface.maPartie.url, f.idSalon);
	}

	@Override
	/**
	 * Action a effectuer lors de la fermeture du jeu. Si
	 * c'est le dernier participant, on met la partie en pause.
	 */
	public void windowClosing(WindowEvent e){
		if (ThreadParticipant.nbParticipantEnLigne()<=1)
		{
			monApi.mettrePartieEnPause(Interface.idSalon);
		}
		if (f.chat.btnEnvoi.isEnabled() ){
			chat.envoyerMessage("c'est déconnecter",Interface.maPartie.idUser);
			chat.envoyerMessage("j'ai fini d'argumenter",Interface.maPartie.idUser);
		}
		
		f.dispose();
		f.close();
	}
}