package main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import api.Partie;

/**
 * Cette classe permet de gérer la fermeture de la fenêtre de création de salon.
 */
public class WindowCloserCreerSalon extends WindowAdapter{

	private creerSalon f;
	private Partie mapartie;

	public WindowCloserCreerSalon(creerSalon f) {
		this.f = f;
		this.mapartie = f.getMapartie();
	}

	@Override
	/**
	 * Action à effectuer lors de la fermeture de la fenêtre : on renvoi
	 * vers le menu de choix de connection.
	 */
	public void windowClosing(WindowEvent e){

		f.dispose();
		try {
			Choixconnect frame = new Choixconnect(mapartie);
			frame.setVisible(true);
		} catch (Exception e1) {
		}
	}

}
