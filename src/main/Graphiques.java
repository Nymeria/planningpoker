package main;

import java.awt.Color;

import net.sourceforge.chart2d.Chart2D;
import net.sourceforge.chart2d.Chart2DProperties;
import net.sourceforge.chart2d.Dataset;
import net.sourceforge.chart2d.GraphChart2DProperties;
import net.sourceforge.chart2d.GraphProperties;
import net.sourceforge.chart2d.LBChart2D;
import net.sourceforge.chart2d.LegendProperties;
import net.sourceforge.chart2d.MultiColorsProperties;
import net.sourceforge.chart2d.Object2DProperties;
import net.sourceforge.chart2d.PieChart2D;
import net.sourceforge.chart2d.PieChart2DProperties;
import net.sourceforge.chart2d.WarningRegionProperties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import api.Statistique;

/**
 * Cette classe permet de générer différents graphiques à partirs
 * de JSONArray qui contiennent les statistiques générales et du joueur.
 */
public class Graphiques {

	Statistique stat;

	public Graphiques(Statistique stat){
		this.stat = stat;
	}

	/**
	 * Cette méthode crée un graphique en batons des estimations pour chaque backlog
	 * @param backlogs le JSONArray contant les données sur les backlogs.
	 * @return un graphique de type Chart2D
	 */
	public Chart2D GraphEstimationBacklogs(JSONArray backlogs) {

		// On crée le graphique d'estimation des backlogs
		Object2DProperties object2DProps = new Object2DProperties();
		object2DProps.setObjectTitleText ("Estimation par tâche");

		//On lui assigne une précision
		Chart2DProperties chart2DProps = new Chart2DProperties();
		chart2DProps.setChartDataLabelsPrecision (0);

		// On crée un label
		LegendProperties legendProps = new LegendProperties();
		String[] legendLabels = {"Estimation"};
		legendProps.setLegendLabelsTexts (legendLabels);

		// On édite les propriétés du graphique (nombre d'entrées, nom des axes)
		GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
		String[] labelsAxisLabels = new String[backlogs.size()];
		for(int i = 0; i < backlogs.size(); i++){
			labelsAxisLabels[i]=Integer.toString(i);
		}
		graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
		graphChart2DProps.setLabelsAxisTitleText ("Numéro de tâche");
		graphChart2DProps.setNumbersAxisTitleText ("Poids");

		GraphProperties graphProps = new GraphProperties();
		graphProps.setGraphOutlineComponentsExistence (true);

		// On crée la feuille de donnée puis on la rempli avec les estimations
		// de chaque backlog
		Dataset dataset = new Dataset (1, backlogs.size(), 1);

		for (int i = 0; i< backlogs.size(); i++){
			JSONObject back = (JSONObject) backlogs.get(i);
                        if (back.get("estimation") != null)
                            dataset.set(0,i,0,Integer.parseInt(back.get("estimation").toString())); 
		}

		MultiColorsProperties multiColorsProps = new MultiColorsProperties();

		// On génère les zones critiques (rouge et orange)
		WarningRegionProperties warningRegionProps1 = new WarningRegionProperties();
		warningRegionProps1.setHigh (WarningRegionProperties.HIGH);
		warningRegionProps1.setLow (20);

		WarningRegionProperties warningRegionProps2 = new WarningRegionProperties();
		warningRegionProps2.setHigh (20);
		warningRegionProps2.setLow (8);
		warningRegionProps2.setComponentColor (new Color (146, 105, 0));
		warningRegionProps2.setBackgroundColor (new Color (222, 209, 176));

		// On crée le graphique puis on vérifie qu'il est valide
		LBChart2D chart2D = new LBChart2D();
		chart2D.setObject2DProperties (object2DProps);
		chart2D.setChart2DProperties (chart2DProps);
		chart2D.setLegendProperties (legendProps);
		chart2D.setGraphChart2DProperties (graphChart2DProps);
		chart2D.addGraphProperties (graphProps);
		chart2D.addDataset (dataset);
		chart2D.addMultiColorsProperties (multiColorsProps);
		chart2D.addWarningRegionProperties (warningRegionProps1);
		chart2D.addWarningRegionProperties (warningRegionProps2);

		if (!chart2D.validate (false)) chart2D.validate (true);

		return chart2D;
	}

	/**
	 * Cette méthode crée un cammembert avec le pourcentage de temps du projet pour
	 * chaque backlog
	 *
	 * @param backlogs le JSONArray contant les données sur les backlogs.
	 * @return un graphique de type Chart2D
	 */
	public Chart2D PourcentageTemps(JSONArray backlogs){

		// On crée le graphique de pourcentage de temps par backlog
		Object2DProperties object2DProps = new Object2DProperties();
		object2DProps.setObjectTitleText ("Pourcentage de temps par tâche");

		// On lui assigne une précision
		Chart2DProperties chart2DProps = new Chart2DProperties();
		chart2DProps.setChartDataLabelsPrecision (-3);

		LegendProperties legendProps = new LegendProperties();

		// On crée les différents labels (backlogs)
		String[] legendLabels = new String[backlogs.size()];
		for(int i = 0; i < backlogs.size(); i++){
			legendLabels[i]=Integer.toString(i);
		}
		legendProps.setLegendLabelsTexts (legendLabels);

		// On crée la feuille de donnée puis on la rempli
		Dataset dataset = new Dataset (backlogs.size(), 1, 1);
		for (int i = 0; i< backlogs.size(); i++){
			JSONObject back = (JSONObject) backlogs.get(i);
                        if(back.get("estimation") != null)
			dataset.set(i,0,0,Integer.parseInt(back.get("estimation").toString())); 
		}

		// On gère les couleurs
		MultiColorsProperties multiColorsProps = new MultiColorsProperties();

		PieChart2DProperties pieChart2DProps = new PieChart2DProperties();

		// On crée le graphique et vérifions qu'il est valide
		PieChart2D chart2D = new PieChart2D();
		chart2D.setObject2DProperties (object2DProps);
		chart2D.setChart2DProperties (chart2DProps);
		chart2D.setLegendProperties (legendProps);
		chart2D.setDataset (dataset);
		chart2D.setMultiColorsProperties (multiColorsProps);
		chart2D.setPieChart2DProperties (pieChart2DProps);
		if (!chart2D.validate (false)) chart2D.validate (true);

		return chart2D;
	}

	/**
	 * Cette méthode crée un graphique avec deux entrées : le vote moyen
	 * et le vote du joueur
	 *
	 * @param tendance le JSONArray contant les données sur les votes moyens et du joueur.
         * @param nomBacklog
	 * @return un graphique de type Chart2D
	 */
	public Chart2D TendanceVoteTache(JSONArray tendance, String nomBacklog) {
		int taille;

		Object2DProperties object2DProps = new Object2DProperties();
		object2DProps.setObjectTitleText ("Tendance de vote de la tâche : " + nomBacklog);

		Chart2DProperties chart2DProps = new Chart2DProperties();

		LegendProperties legendProps = new LegendProperties();
		String[] legendLabels = {"Votre vote", "Vote moyen"};
		legendProps.setLegendLabelsTexts (legendLabels);
                
		JSONArray copie = new JSONArray();

		GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();

		if (tendance == null){
			taille = 1;
		}
		else{
			taille = tendance.size();
			JSONObject objetTendance;
			for (int i = 0; i<taille; i++){
				if (tendance != null){
					objetTendance = (JSONObject) tendance.get(i);
					if (objetTendance.get("vote") != null && !objetTendance.get("vote").equals("null")){
						copie.add(objetTendance);
					}
				}
			}
			taille = copie.size();
		}


			String[] labelsAxisLabels = new String[taille];

			for (int i = 0; i<taille; i++){
				labelsAxisLabels[i] = Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tour");
			graphChart2DProps.setNumbersAxisTitleText ("Estimation");
			graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);

			//Configure graph properties
			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphBarsExistence (false);
			graphProps.setGraphLinesExistence (true);
			graphProps.setGraphOutlineComponentsExistence (true);
			graphProps.setGraphAllowComponentAlignment (true);
			graphProps.setGraphDotsExistence (true);

			//Configure dataset
			Dataset dataset = new Dataset (2, taille, 1);
			JSONObject objetTendance;
			for (int i=0; i<taille; i++){
				if (tendance != null){
					objetTendance = (JSONObject) copie.get(i);
					if (objetTendance.get("vote") != null){
						dataset.set(0,  i, 0, Float.parseFloat((String) objetTendance.get("vote")));
					}
					if (objetTendance.get("valeur_moyenne") != null){
						dataset.set(1,  i, 0, Float.parseFloat((String) objetTendance.get("valeur_moyenne")));
					}
				}
			}

			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

		/**
		 * Cette méthode crée un graphique avec le nombre de tours nécessaires
		 * afin de convenir d'une valeur commune.
		 *
		 * @param stat les statistiques.
		 * @param backlogs le JSONArray contant les données sur les backlogs.
		 * @return un graphique de type Chart2D
		 */
		public Chart2D GraphNbToursBacklog(Statistique stat, JSONArray backlogs) {

			// On crée le graphique d'estimation des backlogs
			Object2DProperties object2DProps = new Object2DProperties();
			object2DProps.setObjectTitleText ("Nombre de tours par tâche");

			//On lui assigne une précision
			Chart2DProperties chart2DProps = new Chart2DProperties();
			chart2DProps.setChartDataLabelsPrecision (0);

			// On crée un label
			LegendProperties legendProps = new LegendProperties();
			String[] legendLabels = {"Nombre de tours"};
			legendProps.setLegendLabelsTexts (legendLabels);

			// On édite les propriétés du graphique (nombre d'entrées, nom des axes)
			GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
			String[] labelsAxisLabels = new String[backlogs.size()];
			for(int i = 0; i < backlogs.size(); i++){
				labelsAxisLabels[i]=Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tâche");
			graphChart2DProps.setNumbersAxisTitleText ("Nombre de tours");

			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphOutlineComponentsExistence (true);

			// On crée la feuille de donnée puis on la rempli avec les estimations
			// de chaque backlog
			Dataset dataset = new Dataset (1, backlogs.size(), 1);
			for(int i = 0; i < backlogs.size(); i++){
				JSONObject back = (JSONObject) backlogs.get(i);
				String idTache = back.get("idBacklog").toString();
				int nbTour = Integer.valueOf(stat.getNbTourParTache(idTache).get("nbTour").toString());
				dataset.set(0,i,0,nbTour);
			}

			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			// On crée le graphique puis on vérifie qu'il est valide
			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

		/**
		 * Cette méthode crée un graphique avec comme valeur la somme
		 * de l'écart relatif du joueur pour un backlog
		 *
		 * @param ecart le JSONArray contant les données sur les ecarts lors du backlog.
		 * @param nomTache 
		 * @return un graphique de type Chart2D
		 */
		public Chart2D EcartFinalRelatif(JSONArray ecart, String nomBacklog) {	    
			Object2DProperties object2DProps = new Object2DProperties();
			object2DProps.setObjectTitleText ("Ecart final relatif de la tâche : " + nomBacklog);

			Chart2DProperties chart2DProps = new Chart2DProperties();

			LegendProperties legendProps = new LegendProperties();
			String[] legendLabels = {"Somme de l'ecart de la tâche "};
			legendProps.setLegendLabelsTexts (legendLabels);

			GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
			String[] labelsAxisLabels = new String[ecart.size()];
			for (int i = 0; i<ecart.size(); i++){
				labelsAxisLabels[i] = Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tour");
			graphChart2DProps.setNumbersAxisTitleText ("Somme de l'ecart");
			graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);

			//Configure graph properties
			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphBarsExistence (false);
			graphProps.setGraphLinesExistence (true);
			graphProps.setGraphOutlineComponentsExistence (true);
			graphProps.setGraphAllowComponentAlignment (true);
			graphProps.setGraphDotsExistence (true);

			//Configure dataset
			Dataset dataset = new Dataset (1, ecart.size(), 1);
			JSONObject objetEcart;
			float val = 0;
			for (int i=0; i<ecart.size(); i++){
				objetEcart = (JSONObject) ecart.get(i);
				val = val + Float.parseFloat((String) objetEcart.get("valeur_ecart"));
				dataset.set(0,  i, 0, val );
			}

			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

		/**
		 * Cette méthode crée un graphique avec comme valeur la somme des
		 * écarts absolus du joueur pour un backlog donné.
		 *
		 * @param backlogs le JSONArray contant les données sur les écarts pour un backlog.
		 * @return un graphique de type Chart2D
		 */
		public Chart2D EcartFinalAbsolu(JSONArray ecart, String nomBacklog) {
			Object2DProperties object2DProps = new Object2DProperties();
			object2DProps.setObjectTitleText ("Ecart final absolu de la tâche : " + nomBacklog);

			Chart2DProperties chart2DProps = new Chart2DProperties();

			LegendProperties legendProps = new LegendProperties();
			String[] legendLabels = {"Somme de l'ecart"};
			legendProps.setLegendLabelsTexts (legendLabels);

			GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
			String[] labelsAxisLabels = new String[ecart.size()];
			for (int i = 0; i<ecart.size(); i++){
				labelsAxisLabels[i] = Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tour");
			graphChart2DProps.setNumbersAxisTitleText ("Somme de l'ecart");
			graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);

			//Configure graph properties
			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphBarsExistence (false);
			graphProps.setGraphLinesExistence (true);
			graphProps.setGraphOutlineComponentsExistence (true);
			graphProps.setGraphAllowComponentAlignment (true);
			graphProps.setGraphDotsExistence (true);

			//Configure dataset
			Dataset dataset = new Dataset (1, ecart.size(), 1);
			JSONObject objetEcart;
			float val = 0;
			for (int i=0; i<ecart.size(); i++){
				objetEcart = (JSONObject) ecart.get(i);
				val = val + Math.abs(Float.parseFloat((String) objetEcart.get("valeur_ecart")));
				dataset.set(0,  i, 0, val );
			}



			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

		/**
		 * Cette méthode crée un graphique avec comme valeur la somme des
		 * écarts absolus du joueur pour une partie.
		 *
		 * @param idUser L'id de l'utilisateur.
		 * @param idSalon L'id du salon.
		 * @param backlog le JSONArray contant les données sur les backlogs.
		 * @return un graphique de type Chart2D
		 */
		public Chart2D EcartFinalTotalAbsolu(String idUser, String idSalon, JSONArray backlog) {
			Object2DProperties object2DProps = new Object2DProperties();
			object2DProps.setObjectTitleText ("Ecart final absolu de la partie");



			Chart2DProperties chart2DProps = new Chart2DProperties();

			LegendProperties legendProps = new LegendProperties();
			String[] legendLabels = {"Somme de l'ecart"};
			legendProps.setLegendLabelsTexts (legendLabels);

			GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
			String[] labelsAxisLabels = new String[backlog.size()];
			for (int i = 0; i<backlog.size(); i++){
				labelsAxisLabels[i] = Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tâche");
			graphChart2DProps.setNumbersAxisTitleText ("Somme de l'ecart");
			graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);

			//Configure graph properties
			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphBarsExistence (false);
			graphProps.setGraphLinesExistence (true);
			graphProps.setGraphOutlineComponentsExistence (true);
			graphProps.setGraphAllowComponentAlignment (true);
			graphProps.setGraphDotsExistence (true);

			//Configure dataset
			Dataset dataset = new Dataset (1, backlog.size(), 1);
			JSONObject backlog_courrant;
			float val = 0;
			for (int i=0; i<backlog.size(); i++){
				backlog_courrant = (JSONObject) backlog.get(i);
				String idBacklog = backlog_courrant.get("idBacklog").toString();
				val = val + Float.parseFloat(
						stat.getEcartCumuleeParTache(idUser, idSalon, idBacklog).get("absolu").toString());
				dataset.set(0,  i, 0, val );
			}

			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

		/**
		 * Cette méthode crée un graphique avec comme valeur la somme des
		 * écarts absolus du joueur pour une partie.
		 *
		 * @param idUser L'id de l'utilisateur.
		 * @param idSalon L'id du salon.
		 * @param backlog le JSONArray contant les données sur les backlogs.
		 * @return un graphique de type Chart2D
		 */
		public Chart2D EcartFinalTotalRelative(String idUser, String idSalon, JSONArray backlog) {
			Object2DProperties object2DProps = new Object2DProperties();
			object2DProps.setObjectTitleText ("Ecart final relatif de la partie");



			Chart2DProperties chart2DProps = new Chart2DProperties();

			LegendProperties legendProps = new LegendProperties();
			String[] legendLabels = {"Somme de l'ecart"};
			legendProps.setLegendLabelsTexts (legendLabels);

			GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
			String[] labelsAxisLabels = new String[backlog.size()];
			for (int i = 0; i<backlog.size(); i++){
				labelsAxisLabels[i] = Integer.toString(i);
			}
			graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
			graphChart2DProps.setLabelsAxisTitleText ("Numéro de tâche");
			graphChart2DProps.setNumbersAxisTitleText ("Somme de l'ecart");
			graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);

			//Configure graph properties
			GraphProperties graphProps = new GraphProperties();
			graphProps.setGraphBarsExistence (false);
			graphProps.setGraphLinesExistence (true);
			graphProps.setGraphOutlineComponentsExistence (true);
			graphProps.setGraphAllowComponentAlignment (true);
			graphProps.setGraphDotsExistence (true);

			//Configure dataset
			Dataset dataset = new Dataset (1, backlog.size(), 1);
			JSONObject backlog_courrant;
			float val = 0;
			for (int i=0; i<backlog.size(); i++){
				backlog_courrant = (JSONObject) backlog.get(i);
				String idBacklog = backlog_courrant.get("idBacklog").toString();
				val = val + Float.parseFloat(
						stat.getEcartCumuleeParTache(idUser, idSalon, idBacklog).get("relative").toString());
				dataset.set(0,  i, 0, val );
			}

			MultiColorsProperties multiColorsProps = new MultiColorsProperties();

			LBChart2D chart2D = new LBChart2D();
			chart2D.setObject2DProperties (object2DProps);
			chart2D.setChart2DProperties (chart2DProps);
			chart2D.setLegendProperties (legendProps);
			chart2D.setGraphChart2DProperties (graphChart2DProps);
			chart2D.addGraphProperties (graphProps);
			chart2D.addDataset (dataset);
			chart2D.addMultiColorsProperties (multiColorsProps);

			if (!chart2D.validate (false)) chart2D.validate (true);

			return chart2D;
		}

	}
