package main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Cete classe implémente les cliques sur les cartes de jeu.	
 */
public class BtnListener implements MouseListener {

	private Interface f;
	private String s;
	private String idSalon;

	/**
	 * Crée le listener.
	 * 
	 * @param f la table de jeu.
	 * @param b le booléen permettant de connaitre l'état du double clique.
	 * @param s la valeur de la carte.
	 * @param idSalon l'id du salon.
	 */
	public BtnListener(Interface f, boolean b, String s, String idSalon) {
		this.s = s;
		this.f = f;
		this.idSalon = idSalon;

	}

	@Override
	/**
	 * Permet de voter la carte double cliquée.
	 */
	public void mouseClicked(MouseEvent me) {

		if (f.isEstClique() == false) {
			if (me.getClickCount() == 2) {
				String carte = "";
				switch (s) {
				case "?":
					carte = "?";
					break;
				case "café":
					carte = "café";
					break;
				case "0":
					carte = "0";
					break;
				case "0.5":
					carte = "0.5";
					break;
				case "1":
					carte = "1";
					break;
				case "2":
					carte = "2";
					break;
				case "3":
					carte = "3";
					break;
				case "5":
					carte = "5";
					break;
				case "8":
					carte = "8";
					break;
				case "13":
					carte = "13";
					break;
				case "20":
					carte = "20";
					break;
				case "40":
					carte = "40";
					break;
				case "100":
					carte = "100";
					break;
				}

				if (carte != "café") {
					f.setEstClique(true);
					f.disableInterface();
					f.popUp("Vous avez cliqué sur : " + carte, "carte jouée");
					TestFram.getMaPartie().jouerCarte(carte, f.nbTour, idSalon, f.tacheEnCour);
				} else {
					f.popUp("Votre carte cafée est prise en compte", "Information");
					TestFram.getMaPartie().jouerCarteCafe(f.nbTour, idSalon, f.tacheEnCour);
				}
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
