package main;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTextField;

import api.Partie;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Hashtable;

import javax.swing.JComboBox;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.SystemColor;

import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Cette classe implémente la fenêtre de choix de connection.
 */
public class Choixconnect extends JFrame {

	private JPanel contentPane;
	private JTextField txtChargerUnSalon;
	private JTextField txtNouveauSalon;
	Partie maPartie;
	private JComboBox listeSalon;
	private JSONArray res;
	private JSONArray resOwner;
	private String nomSalonCourant ;
	private String idSalonCourant ;
	private JTextField txtSalonOuVous;
	private JComboBox listeSalonOwner;
	private JButton gererPartie;
	private JTextField genererPdf;
	private JComboBox comboBox_pdf;
	private JButton button_pdf;

	/**
	 * Crée la fenêtre.
	 * 
	 * @param maPartie la partie en cours.
	 */
	public Choixconnect(final Partie mapartie) {

		this.maPartie = mapartie;
		res = maPartie.getSalonByIdUser();
		resOwner = maPartie.salonByOwner();
		final Hashtable idNomSalon = new Hashtable();
		final Hashtable salonByOwner = new Hashtable();

		String idSalon;
		String salon;
		String salonOwner;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 397);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{425, 0};
		gbl_contentPane.rowHeights = new int[]{43, 33, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);

		txtNouveauSalon = new JTextField();
		txtNouveauSalon.setHorizontalAlignment(SwingConstants.CENTER);
		txtNouveauSalon.setFocusable(false);
		txtNouveauSalon.setEditable(false);
		txtNouveauSalon.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtNouveauSalon.setText("Créer un nouveau salon :");
		GridBagConstraints gbc_txtNouveauSalon = new GridBagConstraints();
		gbc_txtNouveauSalon.anchor = GridBagConstraints.SOUTH;
		gbc_txtNouveauSalon.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNouveauSalon.insets = new Insets(0, 0, 5, 0);
		gbc_txtNouveauSalon.gridx = 0;
		gbc_txtNouveauSalon.gridy = 0;
		contentPane.add(txtNouveauSalon, gbc_txtNouveauSalon);
		txtNouveauSalon.setColumns(10);

		JButton btnCreeSalon = new JButton("Créer salon");
		btnCreeSalon.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent arg0) {

				try {
					creerSalon frame = new creerSalon(maPartie);
					dispose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
		});

		GridBagConstraints gbc_btnCreeSalon = new GridBagConstraints();
		gbc_btnCreeSalon.anchor = GridBagConstraints.NORTH;
		gbc_btnCreeSalon.insets = new Insets(0, 0, 5, 0);
		gbc_btnCreeSalon.gridx = 0;
		gbc_btnCreeSalon.gridy = 1;
		contentPane.add(btnCreeSalon, gbc_btnCreeSalon);

		txtChargerUnSalon = new JTextField();
		txtChargerUnSalon.setHorizontalAlignment(SwingConstants.CENTER);
		txtChargerUnSalon.setFocusable(false);
		txtChargerUnSalon.setEditable(false);
		txtChargerUnSalon.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtChargerUnSalon.setText("Ouvrir un salon existant :");
		GridBagConstraints gbc_txtChargerUnSalon = new GridBagConstraints();
		gbc_txtChargerUnSalon.anchor = GridBagConstraints.NORTH;
		gbc_txtChargerUnSalon.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtChargerUnSalon.insets = new Insets(0, 0, 5, 0);
		gbc_txtChargerUnSalon.gridx = 0;
		gbc_txtChargerUnSalon.gridy = 2;
		contentPane.add(txtChargerUnSalon, gbc_txtChargerUnSalon);
		txtChargerUnSalon.setColumns(10);

		JButton btnOuvrirSalon = new JButton("Ouvrir salon");
		btnOuvrirSalon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnOuvrirSalon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				nomSalonCourant = (String) listeSalon.getSelectedItem();

				if (nomSalonCourant != null){
					for (int i=0; i< idNomSalon.size();++i) {


						idSalonCourant = (String) idNomSalon.get(nomSalonCourant);
					}

					JSONObject Salon = maPartie.detailSalon(idSalonCourant);
					if(Salon.get("PartieDemarrer")!=null && Salon.get("partieDemarrer").equals("2")){
						JOptionPane jop;
						//Boîte du message préventif
						jop = new JOptionPane();
						jop.showMessageDialog(null, "La partie est déjà"
								+ " terminée", "Partie terminée", JOptionPane.INFORMATION_MESSAGE);

					}
					else{
						JSONObject pourTest = maPartie.rejoindreSalon(idSalonCourant);
						dispose();
						try {
							TestFram frame = new TestFram(maPartie,idSalonCourant,nomSalonCourant);

							frame.setVisible(true);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		});

		listeSalon = new JComboBox();
		GridBagConstraints gbc_listeSalon = new GridBagConstraints();
		gbc_listeSalon.fill = GridBagConstraints.HORIZONTAL;
		gbc_listeSalon.insets = new Insets(0, 0, 5, 0);
		gbc_listeSalon.gridx = 0;
		gbc_listeSalon.gridy = 3;
		contentPane.add(listeSalon, gbc_listeSalon);
		GridBagConstraints gbc_btnOuvrirSalon = new GridBagConstraints();
		gbc_btnOuvrirSalon.insets = new Insets(0, 0, 5, 0);
		gbc_btnOuvrirSalon.gridx = 0;
		gbc_btnOuvrirSalon.gridy = 4;
		contentPane.add(btnOuvrirSalon, gbc_btnOuvrirSalon);

		txtSalonOuVous = new JTextField();
		txtSalonOuVous.setHorizontalAlignment(SwingConstants.CENTER);
		txtSalonOuVous.setFocusable(false);
		txtSalonOuVous.setEditable(false);
		txtSalonOuVous.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtSalonOuVous.setText("Gestion d'un salon :");
		GridBagConstraints gbc_txtSalonOuVous = new GridBagConstraints();
		gbc_txtSalonOuVous.anchor = GridBagConstraints.SOUTH;
		gbc_txtSalonOuVous.insets = new Insets(0, 0, 5, 0);
		gbc_txtSalonOuVous.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSalonOuVous.gridx = 0;
		gbc_txtSalonOuVous.gridy = 5;
		contentPane.add(txtSalonOuVous, gbc_txtSalonOuVous);
		txtSalonOuVous.setColumns(10);

		listeSalonOwner = new JComboBox();
		GridBagConstraints gbc_listeSalonOwner = new GridBagConstraints();
		gbc_listeSalonOwner.insets = new Insets(0, 0, 5, 0);
		gbc_listeSalonOwner.fill = GridBagConstraints.HORIZONTAL;
		gbc_listeSalonOwner.gridx = 0;
		gbc_listeSalonOwner.gridy = 6;
		contentPane.add(listeSalonOwner, gbc_listeSalonOwner);

		gererPartie = new JButton("Gerer Partie");
		gererPartie.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				nomSalonCourant = (String) listeSalonOwner.getSelectedItem();
				if (nomSalonCourant != null){
					for (int i=0; i< salonByOwner.size();++i) {
						idSalonCourant = (String) salonByOwner.get(nomSalonCourant);
					}
					dispose();
					gestionSalon gestion = new gestionSalon(maPartie, idSalonCourant);
					gestion.setVisible(true);
				}
			}
		});
		GridBagConstraints gbc_gererPartie = new GridBagConstraints();
		gbc_gererPartie.insets = new Insets(0, 0, 5, 0);
		gbc_gererPartie.gridx = 0;
		gbc_gererPartie.gridy = 7;
		contentPane.add(gererPartie, gbc_gererPartie);

		for (int i=0; i<res.size();++i) {

			JSONObject result =  (JSONObject) res.get(i);
			idSalon = (String) result.get("Salon_idSalon");
			JSONObject salo = maPartie.detailSalon(idSalon);	

			salon = (String) salo.get("nom_salon");
			idNomSalon.put( salon ,idSalon);
			listeSalon.addItem(salon);

		}
		listeSalon.setSelectedItem(null);

		genererPdf = new JTextField();
		genererPdf.setText("Génération d'un PDF :");
		genererPdf.setHorizontalAlignment(SwingConstants.CENTER);
		genererPdf.setFont(new Font("Dialog", Font.BOLD, 14));
		genererPdf.setFocusable(false);
		genererPdf.setEditable(false);
		genererPdf.setColumns(10);
		GridBagConstraints gbc_gen_pdf = new GridBagConstraints();
		gbc_gen_pdf.insets = new Insets(0, 0, 5, 0);
		gbc_gen_pdf.fill = GridBagConstraints.HORIZONTAL;
		gbc_gen_pdf.gridx = 0;
		gbc_gen_pdf.gridy = 8;
		contentPane.add(genererPdf, gbc_gen_pdf);

		comboBox_pdf = new JComboBox();
		GridBagConstraints gbc_comboBox_pdf = new GridBagConstraints();
		gbc_comboBox_pdf.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_pdf.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_pdf.gridx = 0;
		gbc_comboBox_pdf.gridy = 9;
		contentPane.add(comboBox_pdf, gbc_comboBox_pdf);
		for (int i=0; i<res.size();++i) {

			JSONObject result =  (JSONObject) res.get(i);
			idSalon = (String) result.get("Salon_idSalon");
			JSONObject salo = maPartie.detailSalon(idSalon);	

			salon = (String) salo.get("nom_salon");
			idNomSalon.put( salon ,idSalon);
			comboBox_pdf.addItem(salon);
		}
		comboBox_pdf.setSelectedItem(null);

		button_pdf = new JButton("Générer le PDF");
		button_pdf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button_pdf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				nomSalonCourant = (String) comboBox_pdf.getSelectedItem();
				if (nomSalonCourant != null){
					for (int i=0; i< idNomSalon.size();++i) {
						idSalonCourant = (String) idNomSalon.get(nomSalonCourant);
					}

					JSONObject Salon = maPartie.detailSalon(idSalonCourant);
					if(Salon.get("partieDemarrer") != null && !Salon.get("partieDemarrer").equals("2")){
						JOptionPane jop;
						//Boîte du message préventif
						jop = new JOptionPane();
						jop.showMessageDialog(null, "La partie n'est pas "
								+ "encore terminée, génération"
								+ " impossible", "Partie non terminée", JOptionPane.INFORMATION_MESSAGE);

					}
					else{
						GenerationPdf gen = new GenerationPdf(mapartie, idSalonCourant);
						JOptionPane jop;
						//Boîte du message préventif
						jop = new JOptionPane();
						jop.showMessageDialog(null, "Le PDF a bien"
								+ " été généré.", "PDF généré", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
		});

		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.gridx = 0;
		gbc_button.gridy = 10;
		contentPane.add(button_pdf, gbc_button); 

		for (int i=0; i<resOwner.size();++i) {

			JSONObject result =  (JSONObject) resOwner.get(i);
			salonOwner = (String) result.get("nom_salon");
			String id = (String) result.get("idSalon");
			listeSalonOwner.addItem(salonOwner);
			salonByOwner.put(salonOwner, id);
			listeSalonOwner.setSelectedItem(null);
		}
	}

}
