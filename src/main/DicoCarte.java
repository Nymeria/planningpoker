package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

/**
 * Cette classe implémente un dictionnaire de cartes pour les
 * cartes des joueurs.
 */
public class DicoCarte {

	// Variables de la classe DicoCarte
	private Hashtable<String, Carte> dico;

	// Constructeur de la classe DicoCarte

	/**
	 * Permet de construire un objet de type DicoCarte avec un
	 * 
	 * dictionnaire initialement vide
	 */
	public DicoCarte() {
		this.dico = new Hashtable<String, Carte>();
	}

	// Getteurs et setteurs de la classe DicoCarte


	/**
	 * Permet de modifier le dictionnaire de cartes
	 * 
	 * @param dico Le nouveau dictionnaire
	 */
	public void setDico(Hashtable<String, Carte> dico){
		this.dico = dico;
	}

	/**
	 * Permet d'obtenir le dictionnaire de cartes
	 * @return Le dictionnaire de cartes
	 */
	public Hashtable<String, Carte> getDico(){
		return this.dico;
	}

	// Fonction de la classe DicoCarte

	/**
	 * Retourne la carte suivante par exemple la carte suivante de "1" est "2"
	 *
	 * @param carte La carte dont on veut la carte suivante
	 * @return La carte suivante de la carte entrée en paramètre
	 */
	public static String carteSuivante(String carte) {

		if (carte.equals("0")) {
			return "0.5";
		} else if (carte.equals("0.5")) {
			return "1";
		} else if (carte.equals("1")) {
			return "2";
		} else if (carte.equals("2")) {
			return "3";
		} else if (carte.equals("3")) {
			return "5";
		} else if (carte.equals("5")) {
			return "8";
		} else if (carte.equals("8")) {
			return "13";
		} else if (carte.equals("13")) {
			return "20";
		} else if (carte.equals("20")) {
			return "40";
		} else if (carte.equals("40")) {
			return "100";
		} else if (carte.equals("100")) {
			return "100";
		} else {
			return "0";
		}

	}

	/**
	 * Permet d'ajouer une carte au dictionnaire avec comme clé l'id de
	 * l'utilisateur.
	 *
	 * @param carte La carte à ajouter au dictionnaire.
	 * @param idUtilisateur L'identifiant à utiliser comme clé.
	 */
	public void AjouterCarte(String idUtilisateur, Carte carte) {
		if (!this.dico.containsKey(idUtilisateur)) {
			this.dico.put(idUtilisateur, carte);
		}
	}

	/**
	 * Permet d'obtenir une carte du dictionnaire en fonction de l'id de
	 * l'utilisateur
	 *
	 * @param idUtilisateur L'identifiant de l'utilisateur permettant de retrouver la
	 * carte.
	 * @return La carte ayant pour clé cet id d'utilisateur.
	 */
	public Carte ObtenirCarte(String idUtilisateur) {
		return this.dico.get(idUtilisateur);
	}

	/**
	 * Permet d'enlever une carte du dictionnaire avec comme clé l'id de
	 * l'utilisateur.
	 *
	 * @param idUtilisateur La clé de la carte à enlever.
	 */
	public void RetirerCarte(String idUtilisateur) {
		this.dico.remove(idUtilisateur);
	}

	/**
	 * Permet de calculer la moyenne des cartes jouées.
	 *
	 * @return La moyenne des cartes jouées.
	 */
	public float Moyenne() {
		float somme = 0;
		float valeur;
		int nbCartesUtiles = 0;
		Collection<Carte> cartes = this.dico.values();
		Carte[] tabCartes = (Carte[]) cartes.toArray();
		for (int i = 0; i < tabCartes.length; i++) {

			try {
				valeur = tabCartes[i].valeurNumerique();
				somme = somme + valeur;
				nbCartesUtiles = nbCartesUtiles + 1;
			} catch (IllegalArgumentException e) {
			}
		}

		return somme / nbCartesUtiles;
	}

	/**
	 * Permet de connaître la valeur de la plus petite carte jouée.
	 *
	 * @return La valeur de la plus petite carte jouée.
	 */
	public float Min() {
		float min = 100;
		float valeur;
		ArrayList<Carte> tabCartes = new ArrayList<Carte>(this.dico.values());

		for (int i = 0; i < tabCartes.size(); i++) {

			try {
				valeur = tabCartes.get(i).valeurNumerique();

				if (valeur < min) {
					min = valeur;
				}
			} catch (IllegalArgumentException e) {
			}
		}
		return min;
	}

	/**
	 * Permet de connaître la valeur de la plus grande.
	 *
	 * @return La valeur de la plus grande carte jouée.
	 */
	public float Max() {
		float max = 0;
		float valeur;
		ArrayList<Carte> tabCartes = new ArrayList<Carte>(this.dico.values());

		for (int i = 0; i < tabCartes.size(); i++) {

			try {
				valeur = tabCartes.get(i).valeurNumerique();

				if (valeur > max) {
					max = valeur;
				}
			} catch (IllegalArgumentException e) {
			}
		}
		return max;
	}

	/** 
	 * Permet de transformer la valeur d'un flottant en chaine de caractères
	 * 
	 * @param val Le flottant à transformer
	 * @return La chaîne de caractères correspondant au flottant entré en paramètre
	 */
	public static String floatToString(float val) {
		if (val == 0) {
			return "0";
		} else if (val == 0.5) {
			return "0.5";
		} else if (val == 1) {
			return "1";
		} else if (val == 2) {
			return "2";
		} else if (val == 3) {
			return "3";
		} else if (val == 5) {
			return "5";
		} else if (val == 8) {
			return "8";
		} else if (val == 13) {
			return "13";
		} else if (val == 20) {
			return "20";
		} else if (val == 40) {
			return "40";
		} else if (val == 100) {
			return "100";
		} else {
			return "0";
		}
	}

	/**
	 * Permet d'enlever une carte appartenant à un joueur
	 * 
	 * @param idJoueur L'identifiant du joueur auquel appartient la carte
	 * à enlever.
	 */
	public void EnleverCarte(String idJoueur) {
		this.dico.remove(idJoueur);
	}
}
