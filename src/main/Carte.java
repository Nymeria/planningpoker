package main;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class Carte extends JLabel{

	// Variables de la classe Carte
	private String idJoueur;
	private String nomJoueur;
	private ImageIcon image;
	private String valeur;

	// Constructeurs de la classe Carte

	/**
	 * Permet de construire un objet de type carte en passant
	 * en paramètres les variables d'une Carte
	 * 
	 * @param idJoueur L'identifiant du joueur associé à la carte
	 * @param nomJoueur Le nom du joueur associé à la carte
	 * @param image L'image correspondant à la valeur de la carte
	 */
	public Carte(String idJoueur, String nomJoueur, ImageIcon image){
		this.idJoueur = idJoueur;
		this.nomJoueur = nomJoueur;
		this.image = image;
		this.setIcon(image);
		this.setText(nomJoueur);
		this.valeur = "null";
		this.setVerticalAlignment(JLabel.BOTTOM);
		this.setHorizontalTextPosition(JLabel.CENTER);
		this.setVerticalTextPosition(JLabel.BOTTOM);
	}

	// Getteurs et setteurs de la classe Carte


	/**
	 * Permet de modifier la valeur de l'identifiant du joueur de la carte
	 * 
	 * @param idJoueur Le nouvel identifiant du joueur
	 */
	public void setIdJoueur(String idJoueur){
		this.idJoueur = idJoueur;
	}

	/**
	 * Permet d'obtenir la valeur de l'identifiant du joueur de la carte
	 * 
	 * @return L'identifiant du joueur
	 */
	public String getIdJoueur(){
		return this.idJoueur;
	}

	/**
	 * Permet de modifier la valeur du nom du joueur de la carte
	 * 
	 * @param nomJoueur Le nouveau nom du joueur
	 */
	public void setNomJoueur(String nomJoueur){
		this.nomJoueur = nomJoueur;
	};

	/**
	 * Permet d'obtenir le nom du joueur de la carte
	 * 
	 * @return Le nom du joueur
	 */
	public String getNomJoueur(){
		return this.nomJoueur;
	}

	/**
	 * Permet de modifier l'image associée à la carte
	 * 
	 * @param valeur La valeur de l'image à afficher
	 */
	public void setImage(String valeur){

		switch (valeur){
		case "0" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/0.png"));
			break;
		case "1" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/1.png"));
			break;
		case "0.5" :			
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/1-2.png"));
			break;
		case "2" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/2.png"));
			break;
		case "3" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/3.png"));
			break;
		case "5" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/5.png"));
			break;
		case "8" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/8.png"));
			break;
		case "13" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/13.png"));
			break;
		case "20" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/20.png"));
			break;
		case "40" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/40.png"));
			break;
		case "100" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/100.png"));
			break;
		case "interro" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/interro.png"));
			break;
		case "cafe" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/cafe.png"));
			break;
		default :
			break;
		}
		this.setIcon(this.image);
		this.repaint();
	}

	/**
	 * Permet d'obtenir l'image de la carte
	 * 
	 * @return L'image de la carte
	 */
	public ImageIcon getImage(){
		return this.image;
	}

	/**
	 * Permet de modifier la valeur de la carte
	 * 
	 * @param valCarte La nouvelle valeur de la carte 
	 */
	public void setValeur(String valCarte) {
		this.valeur = valCarte;
	}

	/**
	 * Permet d'obtenir la valeur de la carte
	 * 
	 * @return La couleur de la carte
	 */
	public String getValeur(){
		return this.valeur;
	}

	// Fonctions de la classe Carte


	/**
	 * Permet de transformer la valeur de la carte en un flottant
	 * 
	 * @return La valeur de la carte au format flottant
	 */
	public float valeurNumerique(){
		switch (this.valeur){
		case "0" :
			return 0; 
		case "1" : 
			return 1;
		case "0.5" :			
			return 0.5f;
		case "2" :
			return 2;
		case "3" :
			return 3;
		case "5" :
			return 5;
		case "8" :
			return 8;
		case "13" :
			return 13;
		case "20" :
			return 20;
		case "40" :
			return 40;
		case "100" :
			return 100;
		default :
			throw new IllegalArgumentException();
		}	
	}

	/**
	 * Permet de griser une carte
	 */
	public void grisage(){
		switch (valeur){
		case "null" :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/dos_non_vote.png"));
			break;
		default :
			this.image = new ImageIcon(TestFram.class.getResource("/ImgCarte/dos.png"));
			break;
		}
		this.setIcon(this.image);
		this.repaint();
	}

}
