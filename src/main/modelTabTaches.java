package main;

import javax.swing.table.DefaultTableModel;

/**
 * Cette classe implémente le modèle pour le tableau de taches.
 */
public class modelTabTaches extends DefaultTableModel {

	@Override
	public boolean isCellEditable(int RowIndex, int columnIndex) {
		return columnIndex != 0;
	}

}
