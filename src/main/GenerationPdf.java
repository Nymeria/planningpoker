package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import javax.imageio.ImageIO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import api.Partie;
import api.Statistique;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import net.sourceforge.chart2d.*;

/**
 * Cette classe permet la génération d'un pdf avec les statistiques
 * du joueur pour une partie donnée.
 */
public class GenerationPdf {

	/** Times Roman 18 Bold */
	private static final Font CATFONT = new Font(Font.getFamily("TIMES_ROMAN"), 18, Font.BOLD);

	/** Times Roman 12 Normal */
	private static final Font REDFONT = new Font(Font.getFamily("TIMES_ROMAN"), 12, Font.NORMAL);

	/** Times Roman 16 Bold */
	private static final Font SUBFONT = new Font(Font.getFamily("TIMES_ROMAN"), 16, Font.BOLD);

	/** Times Roman 12 Bold */
	private static final Font SMALLBOLD = new Font(Font.getFamily("TIMES_ROMAN"), 12, Font.BOLD);

	private String idSalon;
	private Partie maPartie;
	private Document document;
	private String out;
	private Statistique stat;
	private Graphiques graph;
	private JSONArray backlogs;

	/**
	 * Permet de générer un PDF avec les stats perso et globaux.
	 * 
	 * @param maPartie La partie courante.
	 * @param idSalon Le salon dont on veut les statistiques
	 */
	public GenerationPdf(Partie maPartie, String idSalon) {

		this.document = new Document(PageSize.A4);
		this.maPartie = maPartie;
		this.idSalon = idSalon;
		this.out = maPartie.detailSalon(idSalon).get("nom_salon")+" "+maPartie.idUser+".pdf";
		this.stat = new Statistique(maPartie.url);
		this.graph = new Graphiques(stat);
		backlogs = maPartie.recupererBackLog(idSalon);

		try {
			PdfWriter.getInstance(document, new FileOutputStream(out));
			document.open();
			ajouterEnTete();
			ajouterDetailsBacklogs();
			ajouterStatGenerales();
			ajouterSatPerso();     

		}
		catch(DocumentException de) {
			System.err.println(de.getMessage());
		}
		catch(IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		document.close();
	}

	private void ajouterLigneVide(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	/**
	 * Cette fonction ajoute l'en tête au document.
	 */
	private void ajouterEnTete(){
		Paragraph preface = new Paragraph();

		ajouterLigneVide(preface, 1);
		preface.add(new Paragraph(maPartie.detailSalon(idSalon).get("nom_salon").toString(), CATFONT));
		ajouterLigneVide(preface, 1);
		preface.add(new Paragraph("Rapport généré le " + new Date(), SMALLBOLD));
		ajouterLigneVide(preface, 1);
		preface.add(new Paragraph("fiche récapitulative de : " +
				maPartie.detailsUser(maPartie.idUser).get("prenom") + " " +
				maPartie.detailsUser(maPartie.idUser).get("nom"), SMALLBOLD));

		try {
			document.add(preface);
		} catch (DocumentException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
	}

	/**
	 * Cette fonction ajoute l'en tête du résumé des backlogs au document.
	 */
	private void ajouterDetailsBacklogs(){

		Paragraph DetailsBacklogs = new Paragraph();
		ajouterLigneVide(DetailsBacklogs, 1);
		DetailsBacklogs.add(new Paragraph("Résumé des tâches :", SUBFONT));
		ajouterLigneVide(DetailsBacklogs, 2);

		try {
			creerTableau(DetailsBacklogs);
		} catch (BadElementException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}

		try {
			document.add(DetailsBacklogs);
		} catch (DocumentException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}

	}

	/**
	 * Cette fonction permet d'ajouter les statistiques générales
	 * au PDF
	 * @throws MalformedURLException
	 */
	private void ajouterStatGenerales() throws MalformedURLException{

		Paragraph StatGen = new Paragraph();

		ajouterLigneVide(StatGen, 1);
		StatGen.add(new Paragraph("Statistiques générales :", SUBFONT));
		ajouterLigneVide(StatGen, 2);

		AjoutGraphique(graph.GraphEstimationBacklogs(backlogs), StatGen);
		ajouterLigneVide(StatGen, 1);
		AjoutGraphique(graph.PourcentageTemps(backlogs), StatGen);
		ajouterLigneVide(StatGen, 1);
		AjoutGraphique(graph.GraphNbToursBacklog(stat, backlogs), StatGen);

		try {
			document.add(StatGen);
		} catch (DocumentException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
	}

	/**
	 * Cette fonction permet d'ajouter les statistiques personnels
	 * au PDF
	 */
    private void ajouterSatPerso() {

        Paragraph StatPerso = new Paragraph();

        document.newPage();
        ajouterLigneVide(StatPerso, 1);
        StatPerso.add(new Paragraph("Statistiques personnelles :", SUBFONT));
        ajouterLigneVide(StatPerso, 1);
        StatPerso.add(new Paragraph("Nombre de cartes bleues :", REDFONT));
        ajouterLigneVide(StatPerso, 1);
        try {
            tabBleues(StatPerso);
        } catch (BadElementException e1) {
            // TODO Bloc catch généré automatiquement
            e1.printStackTrace();
        }
        JSONArray copie = new JSONArray();
        for (int i = 0; i < backlogs.size(); i++) {
            JSONObject test = (JSONObject) backlogs.get(i);
            if (test.get("estimation") != null && (test.get("estimation").equals("null") == false)) {
                copie.add((JSONObject) backlogs.get(i));
            }
        }
        ajouterLigneVide(StatPerso, 1);
        AjoutGraphique(graph.EcartFinalTotalRelative(maPartie.idUser, idSalon, copie), StatPerso);
        ajouterLigneVide(StatPerso, 1);
        AjoutGraphique(graph.EcartFinalTotalAbsolu(maPartie.idUser, idSalon, copie), StatPerso);

        try {
            document.add(StatPerso);
        } catch (DocumentException e) {
            // TODO Bloc catch généré automatiquement
            e.printStackTrace();
        }

        for (int i = 0; i < copie.size(); i++) {
            Paragraph StatBacklog = new Paragraph();

            JSONObject tache = (JSONObject) copie.get(i);
            String idTache = tache.get("idBacklog").toString();
            String nomTache = tache.get("tache").toString();
            JSONArray tendance = stat.getCartesParTacheEtJoeur(idSalon, idTache, maPartie.idUser);

            if (tendance != null && tendance.size() > 0) {
                document.newPage();
                ajouterLigneVide(StatBacklog, 1);
                StatBacklog.add(new Paragraph("Tâche :" + nomTache, SUBFONT));
                ajouterLigneVide(StatBacklog, 1);

                AjoutGraphique(graph.TendanceVoteTache(tendance, nomTache), StatBacklog);
                ajouterLigneVide(StatBacklog, 1);
                AjoutGraphique(graph.EcartFinalRelatif(tendance, nomTache), StatBacklog);
                ajouterLigneVide(StatBacklog, 1);
                AjoutGraphique(graph.EcartFinalAbsolu(tendance, nomTache), StatBacklog);

                try {
                    document.add(StatBacklog);
                } catch (DocumentException e) {
                    // TODO Bloc catch généré automatiquement
                    e.printStackTrace();
                }
            }
        }

    }

	/**
	 * Cette fonction permet de générer et ajouter un tableau contenant les
	 * statistiques pour les backlogs d'un salon dans un paragraphe.
	 * 
	 * @param detailsBacklogs Le paragraphe où ajouter le tableau.
	 * @throws BadElementException
	 */
	private void creerTableau(Paragraph detailsBacklogs) throws BadElementException {

		// On crée un tableau et on initialise les titres de colones.
		final PdfPTable table = new PdfPTable(4);

		PdfPCell nom = new PdfPCell(new Paragraph("Nom de la tâche"));
		PdfPCell description = new PdfPCell(new Paragraph("Description"));
		PdfPCell vote = new PdfPCell(new Paragraph("Estimation"));
		PdfPCell numero = new PdfPCell(new Paragraph("Numéro associé"));

		nom.setGrayFill(0.85f);
		description.setGrayFill(0.85f);
		vote.setGrayFill(0.85f);
		numero.setGrayFill(0.85f);

		table.addCell(nom);
		table.addCell(description);
		table.addCell(vote);
		table.addCell(numero);

		// On remplie les lignes du tableau avec les valeurs voulues.
		for (int i = 0; i<backlogs.size(); i++){
			JSONObject back =  (JSONObject) backlogs.get(i);
			String val = null;
			if(back.get("estimation")==null){
				val = "0";
			}
			else val = back.get("estimation").toString();
				table.addCell(back.get("tache").toString());
				if(back.get("description")!= null){
				table.addCell(back.get("description").toString());
				table.addCell(val);
				table.addCell(String.valueOf(i));
			}
		}

		// On ajoute le tableau au paragraphe.
		detailsBacklogs.add(table);
	}

	/**
	 * Cette fonction permet de générer un tableau pour connaitre
	 * le nombre de cartes bleues jouées par un utilisateur.
	 * 
	 * @param DetailSalon Le paragraphe ou ajouter le tableau.
	 * @throws BadElementException
	 */
	private void tabBleues(Paragraph DetailSalon) throws BadElementException {

		// On crée une table de 3 colonne et on initialise les titres
		final PdfPTable table = new PdfPTable(3);

		PdfPCell cafe = new PdfPCell(new Paragraph("Carte Café"));
		PdfPCell zero = new PdfPCell(new Paragraph("Carte 0"));
		PdfPCell interro = new PdfPCell(new Paragraph("Carte interrogation"));


		cafe.setGrayFill(0.85f);
		zero.setGrayFill(0.85f);
		interro.setGrayFill(0.85f);

		table.addCell(cafe);
		table.addCell(zero);
		table.addCell(interro);

		// On ajoute les valeurs dans le tableau
		JSONObject statsBleus = stat.getCartesBleus(maPartie.idUser,idSalon);
		table.addCell(statsBleus.get("cafe").toString());
		table.addCell(statsBleus.get("0").toString());
		table.addCell(statsBleus.get("?").toString());

		DetailSalon.add(table);
	}

	/**
	 * Cette fonction permet de ajouter un graphique de type chart2D dans le PDF
	 * et en assure la conversion en image.
	 * 
	 * @param chart2D Le graphique à inserer.
	 * @param paragraphe le paragraphe ou inserer le graphique.
	 */
	private void AjoutGraphique(Chart2D chart2D, Paragraph paragraphe){

		// On crée un fichier image
		File outputfile = null;
		Image img = null;

		// On écrit le graphique dans le fichier image au format png
		try {
			// retrieve image
			BufferedImage bi = chart2D.getImage();
			outputfile = new File("out.png");
			ImageIO.write(bi, "png", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// On lit le fichier png pour le transformer en image
		// puis on supprime le fichier png
		try {
			img = Image.getInstance("out.png");
			outputfile.delete();
		} catch (BadElementException | IOException e1) {
			// TODO Bloc catch généré automatiquement
			e1.printStackTrace();
		}

		// On ajoute l'image au paragraphe
		img.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
		paragraphe.add(img);
	}
}

