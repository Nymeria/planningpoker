package main;

import javax.swing.JPanel;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import api.Partie;


/**
 * Cette classe permet de gérer la mise des backlogs
 * dans un tableau.
 */
public class Backlog extends JPanel {
	private JTable table;
	private JSONArray backlogs = null;
	private Partie maPartie;

	/**
	 * Création du Panel
	 * 
	 * @param mapartie la partie en cours.
	 */
	public Backlog(Partie mapartie) {
		this.maPartie = mapartie;
		table = new JTable();
		mettreBacklogDansTableau();

	}

	/**
	 * Permet de mettre les backlogs et les
	 * informations relatives à ceux ci dans une tableau.
	 */
	void mettreBacklogDansTableau() {
		backlogs = maPartie.recupererBackLog(TestFram.idSalon);

		//JScrollPane scrollPane = new JScrollPane();
		//add(scrollPane, BorderLayout.CENTER);

		//JPanel panel = new JPanel();
		//scrollPane.setViewportView(panel);
		//panel.setLayout(new BorderLayout(0, 0));

		//setTable(new JTable());
		//panel.add(getTable(), BorderLayout.CENTER);
		if(backlogs != null) {
			DefaultTableModel model2 = new DefaultTableModel();
			model2.addColumn("nom");
			model2.addColumn("description");
			model2.addColumn("état");
			model2.addColumn("vote");
			getTable().setModel(model2);

			int nb_backlog;
			for (nb_backlog = 0; nb_backlog < backlogs.size(); nb_backlog++) {
				JSONObject back = (JSONObject) backlogs.get(nb_backlog);
				String estVoter = (String) back.get("estVoter");

				if (estVoter == null || estVoter.equals("1") == false) {
					estVoter = "NON";
				} else {
					estVoter = "OUI";
				}
				model2.addRow(new Object[]{(String) back.get("tache"), (String) back.get("description"),
						estVoter, (String) back.get("estimation")});
				getTable().setModel(model2);
			}
		}
	}

	/**
	 * Permet de récupérer le tableau des backlogs.
	 * 
	 * @return le tableau des backlogs.
	 */
	public JTable getTable() {
		return table;
	}

	/** 
	 * Permet de modifier le tableau des backlogs.
	 * 
	 * @param table le nouveau tableau de backlogs.
	 */
	public void setTable(JTable table) {
		this.table = table;
	}
}
