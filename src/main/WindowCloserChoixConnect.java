package main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import api.Partie;

/**
 * Cette classe implémente la fermeture d'une fenêtre.
 */
public class WindowCloserChoixConnect extends WindowAdapter {

	private gestionSalon f;
	private Partie mapartie;


	public WindowCloserChoixConnect(gestionSalon f) {
		this.f = f;
		this.mapartie = f.getMapartie();
	}

	@Override
	/**
	 * Action a effectuer lors de la fermeture. On renvoi vers le choix de
	 * connection.
	 */
	public void windowClosing(WindowEvent e){

		f.dispose();
		try {
			Choixconnect frame = new Choixconnect(mapartie);
			frame.setVisible(true);
		} catch (Exception e1) {
		}
	}
}
