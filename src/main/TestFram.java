package main;

import thread.ThreadChat;
import thread.ThreadParticipant;
import thread.ThreadPartieDemarrer;
import thread.ThreadJeu;
import thread.ThreadCafe;
import api.Chat;
import api.Partie;
import api.ScrumMaster;
import api.Statistique;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;

import thread.ThreadEnAttente;

/**
 * Cette classe implémente la fenêtre de plateau de jeu.
 */
public final class TestFram extends Interface {

	ScrumMaster scrum;
	public Statistique stat;


	private void debug(int ft) {
		if (ft == 1) {
			System.out.println("************ Fin du tour ********");
		} else {
			System.out.println("************ Fin Evaluation ********");
		}

		System.out.println("*** PARTIE ***");
		System.out.println("Tâche : " + this.tacheEnCour);
		System.out.println("N° tour : " + this.nbTour);
		System.out.println("fin tour  : " + this.finTour);

	}

	/**
	 * Crée la fenêtre
	 *
	 * @param mapartie la partie en cours.
	 * @param idsalon l'id du salon.
	 * @param nomsalon le no du salon.
	 * @throws java.io.IOException
	 */
	public TestFram(Partie mapartie, String idsalon, String nomsalon) throws IOException {
		super();

		/* donnée pour l'api */
		idSalon = idsalon;
		setMaPartie(mapartie);
		URL = mapartie.url;
		setIdUtilisateur(mapartie.idUser);
		apiChat = new Chat(URL, idSalon);
		scrum = new ScrumMaster(URL);
		stat = new Statistique(URL);

		debutEvaluation();
		tache_rejoint = tacheEnCour;

		/* initialisation de la fenêtre */
		initialize();

		//debutDeTour();
		// on lance le thread chat pour récuperer les messages
		chatThread = new ThreadChat(this, chat.textArea, apiChat);

		/* je desactive l'interface tant que la partie n'est pas démarrer */
		disableInterface();

		/* je lance le thread qui va s'occuper de détecter le démarrage de la partie */
		partieDemarrerThread = new ThreadPartieDemarrer(this);
		participantThread = new ThreadParticipant(this);
		jeuThread = new ThreadJeu(this);
		cafeThread = new ThreadCafe(this);

		System.out.println("=============================== DEUBG =======================");
		System.out.println("************ INFO ********");
		System.out.println("Id Utilisateur : " + this.getIdUtilisateur());
		System.out.println("Id Salon : " + this.idSalon);


	}

	/**
	 * Si mon statut est en mode "EN_ATTENTE" 
	 * je démarre un thread qui va s'occuper de savoir quand je pourrais jouer
	 */
	public void demarrerThreadAttente() {
		if(suisJeEnAttente() == true)
			attenteThread = new ThreadEnAttente(this);
	}

	/**
	 * permet de savoir si mon statut est "EN_ATTENTE"
	 * @return le booléen correspondant à l'état du statut du joueur.
	 */
	public boolean suisJeEnAttente() {
		/* je parcours la liste des participants */
		for (int i = 0; i < participants.size(); i++) {
			JSONObject participant = (JSONObject) participants.get(i);

			/* si mon etat est passer en mode "EN_ATTENTE" je démarre le thread */
			if (participant.get("Utilisateur_idUtilisateur").equals(getIdUtilisateur()) && participant.get("etat").equals("EN_ATTENTE")) {
				return true;
			}
		}
		return false;
	}

	@Override
	/**
	 * Initialise la fenêtre.
	 */
	protected void initialize() {
		super.initialize();
		this.addWindowListener(new WindowCloser(this));
		jbackl = TableauBacklogEntier();

	}

	/**
	 * Permet de lancer le début d'une évaluation.
	 */
	private void debutEvaluation() {
		/* on met à jour la liste des tâches */
		if (idSalon != null) {
			clavardeurs = null; // ré-initialise la liste des clavardeurs
			backlogs = getMaPartie().recupererBackLog(TestFram.idSalon);

			/* on récupère la tâche en cour (c'est la première tache où estVoter = null) */
			JSONObject backlog = null;
			for (int i = 0; i < backlogs.size(); i++) {
				backlog = (JSONObject) backlogs.get(i);
				String estVoter = (String) backlog.get("estVoter");

				if (estVoter == null) {
					tacheEnCour = (String) backlog.get("idBacklog");
					nomEnCour = (String) backlog.get("tache");
					descriptionEnCour = (String) backlog.get("description");
					if (nom_backlog_cours != null) {
						nom_backlog_cours.setText(nomEnCour);
					}
					break;
				}
			}
			if (table_1 != null) {
				mettreBacklogDansTableau();
			}

			if (backlog.get("estVoter") != null && backlog.get("estVoter").equals("1")) {
				finDuJeu();
			}
		}
		if (stats != null){
			stats.changeNomBacklog(nomEnCour);
		}

		Object val = stat.getNbTourParTache(tacheEnCour).get("nbTour");
		if(val != null ){
			nbTour = Integer.valueOf(val.toString());
		}
		else{
			nbTour = 0;
		}

	}

	/**
	 * Permet de finir le jeu.
	 */
	private void finDuJeu() {

		maPartie.fermerSalon(idSalon);

		if (chatThread != null) {
			chatThread.arret();
		}
		if (partieDemarrerThread != null) {
			partieDemarrerThread.arret();
		}
		if (participantThread != null) {
			participantThread.arret();
		}
		if (jeuThread != null) {
			jeuThread.arret();
		}
		if (cafeThread != null) {
			cafeThread.arret();
		}

		popUp("Cette partie est terminée, vous allez être"
				+ " redirigé vers l'écran de choix de salon.", "Fin de partie");
		Choixconnect connect = new Choixconnect(maPartie);
		connect.setVisible(true);
		this.dispose();
	}

	/**
	 * Permet de finir une évaluation.
	 */
	public void finEvaluation() {
		getMaPartie().finirEvaluationTache(tacheEnCour);
		try {
			Thread.sleep(5000);

		} catch (InterruptedException e) {
		}
		enableInterface();
		super.backdetail.mettreBacklogDansTableau();
		stats.resetStats();
		debutDeTour();

	}

	/**
	 * Cette méthode est appelé à chaque fin de tour
	 */
	public void finDuTour() {

		if (finTour == false) {

			String min = DicoCarte.floatToString(cartes.Min());
			String max = DicoCarte.floatToString(cartes.Max());

			// si il n'y a pas un écart de plus d'une carte, on a fini le tour
			if (max.equals(DicoCarte.carteSuivante(min)) || max.equals(min)) {
				debug(0);
				finEvaluation();
				debutEvaluation();
			} else {
				clavardeurs = getMaPartie().finTour(nbTour, TestFram.idSalon, tacheEnCour);
				this.nbTour = this.nbTour + 1;
				for (int i = 0; i < clavardeurs.size(); i++) {
					JSONObject cl = (JSONObject) clavardeurs.get(i);
					if (TestFram.getIdUtilisateur().equals((String) cl.get("Utilisateur_idUtilisateur"))) {
						chat.enableChat();
					}
				}
				JSONArray tendance = stat.getCartesParTacheEtJoeur(idSalon, tacheEnCour, getIdUtilisateur());
				stats.majStats(tendance);
				finTour = true;
				debug(1);
			}
		} else {
			System.out.println("tentative de fin de tour, mais false");
		}
	}

	/**
	 * Cette méthode est appelé à chaque début de tour
	 */
	public void debutDeTour() {
		finTour = false;
		setEstClique(false);
		this.revalidate();
	}

	/**
	 * Cette méthode affiche une carte face verso pour chaque participant
	 * connecter
	 */
	public void afficherCarteFaceVerso() {
		if (participants != null) {
			ImageIcon verso = new ImageIcon(TestFram.class.getResource("/ImgCarte/dos_non_vote.png"));

			for (int i = 0; i < participants.size(); i++) {
				JSONObject part = (JSONObject) participants.get(i);
				String idJoueur = (String) part.get("Utilisateur_idUtilisateur");
				JSONObject info = getMaPartie().detailsUser(idJoueur);
				String nomJoueur = (String) info.get("prenom") + " " + info.get("nom");
				//System.out.println((String)part.get("etat"));
				//System.out.println((String)part.get("etat")=="EN_LIGNE");
				if (part.get("etat").equals("EN_LIGNE")) {
					if (!cartes.getDico().containsKey(idJoueur)) {
						cartes.AjouterCarte(idJoueur, new Carte(idJoueur, nomJoueur, verso));
						panel_cartes_participants.add(cartes.ObtenirCarte(idJoueur));
						cartes.ObtenirCarte(idJoueur).repaint();
					}
				} else if (cartes.getDico().containsKey(idJoueur)) {
					panel_cartes_participants.remove(cartes.ObtenirCarte(idJoueur));
					cartes.EnleverCarte(idJoueur);
				}
			}
		}
	}

	/**
	 * Cette méthode affiche une carte face recto pour chaque participant
	 * connecté
	 */
	public void afficherCarteFaceRecto() {

		JSONObject carte;
		String valCarte, idUtilisateur;

		for (int i = 0; i < cartesRecto.size(); i++) {

			carte = (JSONObject) cartesRecto.get(i);
			valCarte = (String) carte.get("vote");
			idUtilisateur = (String) carte.get("Utilisateur_idUtilisateur");

			if (!cartes.getDico().isEmpty()) {
				if (idUtilisateur != null) {
					cartes.ObtenirCarte(idUtilisateur).setValeur(valCarte);
					cartes.ObtenirCarte(idUtilisateur).setImage(valCarte);
					cartes.ObtenirCarte(idUtilisateur).repaint();
				}
			}
		}
	}

	/**
	 * Affiche la liste des participants dans le tableau, rafraichit le tableau
	 * des participant si il y a eu des modifications.
	 */
	public void participantDansTableau() {
		int nb_user;
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Id");
		model.addColumn("Connecté");
		table_3.setModel(model);
		if (participants != null) {
			for (nb_user = 0; nb_user < participants.size(); nb_user++) {
				JSONObject part = (JSONObject) participants.get(nb_user);
				String id = (String) part.get("Utilisateur_idUtilisateur");
				JSONObject info = getMaPartie().detailsUser(id);

				model.addRow(new Object[]{(String) info.get("prenom") + " " + (String) info.get("nom"), part.get("etat")});
				table_3.setModel(model);
			}
		}

	}

	/**
	 * Permet de récuperer la liste des tâches.
	 */
	@Override
	void mettreBacklogDansTableau() {
		if (backlogs != null) {
			DefaultTableModel model2 = new DefaultTableModel();
			model2.addColumn("nom");
			model2.addColumn("état");
			model2.addColumn("vote");
			table_1.setModel(model2);

			int nb_backlog;
			for (nb_backlog = 0; nb_backlog < backlogs.size(); nb_backlog++) {
				JSONObject back = (JSONObject) backlogs.get(nb_backlog);
				String estVoter = (String) back.get("estVoter");

				if (estVoter == null || estVoter.equals("1") == false) {
					estVoter = "NON";
				} else {
					estVoter = "OUI";
				}

				model2.addRow(new Object[]{(String) back.get("tache"), estVoter,
						(String) back.get("estimation")});
				table_1.setModel(model2);
			}
		}
	}


	/**
	 * Permet de récuperer la liste des tâches dans un tableau
	 *
	 * @return le tableau des tâches.
	 */
	public JTable TableauBacklogEntier() {
		JTable tableBacklog = new JTable();
		if (backlogs != null) {
			DefaultTableModel modelTab = new DefaultTableModel();
			modelTab.addColumn("nom");
			modelTab.addColumn("description");
			modelTab.addColumn("état");
			modelTab.addColumn("vote");
			tableBacklog.setModel(modelTab);

			int nb_backlog;
			for (nb_backlog = 0; nb_backlog < backlogs.size(); nb_backlog++) {
				JSONObject back = (JSONObject) backlogs.get(nb_backlog);
				String estVoter = (String) back.get("estVoter");

				if (estVoter == null || estVoter.equals("1") == false) {
					estVoter = "NON";
				} else {
					estVoter = "OUI";
				}
				modelTab.addRow(new Object[]{(String) back.get("tache"), (String) back.get("description"),
						estVoter, (String) back.get("estimation")});
			}
			tableBacklog.setModel(modelTab);
		}
		return tableBacklog;
	}

	/**
	 * Cette méthode permet de récuperer un tableau JSON pour savoir si il y a
	 * eu une carte café de jouer
	 *
	 * @return les informations relatives aux cartes cafées
	 */
	public JSONArray recupererCarteCafe() {
		if (tacheEnCour != null) {
			return getMaPartie().voirCartesCafe(nbTour, idSalon, tacheEnCour);
		} else {
			return null;
		}
	}

	/**
	 * méthode à appeler lorsque la fenêtre est fermer
	 */
	public void close() {

		/* on indique à l'api que l'on quitte le salon */
		getMaPartie().quiterSalon(idSalon);

		/* on ferme le thread du chat */
		if (chatThread.getEtat() == true) {
			chatThread.arret();
		}

		/* si la partie n'est pas encore commencer, on quitte le thread */
		if (partieDemarrerThread.getEtat() == true) {
			partieDemarrerThread.arret();
		}

		if (participantThread.getEtat() == true) {
			participantThread.arret();
		}

		if (jeuThread.getEtat() == true) {
			jeuThread.arret();
		}

		if (cafeThread.isEtat() == true) {
			cafeThread.arret();
		}

		if (attenteThread != null && attenteThread.getEtat() == true) {
			attenteThread.arret();
		}
	}

}
