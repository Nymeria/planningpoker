package main;

import api.Chat;
import api.Partie;
import composant.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.Action;

import static javax.swing.Action.NAME;
import static javax.swing.Action.SHORT_DESCRIPTION;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import static main.TestFram.apiChat;
import static main.TestFram.idSalon;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import thread.ThreadCafe;
import thread.ThreadChat;
import thread.ThreadEnAttente;
import thread.ThreadJeu;
import thread.ThreadParticipant;
import thread.ThreadPartieDemarrer;

/**
 * Cette classe implémente la fenêtre principale de jeu.
 */
public abstract class Interface extends JFrame {

	Interface Interfc;
	protected Menu menu;
	public ChatGraphique chat;
	public Cartes Ccartes;
	public StatPerso stats;

	public JTable table_1;
	public JTable table_3;

	public JPanel panel_cartes_participants;
	protected JPanel panel_cartes_choix;
	protected JTabbedPane onglets = new JTabbedPane();   

	protected Component ContentPane;
	protected Action action = new SwingAction();
	protected JButton btnBacklogDtaill;
	protected JTable jbackl;
	protected Backlog backdetail;
	protected JLabel nom_backlog_cours = new JLabel("");

	/* thread */
	public ThreadChat chatThread;
	public ThreadPartieDemarrer partieDemarrerThread;
	public ThreadParticipant participantThread;
	protected ThreadJeu jeuThread;
	protected ThreadCafe cafeThread;
	protected ThreadEnAttente attenteThread;

	/* données pour l'api */
	protected static Chat apiChat;
	protected static Partie maPartie;
	public static String idSalon = null;
	private static String idUtilisateur = null;
	protected static String URL;
	public JSONArray participants = null;
	public JSONArray backlogs = null;
	public boolean is_init = false;
	public JSONArray cartesRecto = null;
	public JSONArray clavardeurs = null;



	/* donnée de la partie */
	public int nbTour;
	public boolean finTour = false;
	public String tacheEnCour = null;
	protected String nomEnCour = null;
	protected String descriptionEnCour = null;
	boolean puisJeParlerDansLeChat;
	public String tache_rejoint;
	private static boolean finDiscution;
        boolean is_enable=false;



	public DicoCarte cartes = new DicoCarte();
	public JPanel zone_clavardage;
	public JPanel panel;

	/**
	 * Crée la fenêtre principale de jeu.
	 */
	public Interface() {
		this.chat = new ChatGraphique();
		this.menu = new Menu();
		this.Interfc = this;
		Ccartes = new Cartes();
	}

	/**
	 * Permet de savoir si une carte à été cliquée.
	 * 
	 * @return le booléen correspondant.
	 */
	public boolean isEstClique() {
		return Ccartes.estClique;
	}

	/**
	 * Permet d'indiquer qu'une carte est cliquée.
	 * 
	 * @param estClique la nouvelle valeur du booléen estClique.
	 */
	public void setEstClique(boolean estClique) {
		Ccartes.estClique = estClique;
	}

	/**
	 * Permet de récuperer la partie en cours.
	 * 
	 * @return la partie en cours.
	 */
	public static Partie getMaPartie() {
		return maPartie;
	}

	/**
	 * Permet de modifier la partie en cours.
	 * 
	 * @param maPartie la nouvelle partie.
	 */
	public static void setMaPartie(Partie maPartie) {
		Interface.maPartie = maPartie;
	}

	/**
	 * Permet de mettre les backlogs dans un tableau.
	 */
	abstract void mettreBacklogDansTableau();

	/**
	 * permet de désactiver l'interface graphique du chat
	 */
	protected void disableChat() {
		chat.disableChat();
	}

	protected class SwingAction extends AbstractAction {

		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
		}
	}
        
        
        /**
         * Cette méthode permet de mettre la partie en pause 
         * (utiliser depuis le threadPartieDemarrer)
         */
        public void mettreEnPause() {
            if (is_enable == true) {
            Ccartes.disableInterface();
		this.disableChat();
                is_enable=false;
            }
        }
        
        /**
         * Permettre d'enable le jeux 
         * (Utiliser depuis le ThreadPartieDemarrer)
         */
        public void mettreEnJeux(){
            if (is_enable == false) {
                Ccartes.enableInterface();
                is_enable=true;
            }
        }

	/**
	 * Cette méthode permet de désactiver l'interface (tant que la partie n'est
	 * pas démarrer
	 */
	public void disableInterface() {
		Ccartes.disableInterface();
		this.disableChat();
		this.setEstClique(true);
	}

	/**
	 * Cette méthode permet de désactiver l'interface (tant que la partie n'est
	 * pas démarrer
	 */
	public void enableInterface() {

		Ccartes.enableInterface();
		stats.changeNomBacklog(nomEnCour);
		grisage();
		this.setEstClique(false);
	}

	/**
	 * Permet de griser les cartes (face cachée grise)
	 */
	public void grisage() {
		JSONObject carte;
		String idUti;
		for (int i = 0; i < cartesRecto.size(); i++) {

			carte = (JSONObject) cartesRecto.get(i);
			idUti = (String) carte.get("Utilisateur_idUtilisateur");
			if (!cartes.getDico().isEmpty()) {
				if (idUti != null) {
					cartes.ObtenirCarte(idUti).setValeur("null");
					cartes.ObtenirCarte(idUti).grisage();
					cartes.ObtenirCarte(idUti).repaint();
				}
			}
		}
	}

	/**
	 * Permet de dégriser les cartes (face cachée rouge)
	 */
	public void degrisage() {
		JSONObject carte;
		String valCarte, idUti;
		for (int i = 0; i < cartesRecto.size(); i++) {

			carte = (JSONObject) cartesRecto.get(i);
			valCarte = (String) carte.get("vote");
			idUti = (String) carte.get("Utilisateur_idUtilisateur");
			if (!cartes.getDico().isEmpty()) {
				if (idUti != null && cartes.ObtenirCarte(idUti) != null) {
					cartes.ObtenirCarte(idUti).setValeur(valCarte);
					cartes.ObtenirCarte(idUti).grisage();
					cartes.ObtenirCarte(idUti).repaint();
				}
			}
		}
	}

	/**
	 * permet d'afficher un message informatif
	 *
	 * @param message le message à afficher.
	 * @param titre le tritre de la boite de dialogue.
	 */
	public void popUp(String message, String titre) {
		JOptionPane jop = new JOptionPane();
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE);

	}

	/**
	 * permet d'intialiser l'interface graphique
	 */
	protected void initialize() {
		setFinDiscution(false);
		setMinimumSize(new Dimension(800, 600));
		getContentPane().setBackground(Color.WHITE);
		ContentPane = getContentPane();
		setBackground(Color.WHITE);
		setTitle("Planning Poker");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{1201, 280, 0};
		gridBagLayout.rowHeights = new int[]{424, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);

		TestFram.setDefaultLookAndFeelDecorated(true);
		this.setExtendedState(TestFram.MAXIMIZED_BOTH);
		this.setVisible(true);
		backdetail = new Backlog(maPartie) ;
		//this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		/**
		 * ********************************************************************************************************
		 */
		/**
		 * ********************************************************************************************************
		 */
		JPanel gauche = new JPanel();
		GridBagConstraints gbc_gauche = new GridBagConstraints();
		gbc_gauche.insets = new Insets(0, 0, 0, 5);
		gbc_gauche.fill = GridBagConstraints.BOTH;
		gbc_gauche.gridx = 0;
		gbc_gauche.gridy = 0;
		getContentPane().add(onglets, gbc_gauche);
		onglets.add(gauche);
		onglets.setTitleAt(0, "Salon");
		GridBagLayout gbl_gauche = new GridBagLayout();
		gbl_gauche.columnWidths = new int[]{418, 0};
		gbl_gauche.rowHeights = new int[]{661, 161, 0};
		gbl_gauche.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_gauche.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gauche.setLayout(gbl_gauche);
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * ********************************************************************************************************
		 */

		JPanel zone_jeu = new JPanel();
		GridBagConstraints gbc_zone_jeu = new GridBagConstraints();
		gbc_zone_jeu.insets = new Insets(0, 0, 5, 0);
		gbc_zone_jeu.fill = GridBagConstraints.BOTH;
		gbc_zone_jeu.gridx = 0;
		gbc_zone_jeu.gridy = 0;
		gauche.add(zone_jeu, gbc_zone_jeu);
		zone_jeu.setLayout(new BorderLayout());

		panel_cartes_participants = new JPanel();
		zone_jeu.add(panel_cartes_participants, BorderLayout.NORTH);

		panel_cartes_choix = new JPanel();
		GridBagLayout gbl_zone_jeu = new GridBagLayout();
		gbl_zone_jeu.columnWidths = new int[]{0, 0};
		gbl_zone_jeu.rowHeights = new int[]{0, 163, 0, 0};
		gbl_zone_jeu.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_zone_jeu.rowWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_cartes_choix.setLayout(gbl_zone_jeu);
		zone_jeu.add(panel_cartes_choix, BorderLayout.SOUTH);

		JScrollPane scrollPane = new JScrollPane();
		//scrollPane.setAutoscrolls(true);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		panel_cartes_choix.add(scrollPane,gbc_scrollPane);


		panel = new JPanel();
		scrollPane.setViewportView(panel);
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * ********************************************************************************************************
		 */

		panel.setLayout(new GridLayout(1, 0, 0, 0));
		/**
		 * ********************************************************************************************************
		 */

		Ccartes.init(Interfc);

		/**
		 * ***************JButton***********************	Bouton
		 * ***************************************
		 */
		Component verticalStrut = Box.createVerticalStrut(20);
		zone_jeu.add(verticalStrut);

		/**
		 * ********************************************************************************************************
		 */
		/**
		 * **************************************	Bouton Carte Interrogation
		 * ***************************************
		 */
		zone_clavardage = new JPanel();
		GridBagConstraints gbc_zone_clavardage = new GridBagConstraints();
		gbc_zone_clavardage.fill = GridBagConstraints.BOTH;
		gbc_zone_clavardage.gridx = 0;
		gbc_zone_clavardage.gridy = 1;
		gauche.add((Component) zone_clavardage, gbc_zone_clavardage);
		GridBagLayout gbl_zone_clavardage = new GridBagLayout();
		gbl_zone_clavardage.columnWidths = new int[]{0, 0, 0, 0};
		gbl_zone_clavardage.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_zone_clavardage.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_zone_clavardage.rowWeights = new double[]{1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		zone_clavardage.setLayout(gbl_zone_clavardage);
		/**
		 * ********************************************************************************************************
		 */

		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridheight = 3;
		gbc_textArea.gridwidth = 3;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = 0;
		zone_clavardage.add(ChatGraphique.textArea, gbc_textArea);
		/**
		 * ********************************************************************************************************
		 */

		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.anchor = GridBagConstraints.SOUTH;
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 3;
		zone_clavardage.add(chat.textField, gbc_textField);

		/**
		 * ********************************************************************************************************
		 */
		/**
		 * **************************************	Bouton Envoi Chat
		 * ***************************************
		 */
		chat.btnEnvoi.addMouseListener(new BtnEnvoieListener(this, getIdUtilisateur()));
		GridBagConstraints gbc_btnEnvoi = new GridBagConstraints();
		gbc_btnEnvoi.insets = new Insets(0, 0, 0, 5);
		gbc_btnEnvoi.gridx = 1;
		gbc_btnEnvoi.gridy = 3;
		zone_clavardage.add(chat.btnEnvoi, gbc_btnEnvoi);

		chat.textField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {

			}

			@Override
			public void keyTyped(KeyEvent arg0) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					chat.textField.requestFocus();
					String msg;
					msg = chat.textField.getText();
					//Execute when enter is pressed
					
					if (msg.isEmpty() == false || !msg.equals("Le scrum master à mis fin à la discution") || !msg.equals("c'est déconnecter")) {
						System.out.println("le message est : " + msg);
						apiChat.envoyerMessage(msg, getIdUtilisateur());
						chat.textField.setText("");
					}
					if (msg.equals("j'ai fini d'argumenter")){
						disableChat();
					}
				
				}
			}

		});
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton fin Chat
		 * ***********************************************
		 */
		chat.btnFini.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println(finDiscution);
				if (e.getClickCount() == 2) {
					if (isFinDiscution() == false ){
						finDiscution = true ;
						apiChat.envoyerMessage("j'ai fini d'argumenter", getIdUtilisateur());
						disableChat();

					}
				}
			}
		});
		GridBagConstraints gbc_btnFini = new GridBagConstraints();
		gbc_btnFini.gridx = 2;
		gbc_btnFini.gridy = 3;
		zone_clavardage.add(chat.btnFini, gbc_btnFini);
		/**
		 * ********************************************************************************************************
		 */

		/**
		 * **************************************	Bouton fin Chat
		 * ***********************************************
		 */
		JPanel droite = new JPanel();
		GridBagConstraints gbc_droite = new GridBagConstraints();
		gbc_droite.fill = GridBagConstraints.BOTH;
		gbc_droite.gridx = 1;
		gbc_droite.gridy = 0;
		getContentPane().add(droite, gbc_droite);
		GridBagLayout gbl_droite = new GridBagLayout();
		gbl_droite.columnWidths = new int[]{0, 0};
		gbl_droite.rowHeights = new int[]{163, 176};
		gbl_droite.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_droite.rowWeights = new double[]{1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		droite.setLayout(gbl_droite);
		/**
		 * ********************************************************************************************************
		 */

		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 0;
		droite.add(scrollPane_1, gbc_scrollPane_1);

		table_3 = new JTable();
		table_3.setPreferredSize(new Dimension(250, 170));
		table_3.setFillsViewportHeight(true);
		table_3.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		scrollPane_1.setViewportView(table_3);

		JScrollPane scrollPane_2 = new JScrollPane();

		scrollPane_2.setAutoscrolls(true);
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 1;
		droite.add(scrollPane_2, gbc_scrollPane_2);

		table_1 = new JTable();
		table_1.setFillsViewportHeight(true);
		table_1.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		/* récuperer les backlock et les mettres dans le tableau */
		mettreBacklogDansTableau();

		table_1.getColumnModel().getColumn(0).setResizable(false);
		table_1.getColumnModel().getColumn(0).setPreferredWidth(150);
		table_1.getColumnModel().getColumn(0).setMinWidth(150);
		table_1.getColumnModel().getColumn(1).setResizable(false);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(50);
		table_1.getColumnModel().getColumn(1).setMinWidth(50);
		table_1.getColumnModel().getColumn(2).setResizable(false);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(75);
		table_1.getColumnModel().getColumn(2).setMinWidth(75);
		scrollPane_2.setViewportView(table_1);

		btnBacklogDtaill = new JButton("Backlog détaillé");
		btnBacklogDtaill.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnBacklogDtaill.setEnabled(false);
				backdetail.mettreBacklogDansTableau();
				jbackl = backdetail.getTable();
				onglets.addTab(null, jbackl);
				int pos = onglets.indexOfComponent(jbackl);
				FlowLayout f = new FlowLayout(FlowLayout.CENTER, 5, 0);
				JPanel pnlTab = new JPanel(f);
				pnlTab.setOpaque(false);
				JLabel lblTitle = new JLabel("Backlog");
				JButton btnClose = new JButton();
				btnClose.setOpaque(false);
				btnClose.setRolloverIcon(new ImageIcon(Main.class.getResource("/ImgCarte/closeTabButton.png")));
				btnClose.setRolloverEnabled(true);
				btnClose.setIcon(new ImageIcon(Main.class.getResource("/ImgCarte/closeTabButton_desab.png")));
				btnClose.setBorder(null);
				btnClose.setFocusable(false);
				pnlTab.add(lblTitle);
				pnlTab.add(btnClose);
				pnlTab.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
				onglets.setTabComponentAt(pos, pnlTab);

				btnClose.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						onglets.remove(jbackl);

						btnBacklogDtaill.setEnabled(true);
					}
				});

			}
		});
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 0);
		gbc_button.gridx = 0;
		gbc_button.gridy = 2;
		droite.add(btnBacklogDtaill, gbc_button);

		stats = new StatPerso(URL);
		GridBagConstraints gbc_stats = new GridBagConstraints();
		gbc_stats.insets = new Insets(0, 0, 5, 0);
		gbc_stats.gridx = 0;
		gbc_stats.gridy = 3;
		droite.add(stats.getPanel(), gbc_stats);


		ListModel lm = new AbstractListModel() {
			String headers[] = {"0", "?", "cafe"};

			@Override
			public int getSize() {
				return headers.length;
			}

			@Override
			public Object getElementAt(int index) {
				return headers[index];
			}

		};

		new DefaultTableModel(lm.getSize(), 3);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnSalon = new JMenu("Salon");
		menuBar.add(mnSalon);
		init_menu(menuBar,mnSalon);

		this.is_init = true;
	}

	/**
	 * Permet d'initialiser le menu.
	 * 
	 * @param menuBar la barre de menu ou afficher le menu.
	 * @param mnSalon le menu du salon.
	 */
	private void init_menu(JMenuBar menuBar, JMenu mnSalon) {
		/*menu.mntmCrerSalon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.actionPerformedCreerSalon();
            }
        });
        mnSalon.add(menu.mntmCrerSalon);*/

		menu.mntmGrerSalon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				menu.actionPerformedGererSalon(idSalon, getIdUtilisateur(), onglets);
			}
		});
		mnSalon.add(menu.mntmGrerSalon);

		/* menu.mntmRejoindreSalon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.actionPerformedRejoindreSalon();
            }
        });
        mnSalon.add(menu.mntmRejoindreSalon);
		 */

		menu.mntmQuitterSalon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				menu.actionPerformedQuitterSalon(Interfc);
			}
		});
		mnSalon.add(menu.mntmQuitterSalon);

		/*
        menuBar.add(menu.mnCompte);

        menu.mntmConnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                menu.actionPerformedConnection();

            }
        });
        menu.mnCompte.add(menu.mntmConnection);

        menu.mntmDconnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                menu.actionPerformedDeconnection(Interfc);
            }
        });
        menu.mnCompte.add(menu.mntmDconnection);
		 */
	}

	/**
	 * Permet de savoir si la discussion est terminée.
	 * 
	 * @return le booléen correspondant à la fin de la discussion.
	 */
	public boolean isFinDiscution() {
		return finDiscution;
	}

	/**
	 * Permet d'indiquer que la discussion est finie.
	 * 
	 * @param finDiscution1 la nouvelle valeur de la fin de discussion.
	 */
	public static void setFinDiscution(boolean finDiscution1) {
		finDiscution = finDiscution1;
	}

	/**
	 * Permet de récupérer l'id de l'utilisateur courrant.
	 * 
	 * @return l'id de l'utilisateur courrant.
	 */
	public static String getIdUtilisateur() {
		return idUtilisateur;
	}

	/**
	 * Permet de modifier l'id de l'utilisateur courrant.
	 * 
	 * @param idUtilisateur le nouvel id de l'utilisateur.
	 */
	public static void setIdUtilisateur(String idUtilisateur) {
		Interface.idUtilisateur = idUtilisateur;
	}

}
