
package api;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Cette classe permet de gérer la Partie au niveau de l'Api.
 * 
 * @author morgan
 */
public class Partie extends Api {

	public boolean is_logged;
	public String idUser;
	public String url;

	/**
	 * Constructeur de la partie.
	 * 
	 * @param url l'url du serveur de jeu.
	 */
	public Partie(String url) {
		super(url);
		this.is_logged = false;
		this.url = url;
	}

	/**
	 * Cette méthode permet de se connecter (indispensable pour commencer une
	 * partie)
	 *
	 * @param user l'identifiant de l'utilisateur voulant se connecter.
	 * @param pass le mot de passe associé à l'utilisateur.
	 * @return un JSONObject null si les informations ne correspondent pas,
	 * contenant l'utilisateur sinon.
	 */
	public JSONObject login(String user, String pass) {
		ArrayList<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("Username", user));
		urlParameters.add(new BasicNameValuePair("Password", pass));

		JSONObject retval = this.sendContent(this.url + "/planningpoker/index.php/api/login", urlParameters);
		return retval;
	}

	/**
	 * Utiliser pour qu'un utilisateur puisse s'enregistrer
	 * @param nom le nom du futur utilisateur.
	 * @param prenom le prénom du futur utilisateur.
	 * @param pseudo le pseudo du futur utilisateur.
	 * @param mdp le mot de passe du futur utilisateur.
	 * @return un JSONObject contenant les informations du nouvel utilisateur.
	 */
	public JSONObject inscription(String nom, String prenom, String pseudo, String mdp) {
		ArrayList<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("prenom", prenom));
		urlParameters.add(new BasicNameValuePair("nom", nom));
		urlParameters.add(new BasicNameValuePair("pseudo", pseudo));
		urlParameters.add(new BasicNameValuePair("password", mdp));

		JSONObject retval = this.sendContent(this.url + "/planningpoker/index.php/api/utilisateur", urlParameters);
		return retval;
	}


	/**
	 * Permet de signaler à l'api qu'un joueur à jouer tel carte
	 * 
	 * @param carte la carte jouer.
	 * @param numTour le numero du tour.
	 * @param idSalon le salon.
	 * @param idTache la tache.
	 * @return un JSONObject contenant les informations de la carte jouée.
	 */
	public JSONObject jouerCarte(String carte, int numTour, String idSalon, String idTache) {
		if (is_logged == true) {
			Integer ntour = numTour;
			String tour = ntour.toString();
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("vote", carte));
			urlParameters.add(new BasicNameValuePair("tour", tour));
			urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUser));
			urlParameters.add(new BasicNameValuePair("Backlog_idBacklog", idTache));
			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));

			return this.sendContent(this.url + "/planningpoker/index.php/api/statistique", urlParameters);
		} else {
			return null;
		}
	}

	/**
	 * Permet de jouer une carte café
	 * @param numTour le numéro du tour.
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @return un JSONObject contenant les informations du vote.
	 */
	public JSONObject jouerCarteCafe( int numTour, String idSalon, String idTache ){
		if (is_logged == true) {
			Integer nTour = numTour;
			String tour = nTour.toString();
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("ntour", tour));
			urlParameters.add(new BasicNameValuePair("idSalon", idSalon));
			urlParameters.add(new BasicNameValuePair("idBacklog", idTache));
			urlParameters.add(new BasicNameValuePair("idUtilisateur", this.idUser));

			JSONObject retval = this.sendContent(this.url + "/planningpoker/index.php/api/cafe", urlParameters);
			return retval;
		} else
			return null;
	}
	/**
	 * Cette méthode permet de voir si une carte café à été demandé
	 * @param numTour le numéro du tour.
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @return un JSONArray contenant les cartes cafés pour le tour et le backlog donnés
	 * en paramètres.
	 */
	public JSONArray voirCartesCafe( int numTour, String idSalon, String idTache ){
		if (is_logged == true) {
			Integer nTour = numTour;
			String tour = nTour.toString();
			return getContent(this.baseUrl + "/planningpoker/index.php/api/voirCafe/"+idSalon+"/"+idTache +"/"+tour);
		} else
			return null;
	}


	/**
	 * Permet de voir les cartes que les autres joueur ont joués
	 * 
	 * @param numTour le numéro du tour.
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @return un JSONArray contenant les cartes jouées pour ce tour et ce backlog.
	 */
	public JSONArray voirCartesJoueurs( int numTour, String idSalon, String idTache ){
		if (is_logged == true) {
			Integer nTour = numTour;
			String tour = nTour.toString();
			return getContent(this.baseUrl + "/planningpoker/index.php/api/voirCartes/"+idSalon+"/"+idTache +"/"+tour);
		} else
			return null;
	}

	/**
	 * Permet de voir une carte pour l'utilisateur courrant.
	 * 
	 * @param numTour le numéro du tour.
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog
	 * @return la carte jouée par l'utilisateur pour ce tour et ce backlog.
	 */
	public JSONArray voirUneCarte( int numTour, String idSalon, String idTache ){
		if (is_logged == true) {
			Integer nTour = numTour;
			String tour = nTour.toString();
			return getContent(this.baseUrl + "/planningpoker/index.php/api/voirCartes/" + idUser + "/"+idSalon+"/"+idTache +"/"+tour);
		} else
			return null;
	}


	/**
	 *  Recupère les détails (nom du salon, partieDemarrer, etc ...) pour un salon donnée
	 * 
	 * @param idSalon l'id du salon.
	 * @return Les détails du salon.
	 */
	public JSONObject detailSalon(String idSalon) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/salon/" + idSalon);

	}

	/**
	 * Recupère tout les salons dont l'utilisateur est propriétaire
	 * 
	 * @return les salons dont l'utilisateur est propriétaire.
	 */
	public JSONArray salonByOwner() {
		if (is_logged == true) {    
			return getContent(this.baseUrl + "/planningpoker/index.php/api/salonParOwner/" + idUser);
		} else
			return null;
	}

	/**
	 * Permet de fermer un salon
	 * 
	 * @param idSalon l'id du salon.
	 * @return des informations relatives à la fermeture du salon.
	 */
	public JSONObject fermerSalon(String idSalon) {
		if (is_logged == true) {    
			return getUnContent(this.baseUrl + "/planningpoker/index.php/api/fermerSalon/" + idSalon);
		} else
			return null;
	}

	/**
	 * Retourne toutes les tâches d'un salon
	 * 
	 * @param idSalon l'id du salon.
	 * @return les tâches d'un salons.
	 */
	public JSONArray recupererBackLog(String idSalon) {
		if (is_logged == true) {
			return getContent(this.baseUrl + "/planningpoker/index.php/api/Backlog/" + idSalon);
		} else
			return null;
	}

	/**
	 * Indique à l'API que le tour est terminé
	 * 
	 * @param numTour le numéro du tour.
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @return la liste des joueurs autorisés à parler au chat.
	 */
	public JSONArray finTour( int numTour, String idSalon, String idTache) {
		if (is_logged == true) {
			Integer nTour = numTour;
			String tour = nTour.toString();
			return getContent(this.baseUrl + "/planningpoker/index.php/api/tourTerminer/"+idSalon+"/"+idTache +"/"+tour);
		} else
			return null;

	}

	/**
	 * Permet de signaler à l'api qu'on a fini d'évaluer la tâche.
	 * 
	 * @param idTache l'id du backlog.
	 * @return les informations relatives à la fin d'évaluation de la tâche.
	 */
	public JSONObject finirEvaluationTache(String idTache) {
		if (is_logged == true) {
			return getUnContent(this.baseUrl + "/planningpoker/index.php/api/finirEvaluationTache/" + idTache);
		} else
			return null;
	}

	/**
	 * Permet d'obtenir les détails d'un utilisateur.
	 * 
	 * @param idUtilisateur l'id de l'utilisateur.
	 * @return JSONArray (retourne un JSONArray qui enfaite un JSONObject
	 *                      il faut faire un for pour récuperer les données)
	 */
	public JSONObject detailsUser(String idUtilisateur) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/utilisateur/" + idUtilisateur); 
	}


	/**
	 * Recupere les salons auquel un utilisateur peut participer.
	 *
	 * @return la liste des salons de l'utilisateurs sous la forme de JSONArray.
	 */
	public JSONArray getSalonByIdUser() {
		if (is_logged == true) {
			return getContent(this.baseUrl + "/planningpoker/index.php/api/salonByUser/" + idUser);
		} else {
			return null;
		}
	}

	/**
	 * Permet de savoir si une tâche à fini d'être évaluée ou non
	 * 
	 * @param idTache
	 * @return l'état d'une tâche dans la base de donnée
	 */
	public JSONObject etatTache(String idTache) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/backlog/" + idTache);
	}

	/**
	 * Permet de passer le statut du joueur en mode : "EN_LIGNE"
	 * 
	 * @param idSalon l'id su salon.
	 * @return les information sur la mise en ligne du joueur.
	 */
	public JSONObject mettreJoeurEnLigne(String idSalon) {
		if (is_logged == true) {
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
			urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUser));
			return this.sendContent(this.url + "/planningpoker/index.php/api/mettreJouerEnLigne", urlParameters);
		} else {
			return null;
		}
	}



	/**
	 * Permet de rejoindre un salon, appelée lorsque l'utilisateur rejoint un salon.
	 * Permet de passer sont statut en : "EN_LIGNE"
	 * 
	 * @param idSalon l'id du salon.
	 * @return les information sur le fait que l'utilisateur rejoigne le salon.
	 */
	public JSONObject rejoindreSalon(String idSalon) {
		if (is_logged == true) {
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
			urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUser));
			return this.sendContent(this.url + "/planningpoker/index.php/api/rejoindreSalon", urlParameters);
		} else {
			return null;
		}

	}

	/**
	 * Appelée à la deconnexion d'un joeur, pour passer sont statut en "DECO"
	 * 
	 * @param idSalon l'id du salon.
	 * @return les information sur le fait que l'utilisateur quite le salon.
	 */
	public JSONObject quiterSalon(String idSalon) {
		if (is_logged == true) {
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
			urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUser));
			return this.sendContent(this.url + "/planningpoker/index.php/api/quiterSalon", urlParameters);
		} else {
			return null;
		}

	}

}