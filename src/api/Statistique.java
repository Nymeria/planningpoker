/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package api;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Permet de gérer les statistiques au niveau de l'Api.
 * 
 * @author morgan
 */
public class Statistique extends Api {

	/**
	 * Constructeur de statistique.
	 * 
	 * @param url l'url du serveur de jeu.
	 */
	public Statistique(String url) {
		super(url);
	}

	/**
	 * Retourne l'écart cumulé d'une partie pour un utilisateur donnée
	 * 
	 * @param idUser l'id de l'utilisateur.
	 * @param idSalon l'id du salon.
	 * @return {"relative":0,"absolu":3}
	 */
	public JSONObject getEcartCumulee (String idUser, String idSalon) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/getEcartCumulee/"+idUser+"/"+idSalon);
	}

	/**
	 * Retourne l'écart cumulé d'une partie pour un utilisateur donnée
	 * 
	 * @param idUser l'id de l'utilisateur.
	 * @param idSalon l'id du salon.
	 * @return {"relative":-3,"absolu":3}
	 */
	public JSONObject getEcartCumuleeParTache (String idUser, String idSalon, String idTache) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/getEcartCumuleeParTache/"+idUser+"/"+idSalon+"/"+idTache);
	}

	/**
	 * Retourne le nombre de cartes bleus joués par un utilisateur pour un salon donnée
	 * 
	 * @param idUser l'id de l'utilisateur.
	 * @param idSalon l'id du salon.
	 * @return      {"?":0,
	 *              "cafe":0,
	 *              "0":0}
	 */
	public JSONObject getCartesBleus(String idUser, String idSalon) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/getCartesBleus/"+idUser+"/"+idSalon);
	}

	/**
	 * Permet de connaître le nombre de tour qu'il y a eu par tâche avant d'être d'accord
	 * 
	 * @param idTache l'id du backlog.
	 * @return {"nbTour":0}
	 */
	public JSONObject getNbTourParTache(String idTache) {
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/getNbTourParTache/"+idTache);
	}

	/**
	 * Cette méthode retourne le min, le max et la valeur moyenne des cartes joués sur un tour donnée
	 * 
	 * @param numTour : numéro du tour 
	 * @param idSalon : le salon
	 * @param idTache : la tache concerné
	 * @return (min, max, moyenne)
	 */
	public JSONObject getMinMaxMoyenne (int numTour, String idSalon, String idTache) {
		Integer nTour = numTour;
		String tour = nTour.toString();
		return getUnContent(this.baseUrl + "/planningpoker/index.php/api/statistiqueMinMaxMoyen/"+idSalon+"/"+idTache +"/"+tour);  
	}


	/**
	 * Retourne toutes les infos des cartes joués (pour tout les joueurs) pour un tour donné
	 * 
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @return [{"Utilisateur_idUtilisateur":"1","Backlog_idBacklog":"6","enDessus":null,"enDessous":null,"valeur_moyenne":null,"valeur_ecart":null,"vote":"5","tour":"0","Salon_idSalon":"1"}]
	 */
	public JSONArray getCartesParTache(String idSalon, String idTache) {
		return getContent(this.baseUrl + "/planningpoker/index.php/api/getCartesParTache/"+idSalon+"/"+idTache );
	}


	/**
	 * Retourne toutes les infos des cartes joués (pour UN SEUL joueur) pour un tour donné
	 * 
	 * @param idSalon l'id du salon.
	 * @param idTache l'id du backlog.
	 * @param idJoueur l'id du joueur.
	 * @return  [{"Utilisateur_idUtilisateur":"1","Backlog_idBacklog":"6","enDessus":null,"enDessous":null,"valeur_moyenne":null,"valeur_ecart":null,"vote":"5","tour":"0","Salon_idSalon":"1"}]
	 */
	public JSONArray getCartesParTacheEtJoeur(String idSalon, String idTache, String idJoueur) {
		return getContent(this.baseUrl + "/planningpoker/index.php/api/getCartesParTacheEtJoeur/"+idSalon+"/"+idTache +"/"+idJoueur );
	}

}
