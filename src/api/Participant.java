/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import org.json.simple.JSONArray;

/**
 * Cette classe permet de gérer les participants au niveau de l'Api.
 * 
 * @author morgan
 */
public class Participant extends Partie {

	/**
	 * Constructeur de la classe.
	 * 
	 * @param url L'url du serveur de jeu.
	 */
	public Participant(String url) {
		super(url);
	}

	/**
	 * Liste les participant d'un salon (pour savoir si il sont en ligne)
	 *
	 * @param idSalon le numéro du salon de jeu.
	 * @return la liste des participants inscrits au salon sous forme de
	 * JSONArray.
	 */
	public JSONArray listeDesParticipants(String idSalon) {
		return getContent(this.baseUrl + "/planningpoker/index.php/api/participantParSalon/" + idSalon);

	}

	/**
	 * Recuperer la liste des joueurs ayant le statut "EN_LIGNE"
	 *
	 * @param idSalon le numéro du salon de jeu.
	 * @return un JSONArray contenant les joueurs en lignes et des informations
	 * relatives à ceux ci.
	 */
	public JSONArray getJoueurEnLigne(String idSalon) {
		return getContent(this.baseUrl + "/planningpoker/index.php/api/joueurEnLigne/" + idSalon);
	}

}
