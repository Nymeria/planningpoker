package api;

import java.net.URLEncoder;

/**
 * Cette classe permet de gérer le chat au niveau de l'Api.
 */
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
public class Chat extends Api {

	private String idSalon;

	/**
	 * Constructeur du chat.
	 * 
	 * @param url L'url du serveur de jeu.
	 * @param idSalon L'id du salon d'où l'on veut le chat.
	 */
	public Chat(String url, String idSalon) {
		super(url);
		this.idSalon = idSalon;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Cette méthode permet d'envoyer un message au chat.
	 *
	 * @param message le message à envoyer.
	 * @param idUtilisateur l'identifiant de l'utilisateur envoyant le message.
	 */
	public  void envoyerMessage(String message, String idUtilisateur) {

		ArrayList<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

		urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUtilisateur));
		urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
		urlParameters.add(new BasicNameValuePair("message", message));
		System.out.println("message envoyé : "+message);
		this.sendContent(this.baseUrl+"/planningpoker/index.php/api/clavardage/", urlParameters);


	}

	@SuppressWarnings("deprecation")
	/**
	 * Cette méthode permet de recevoir tous les messages du chat.
	 *
	 * @param dernierMsg le dernier message reçu par l'utilisateur.
	 * @return la liste des messages du chat au format JSONArray.
	 */
	public JSONArray recevoirMessage(String dernierMsg) {
		if (dernierMsg != null && dernierMsg.isEmpty() == false )
			return this.getContent(this.baseUrl+"/planningpoker/index.php/api/clavardage/"+idSalon+"/"+URLEncoder.encode(dernierMsg));
		else
			return this.getContent(this.baseUrl+"/planningpoker/index.php/api/clavardage/"+idSalon);

	}

}
