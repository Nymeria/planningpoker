package api;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author morgan , valentin
 *
 */
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

/**
 * Cette classe permet la communication entre le serveur.
 */
public class Api implements Runnable {

	HttpClient client;
	ResponseHandler<String> handler;
	HttpResponse resp;
	String baseUrl;
	private final Lock _mutex = new ReentrantLock(true);

	/**
	 * Constructeur de l'Api.
	 */
	public Api(String url) {
		baseUrl = url;
		client = new DefaultHttpClient();
		handler = new BasicResponseHandler();
		resp = null;

	}

	/**
	 * Cette méthode permet de récuperer la réponse du serveur pour une url
	 * donnée voir la doc d'utilisation de l'api.
	 *
	 * @param url l'url de la requete.
	 * @return la réponse du serveur encodé au format json.
	 */
	public JSONArray getContent(String url) {
		_mutex.lock();
		HttpGet httpGet = new HttpGet(url);
		try {
			resp = client.execute(httpGet);
		} catch (IOException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}
		String body = null;
		try {
			body = handler.handleResponse(resp);
		} catch (IOException ex) {
			System.err.println("reponse du serveur : " + resp);
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}

		int statusCode = resp.getStatusLine().getStatusCode();
		_mutex.unlock();
		JSONArray retour = null;
		try {
			retour = getArray(body);
		} catch (ParseException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}

		return retour;

	}


	/**
	 * Cette méthode permet de récuperer la réponse du serveur pour une url
	 * donnée voir la doc d'utilisation de l'api.
	 *
	 * @param url l'url de la requete.
	 * @return la réponse du serveur encodé au format json.
	 */
	public JSONObject getUnContent(String url) {
		_mutex.lock();
		HttpGet httpGet = new HttpGet(url);
		try {
			resp = client.execute(httpGet);
		} catch (IOException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}
		String body = null;
		try {
			body = handler.handleResponse(resp);
		} catch (IOException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}

		int statusCode = resp.getStatusLine().getStatusCode();
		_mutex.unlock();
		JSONObject retour = null;
		try {
			retour = getObjet(body);
		} catch (ParseException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}

		return retour;
	}

	/**
	 * Cette méthode permet d'envoyer une requête POST au serveur
	 * @usage :  RestRequeteUrl, Paramètres:   
	 *          ArrayList<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	 *          urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", "1"));
	 *          
	 * @param url l'url du server ou effectuer le contenu.
	 * @param urlParameters les paramètres de l'url.
	 * @return le contenu renvoyer par le server au format JSONObject. 
	 */
	public JSONObject sendContent(String url, ArrayList<NameValuePair> urlParameters) {
		_mutex.lock();
		HttpPost httpPost = new HttpPost(url);
		StringBuffer result = new StringBuffer();
		JSONObject retour = null;

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(urlParameters,"UTF-8"));
			resp = client.execute(httpPost);
			int statusCode = resp.getStatusLine().getStatusCode();
			BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			_mutex.unlock();
			retour = getObjet(result.toString());

		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ParseException ex) {
			System.err.println("Erreur sur le getObject() dans la méthode sendContent");
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			System.err.println("Erreur sur le client.execute() dans la méthode sendContent");
			Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
		}

		return retour;


	}


	/**
	 * Cette méthode permet de convertir une chaîne de caractère en objet
	 * JSON.
	 *
	 * @param ligne la ligne La chaîne à convertir.
	 * @return l'objet obtenu à partir de la chaîne de caractère au format JSONObject.
	 */
	private JSONObject getObjet(String ligne) throws ParseException {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(ligne);
		JSONObject jsonObject = (JSONObject) obj;
		return jsonObject;
	}


	/**
	 * Cette méthode permet de convertir une chaîne de caractère en objet
	 * JSON.
	 *
	 * @param ligne la ligne La chaîne à convertir.
	 * @return l'objet obtenu à partir de la chaîne de caractère au format JSONArray.
	 */
	private JSONArray getArray(String ligne) throws ParseException {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(ligne);
		JSONArray Array = (JSONArray) obj;
		return Array;


	}

	@Override
	public void run() {

	}
}
