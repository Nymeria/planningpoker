package api;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Cette classe permet de gérer les actions relatives au
 * scrum master avec l'Api.
 * 
 * @author morgan
 */
public class ScrumMaster extends Partie {

	/**
	 * Constructeur du scrumMaster.
	 * 
	 * @param url l'url du salon de jeu.
	 */
	public ScrumMaster(String url) {
		super(url);
	}


	/**
	 * permet de mettre à jour une tache
	 *
	 * @param nomTache le nom de la tache.
	 * @param description la description de la tache.
	 * @param idTache l'id de la tache.
	 * @return les informations sur la bonne execution de la fonction.
	 */
	public JSONObject majTache(String nomTache, String description, String idTache) {
		if (is_logged == true) {
			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("tache", nomTache));
			urlParameters.add(new BasicNameValuePair("description", description));

			return this.sendContent(this.url + "/planningpoker/index.php/api/backlog/" + idTache, urlParameters);

		} else {
			return null;
		}
	}

	/**
	 * On signale à l'api qu'on à terminer d'évaluer la tache
	 *
	 * @param idTache l'id de la tache
	 * @return JSONObject (l'entrée enregistrée dans la table)
	 */
	@Override
	public JSONObject finirEvaluationTache(String idTache) {
		if (is_logged == true) {
			return getUnContent(this.baseUrl + "/planningpoker/index.php/api/finirEvaluationTache/" + idTache);
		} else {
			return null;
		}
	}

	/**
	 * Cette méthode ajoute un participant à un salon
	 *
	 * @param idSalon : id du salon ou ajouter le participant
	 * @param idUtilisateur : id du participant (si vide, on prend l'utilisateur
	 * courant)
	 * @return JSONObject (l'entrée enregistrée dans la table)
	 */
	public JSONObject ajouterParticipant(String idSalon, String idUtilisateur) {
		if (is_logged == true) {
			if (idUtilisateur.isEmpty()) {
				idUtilisateur = "" + idUser;
			}

			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
			urlParameters.add(new BasicNameValuePair("Utilisateur_idUtilisateur", idUtilisateur));
			return this.sendContent(this.baseUrl + "/planningpoker/index.php/api/Participant", urlParameters);
		} else {
			return null;
		}

	}

	/**
	 * Cette méthode supprime un participant d'un salon
	 *
	 * @param idSalon : id du salon ou ajouter le participant
	 * @param idUtilisateur : id du participant (si vide, on prend l'utilisateur
	 * courant)
	 * @return JSONObject (l'entrée enregistrée dans la table)
	 */
	public JSONObject supprimerParticipant(String idSalon, String idUtilisateur) {
		if (is_logged == true) {
			return this.getUnContent(this.baseUrl + "/planningpoker/index.php/api/supprimerParticipant/" + idUtilisateur + "/" + idSalon);
		} else {
			return null;
		}

	}

	/**
	 * Cette méthode supprime un participant d'un salon
	 *
	 * @param idTache l'id de la tache.
	 * @return JSONObject (l'entrée enregistrée dans la table)
	 */
	public JSONObject supprimerTache(String idTache) {
		if (is_logged == true) {
			return this.getUnContent(this.baseUrl + "/planningpoker/index.php/api/supprimerTache/" + idTache);
		} else {
			return null;
		}

	}


	/**
	 * Cette méthode permet de créer une nouvelle partie (Uniquement pour
	 * l'application ScumMaster)
	 *
	 *  / \
	 * / ! \ l'utilisateur doit impérativement être connecté
	 *
	 *
	 *
	 * @param nom_salon : nom que portera la partie (apparait dans la liste des
	 * parties)
	 * @param url_iceScrum : si l'url est précisé, toutes les données iceScrum
	 * seront importés
	 * @return retourne les données du salon tels qu'enregistrer dans la bdd
	 */
	public JSONObject creerNouveauSalon(String nom_salon, String url_iceScrum, String idSprint) {
		if (is_logged == true) {

			String owner_id = "" + idUser;

			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("nom_salon", nom_salon));
			urlParameters.add(new BasicNameValuePair("owner_id", owner_id));

			/* si la partie est relié à iceScrum */
			if (!url_iceScrum.isEmpty()) {
				urlParameters.add(new BasicNameValuePair("url_icescrum", url_iceScrum));
				urlParameters.add(new BasicNameValuePair("idSprint", idSprint));
			}

			return this.sendContent(this.baseUrl + "/planningpoker/index.php/api/salon", urlParameters);
		} else {
			return null;
		}

	}

	/**
	 * Retourne toutes les utilisateurs
	 *
	 * @return Tout les utilisateurs
	 */
	public JSONArray listeUtilisateurs() {
		if (is_logged == true) {

			return getContent(this.baseUrl + "/planningpoker/index.php/api/utilisateur/");
		} else {
			return null;
		}
	}

	/**
	 * Retourne toutes les utilisateurs
	 *
	 * @return Tout les utilisateurs
	 */
	public JSONArray utilisateursParSalon() {
		if (is_logged == true) {

			return getContent(this.baseUrl + "/planningpoker/index.php/api/utilisateur/");
		} else {
			return null;
		}
	}

	/**
	 * Permet d'ajouter une tâche pour un salon donnée
	 *
	 * @param nom_tache : nom que portera la tache
	 * @param description : description de la tâche
	 * @param idSalon : idDuSalon ou ajouter la tâche
	 * @return JSONObject (l'entrée enregistrer dans la table)
	 */
	public JSONObject ajouterTache(String nom_tache, String description, String idSalon) {

		if (is_logged == true) {

			ArrayList<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("tache", nom_tache));
			if (description != null) {
				urlParameters.add(new BasicNameValuePair("description", description));
			}

			urlParameters.add(new BasicNameValuePair("Salon_idSalon", idSalon));
			return this.sendContent(this.baseUrl + "/planningpoker/index.php/api/backlog", urlParameters);
			//     http://localhost:8080/icescrum/ws/p/PROJ1/task/
			//    http://localhost:8080/icescrum/ws/p/PROJ1/task/2/estimate
		} else {
			return null;
		}

	}

	/**
	 * Permet de démarrer une partie.
	 * 
	 * @param idSalon l'id du salon.
	 * @return les informations relatives à la bonne execution de la fonction.
	 */
	public JSONObject demarrerPartie(String idSalon) {
		if (is_logged == true) {

			return this.getUnContent(this.baseUrl + "/planningpoker/index.php/api/demarrerPartie/" + idSalon);
		} else {
			return null;
		}
	}

	/**
	 * Cette méthode permet de mettre une partie en pause
	 * 
	 * @param idSalon l'id du salon.
	 * @return les informations relatives à la bonne execution de la fonction.
	 */
	public JSONObject mettrePartieEnPause(String idSalon) {
		if (is_logged == true) {
			return this.getUnContent(this.baseUrl + "/planningpoker/index.php/api/mettreEnPausePartie/" + idSalon);
		} else {
			return null;
		}
	}

	/**
	 * Permet de supprimer un salon
	 * 
	 * @param idSalon l'id du salon.
	 * @return les informations relatives à la bonne execution de la fonction.
	 */
	public JSONObject supprimerSalon(String idSalon) {
		if (is_logged == true) {
			return this.getUnContent(this.baseUrl + "/planningpoker/index.php/api/supprimerSalon/" + idSalon);
		} else {
			return null;
		}
	}

}
