package thread;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author morgan
 */
import main.TestFram;
import api.Participant;
import api.Partie;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Cette classe permet de savoir si une partie est démarrée où non.
 */
public class ThreadPartieDemarrer extends Thread {

	private Thread t;
	private Boolean etat;
	static TestFram IHM;
	private JSONObject salon = null;
	private String partieDemarrer;
	private Partie api;
        boolean is_enable =false;

	/** Crée le thread.
	 * 
	 * @param ihm le plateau de jeu.
	 */
	public ThreadPartieDemarrer(TestFram ihm) {
		ThreadPartieDemarrer.IHM = ihm;
		api = new Partie(TestFram.getMaPartie().url);
		api.idUser = TestFram.getMaPartie().idUser;
		api.is_logged = true;
		setEtat(true);
		this.start();
	}

	@Override
	/**
	 * Permet de lancer le thread et de le faire tourner en boucle.
	 */
	public void run() {
		while (getEtat()) {

			if (TestFram.idSalon != null) {
				salon = api.detailSalon(TestFram.idSalon);
			}

			if (salon != null) {
				/* on récupère les informations sur la partie */
				partieDemarrer = (String) salon.get("partieDemarrer");

				/* si la partie est démarrer */
				if (partieDemarrer != null && partieDemarrer.equals("1")) {
                                    
					/* on active l'interface graphique */
					if(suisJeConnecte()){
                                            if (is_enable == false) {
						ThreadChat.setComp(0);
						ThreadChat.setJaiFini(0);
						IHM.enableInterface();
                                                is_enable = true;
                                            } else {
                                                IHM.mettreEnJeux();
                                                
                                            }
                                            JSONArray tendance = IHM.stat.getCartesParTacheEtJoeur(IHM.idSalon, 
                    								IHM.tacheEnCour, IHM.getIdUtilisateur());
                    						IHM.stats.majStats(tendance);
					}
					
				}
                                
                                /* si la partie est en pause */
				if (partieDemarrer != null && (partieDemarrer.equals("0") || partieDemarrer.equals("2"))) {
					/* on desactive l'interface graphique */
					if(is_enable == true){
						IHM.mettreEnPause();
                                                is_enable= false;
					}
				}
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Permet de savoir si l'utilisateur est connecté.
	 * 
	 * @return un booléen indiquant si l'utilisateur est connecté.
	 */
	public boolean suisJeConnecte(){
		Participant part = new Participant(TestFram.getMaPartie().url);
		JSONArray parti = part.listeDesParticipants(TestFram.idSalon);

		for (int i = 0; i<parti.size(); i++){
			JSONObject part_objet = (JSONObject) parti.get(i);
			if (part_objet.get("Utilisateur_idUtilisateur").equals(api.idUser) && 
					!part_objet.get("etat").equals("EN_LIGNE")){
				return false;
			}
		}
		return true;
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet d'obtenir l'état du thread.
	 * 
	 * @return l'état du thread.
	 */
	public Boolean getEtat() {
		return etat;
	}

	/**
	 * Permet de changer l'état du thread.
	 * 
	 * @param etat le nouvel état.
	 */
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}

}
