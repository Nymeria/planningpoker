package thread;


import main.TestFram;
import org.json.simple.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Permet de gérer le jeu.
 *
 * @author morgan
 */
public class ThreadJeu extends Thread {

	private Thread t;
	private Boolean etat;
	static TestFram IHM;

	/**
	 * Construit le thread.
	 * 
	 * @param ihm le plateau de jeu.
	 */
	public ThreadJeu(TestFram ihm) {
		IHM = ihm;
		setEtat(true);
		this.start();
	}

	/**
	 * permet de récuprer les cartes jouer par les joueur
	 */
	private void recupererCarteJoueur() {
		if (IHM.tacheEnCour != null && IHM.tacheEnCour.equals("0") == false) {
			IHM.cartesRecto = TestFram.getMaPartie().voirCartesJoueurs(IHM.nbTour, TestFram.idSalon, IHM.tacheEnCour);
		}

		/* on veux s'assurer que l'utilisateur n'as pas déjà jouer */
		JSONObject uneCarte;
		String idUtilisateur;
		for (int i = 0; i < IHM.cartesRecto.size(); ++i) {
			uneCarte = (JSONObject) IHM.cartesRecto.get(i);
			idUtilisateur = (String) uneCarte.get("Utilisateur_idUtilisateur");
			/* si l'utilisateur à déjà jouer, on lui désactive l'interface */
			if (idUtilisateur != null && idUtilisateur.equals(IHM.getMaPartie().idUser)) {
				IHM.disableInterface();
			}
		}
	}

	public void nbCarteJouer() {

	}

	/**
	 * Permet d'afficher les cartes.
	 */
	private void afficherCarte() {
		int nb_participant = ThreadParticipant.nbParticipantEnLigne();
		int nb_cartes = 0;
		if (IHM.cartesRecto != null) {
			nb_cartes = IHM.cartesRecto.size();
		}

		if (IHM.participants != null) /* si tout les joueurs on jouer */ {
			if (nb_cartes > 0 && nb_cartes == nb_participant && IHM.finTour == false) {
				IHM.afficherCarteFaceRecto();
				IHM.finDuTour();
			}
			else{
				IHM.degrisage();
			}
		}
	}

	/**
	 * Permet de lancer le thread et le faire tourner en boucle.
	 */
	@Override
	public void run() {

		while (getEtat()) {
			recupererCarteJoueur();
			afficherCarte();

			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet de connaitre l'état du thread.
	 * 
	 * @return l'état du thread.
	 */
	public Boolean getEtat() {
		return etat;
	}

	/**
	 * Permet de changer l'état du threadL
	 * 
	 * @param etat le nouvel état du thread.
	 */
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}

}
