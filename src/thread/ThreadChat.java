package thread;

import main.Interface;
import main.TestFram;
import api.Chat;

import java.awt.TextArea;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Cette classe permet de gérer le chat dans un thread.
 */
public class ThreadChat extends Thread {
	private Thread t;
	private TextArea Chat;
	private static int jaiFini ;
	private Boolean etat;
	private Chat monApi;
	private String dernierMsg=" ";
	private TestFram IHM;
	private String ett="";
	private int nb_user3;
	private String ett2;
	private static int comp = 0;
	private static boolean fermer = false;


	/**
	 * Crée le thread du chat.
	 * 
	 * @param IHM la zone de jeu.
	 * @param Chat la zone de chat.
	 * @param apiChat le chat de l'api.
	 */
	public ThreadChat(TestFram IHM, TextArea Chat, Chat apiChat) {
		setEtat(true);
		this.IHM = IHM;
		this.Chat = Chat;
		monApi = apiChat;
		this.start();
	}

	@Override
	/**
	 * Permet de lancer le thread et le faire tourner en boucle.
	 */
	public void run() {
		while (getEtat()) {
			recevoirMessage();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Permet de récuperer les messages.
	 */
	public void recevoirMessage() {
		JSONArray messages = monApi.recevoirMessage(dernierMsg);

		/* on recupere tout les messages de la bases */
		for (int i=0; i< messages.size(); i++) {
			JSONObject message =  (JSONObject) messages.get(i);
			
			String msg = message.get("Utilisateur_idUtilisateur") + " > "+message.get("message") + "\n";
			if (dernierMsg != null && dernierMsg.isEmpty() == false ) {
				int nb_user;
				int nb_user2;
				
				if (IHM.participants != null && IHM.clavardeurs!= null ) { //verifi que les liste sont pas null
					comp = 0;
					
					for (nb_user = 0; nb_user < IHM.clavardeurs.size(); nb_user++) { 			// parcoure la liste des clavardeur
						JSONObject part = (JSONObject) IHM.clavardeurs.get(nb_user);
						String id = (String) part.get("Utilisateur_idUtilisateur");
						
						for (nb_user2 = 0; nb_user2 < IHM.participants.size(); nb_user2++) {			//cherche le clavardeur dans la liste des participant
							JSONObject parti = (JSONObject) IHM.participants.get(nb_user2);
							
							String id_par = (String) parti.get("Utilisateur_idUtilisateur");
							if (id_par.equals(id)){														// regarde sont etat si le participant et la 
								ett =  (String) parti.get("etat");
								break;
							}
						}
						if (ett != null && ett.equals("EN_LIGNE")){							// si il est en ligne on ajout au compteur
							
							comp = comp +1;
							ett ="";
						}	
					}
				}
			}
			dernierMsg = (String) message.get("timestamp");
			if (message.get("message").equals("j'ai fini d'argumenter")){  					//verifi si c le bon mesage
				jaiFini = jaiFini +1 ;
			}
			Chat.append(msg);
			for (nb_user3 = 0; nb_user3 < IHM.participants.size(); nb_user3++) {			
				JSONObject parti = (JSONObject) IHM.participants.get(nb_user3);
				String id_par = (String) parti.get("Utilisateur_idUtilisateur");
				if (id_par.equals((String)Interface.getMaPartie().idUser)){														
					ett2 =  (String) parti.get("etat");
					break;
				}
			}
			if ((message.get("message").equals("c'est déconnecter") && Interface.getMaPartie().detailSalon(Interface.idSalon).get("partieDemarrer").equals("1") && !ett2.equals("EN_ATTENTE"))){
				comp = comp + 1;
			}
			if (getJaiFini() >= getComp() && Interface.getMaPartie().detailSalon(Interface.idSalon).get("partieDemarrer").equals("1") && !ett2.equals("EN_ATTENTE")){
				
				IHM.enableInterface();
				IHM.debutDeTour();
				setComp(0);
				setJaiFini(0);
			}
			if (message.get("message").equals("Le scrum master à mis fin à la discution") && Interface.getMaPartie().detailSalon(Interface.idSalon).get("partieDemarrer").equals("1") && !ett2.equals("EN_ATTENTE")){
				IHM.chat.disableChat();
				IHM.enableInterface();
				IHM.debutDeTour();
				setComp(0);
				setJaiFini(0);
			}
		}
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet d'obtenir l'état du thread.
	 * 
	 * @return l'état du thread.
	 */
	public Boolean getEtat() {
		return etat;
	}

	/**
	 * Permet de changer la valeur de l'état du thread.
	 * 
	 * @param etat la nouvelle valeur de l'état.
	 */
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}

	/**
	 * Permet d'obtenir la valeur du nombre d'utilisateurs en ligne.
	 * 
	 * @return la valeur du nombre d'utilisateurs en ligne.
	 */
	public static int getComp() {
		return comp;
	}

	/**
	 * Permet de changer la valeur du nombre d'utilisateurs en ligne.
	 * 
	 * @param comp1 la valeur du nombre d'utilisateurs en ligne.
	 */
	public static void setComp(int comp1) {
		comp = comp1;
	}

	/**
	 * Permet de savoir si l'utilisateur à fini de parler ou non.
	 * 
	 * @return le booléen indiquant si l'utilisateur a fini de parler.
	 */
	public int getJaiFini() {
		return jaiFini;
	}

	/**
	 * Permet d'indiquer que l'utilisateur a fini de parler ou non.
	 * 
	 * @return le booléen qui indique si l'utilisateur a fini de parler.
	 */
	public static void setJaiFini(int jaiFini1) {
		jaiFini = jaiFini1;
	}

	public boolean isFermer() {
		return fermer;
	}

	public static void setFermer(boolean fermer1) {
		fermer = fermer1;
	}



}