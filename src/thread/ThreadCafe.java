package thread;


import main.TestFram;
import org.json.simple.JSONArray;

/**
 * Permet de gérer le thread pour les cartes café.
 * 
 * @author morgan
 */
public class ThreadCafe extends Thread{
	private TestFram IHM;
	private boolean etat;
	private int n_tour=-1;

	/**
	 * Création du thread.
	 * 
	 * @param ihm le plateau de jeu.
	 */
	public ThreadCafe(TestFram ihm) {
		IHM = ihm;
		setEtat(true);
		this.start();
	}

	@Override
	/**
	 * Permet de lancer le thread et le faire tourner en boucle.
	 */
	public void run() {

		while (isEtat()) {
			if (IHM.is_init == true) {
				try {
					JSONArray cafes = IHM.recupererCarteCafe();
					if (cafes != null) {
						if (cafes.size()>0 && n_tour != IHM.nbTour ) {
							n_tour = IHM.nbTour;
							String utilisateur;
							IHM.popUp("Une carte café à été demandé" , "Demande de pause");
						}
					}
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet d'obtenir l'état du thread.
	 * 
	 * @return l'état du thread.
	 */
	public boolean isEtat() {
		return etat;
	}

	/**
	 * Permet de donner une valeur à létat du thread.
	 * 
	 * @param etat la nouvelle valeur de l'état.
	 */
	public void setEtat(boolean etat) {
		this.etat = etat;
	}

}
