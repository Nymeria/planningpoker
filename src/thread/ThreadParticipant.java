package thread;


import main.TestFram;
import api.Participant;
import org.json.simple.JSONObject;

/**
 * Permet de gérer la liste des participants.
 * 
 * @author morgan
 */
public class ThreadParticipant extends Thread {

	private Thread t;
	private Boolean etat;
	static TestFram IHM;
	private Participant api;

	/**
	 * Construit le thread.
	 * 
	 * @param ihm le plateau de jeu.
	 */
	public ThreadParticipant(TestFram ihm) {
		ThreadParticipant.IHM = ihm;
		api = new Participant(TestFram.getMaPartie().url);
		setEtat(true);
		this.start();
	}

	/**
	 * Met à jour la liste des partipants.
	 */
	private void miseAJourParticipant() {
		/* on récupère la liste des participants, pour s'assuré que personne ne soit partie */
		if (TestFram.idSalon != null) {
			IHM.participants = api.listeDesParticipants(TestFram.idSalon);
		}
	}

	/**
	 * Cette méthode permet de compter le nombre de joueurs en ligne.
	 *
	 * @return le nombre de joueurs en ligne.
	 */
	public static int nbParticipantEnLigne() {
		if (IHM.participants != null) {
			int nb = 0;

			for (int i = 0; i < IHM.participants.size(); i++) {
				JSONObject participant = (JSONObject) IHM.participants.get(i);
				if (participant.get("etat") != null && participant.get("etat").equals("EN_LIGNE")) {
					nb++;
				}
			}
			return nb;
		} else {
			return 0;
		}
	}

	/**
	 * Permet de récuperer la liste des participants.
	 */
	private void recupererParticipant() {
		int size;
		int nb_participant;
		if (IHM.participants != null)
			size = IHM.participants.size();
		else
			size = 0;
		nb_participant = nbParticipantEnLigne();
		miseAJourParticipant();
		/* on lance la méthode de mise à jour des particpants */
		if (IHM.participants != null) {
			if (size == 0) {
				IHM.demarrerThreadAttente();

			}
			if (IHM.participants.size() > size || nb_participant != nbParticipantEnLigne()) {
				IHM.participantDansTableau();
				IHM.afficherCarteFaceVerso();
			}
		}

	}


	@Override
	/**
	 * Permet de lancer le thread et le faire tourner en boucle.
	 */
	public void run() {

		while (getEtat()) {
			if (IHM.is_init == true) {
				recupererParticipant();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet de récuperer l'état du thread.
	 * 
	 * @return l'état du thread
	 */
	public Boolean getEtat() {
		return etat;
	}

	/** Permet de changer l'état du thread.
	 * 
	 * @param etat le nouvel état du thread.
	 */
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}

}
