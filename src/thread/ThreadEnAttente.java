package thread;

import api.Partie;
import main.TestFram;

import org.json.simple.JSONObject;

/**
 * Permet de gérer le fait que les utilisateurs soient en attente ou non.
 * 
 * @author morgan
 */
public class ThreadEnAttente extends Thread {

	private Thread t;
	private Boolean etat;
	static TestFram IHM;
	private Partie api;

	/**
	 * Construit le thread.
	 * 
	 * @param ihm la zone de jeu.
	 */
	public ThreadEnAttente(TestFram ihm) {
		this.IHM = ihm;
		api = new Partie(TestFram.getMaPartie().url);
		api.idUser = TestFram.getMaPartie().idUser;
		api.is_logged = true;
		setEtat(true);
		this.start();
	}

	@Override
	/**
	 * Permet de lancer le thread et le faire tourner en boucle.
	 */
	public void run() {
		while (getEtat()) {

			JSONObject tache = api.etatTache(IHM.tache_rejoint);

			/* si la tache qui était en cours d'évaluation à été voté, je peux passer en mode en ligne */
			if (tache != null && tache.get("estVoter") != null) {
				api.mettreJoeurEnLigne(IHM.idSalon);
				ThreadChat.setComp(0);
				ThreadChat.setJaiFini(0);
				etat = false;
			}
		}
	}

	/**
	 * Permet d'arrêter le thread.
	 */
	public void arret() {
		setEtat(false);
	}

	/**
	 * Permet d'obtenir l'état du thread.
	 * 
	 * @return l'état du thread.
	 */
	public Boolean getEtat() {
		return etat;
	}

	/**
	 * Permet de changer la valeur de l'état du thread.
	 * 
	 * @param etat la nouvelle valeur de l'état du thread.
	 */
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}

}
